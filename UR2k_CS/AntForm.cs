﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Windows.Forms;
using UHFReader;
using UHFReader.Model;

namespace UR2k_CS
{
    public partial class AntForm : Form
    {
        private Reader reader;
        private Client currentClient;
        private ResourceManager rm;
        private CheckBox[] multiAntCB;
        private string noDeviceChoose;

        public AntForm()
        {
            InitializeComponent();
        }

        public AntForm(Reader reader, Client currentClient, ResourceManager rm)
        {
            InitializeComponent();
            this.reader = reader;
            this.currentClient = currentClient;
            this.rm = rm;
            noDeviceChoose = rm.GetString("strChooseDevice");
            multiAntCB = new CheckBox[] { checkBox56 , checkBox57, checkBox59, checkBox58, checkBox63, checkBox62, checkBox61, checkBox60,
                                          checkBox79 , checkBox78, checkBox77, checkBox76, checkBox75, checkBox74, checkBox73, checkBox72,
                                          checkBox87 , checkBox86, checkBox85, checkBox84, checkBox83, checkBox82, checkBox81, checkBox80,
                                          checkBox71 , checkBox70, checkBox69, checkBox68, checkBox67, checkBox66, checkBox65, checkBox64,
            };
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void checkBox77_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox78_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox79_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox87_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox86_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox85_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox76_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox84_CheckedChanged(object sender, EventArgs e)
        {

        }

        internal void UpdateMultiAnt(multichannelAnt multichannelAnt)
        {
            for (int i = 0; i < 32; i++)
            {
                multiAntCB[i].Checked = false;
            }
            for (int index = 0; index < multichannelAnt.enable.Length; index++)
            {
                if (multichannelAnt.enable[index] == 0x01)
                {
                    multiAntCB[index].Checked = true;
                }
                else
                {
                    multiAntCB[index].Checked = false;
                }
            }
            MultiAntTime.Text = multichannelAnt.dwell_time.ToString();
            MultiAntPowerTB.Text = multichannelAnt.power.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                if (currentClient != null)
                {
                    reader.GetAnt(currentClient);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            int[] dt = new int[4];
            int[] pwr = new int[4];
            if (int.TryParse(MultiAntTime.Text, out dt[0]) || MultiAntTime.Text == "")
            {
                if (dt[0] < 50 || dt[0] > 10000)
                {
                    MessageBox.Show(rm.GetString("strErrorAnttime"));
                    return;
                }
            }
            if (int.TryParse(MultiAntPowerTB.Text, out pwr[0]))
            {
                if (pwr[0] > 33 || MultiAntPowerTB.Text == "")
                {
                    MessageBox.Show(rm.GetString("strErrorPower"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorPower"));
                return;
            }

            byte channel = 16;
            for (int i = 24; i < 32; i++)
            {
                if (multiAntCB[i].Checked == true)
                {
                    channel = 32;
                }
            }
            for (int i = 16; i < 24; i++)
            {
                if (multiAntCB[i].Checked == true)
                {
                    channel = 32;
                }
            }
            if (rb32Ant.Checked == true)
            {
                channel = 32;
            }
            multichannelAnt ant = new multichannelAnt(channel);
            ant.state = channel;
            if (ant.state == 16)
            {
                for (int index = 0; index < 16; index++)
                {
                    if (multiAntCB[index].Checked == true)
                    {
                        ant.enable[index] = 0x01;
                    }
                }
            }
            else if (ant.state == 32)
            {
                for (int index = 0; index < 32; index++)
                {
                    if (multiAntCB[index].Checked == true)
                    {
                        ant.enable[index] = 0x01;
                    }
                }
            }
            ant.dwell_time = int.Parse(MultiAntTime.Text);
            ant.power = int.Parse(MultiAntPowerTB.Text);
            try
            {
                if (currentClient != null)
                {
                    reader.SetMultiAnt(currentClient, ant);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnGetAnts_Click(object sender, EventArgs e)
        {
            try
            {
                if (currentClient != null)
                {
                    reader.GetAnt(currentClient);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnSetAnts_Click(object sender, EventArgs e)
        {
            int[] dt = new int[4];
            int[] pwr = new int[4];

            System.DateTime currentTime = new System.DateTime();
            currentTime = System.DateTime.Now;
            string strT = currentTime.ToString("t");//<llp>

            if (int.TryParse(comboBoxWT1.Text, out dt[0]))
            {
                if (dt[0] < 50 || dt[0] > 10000)
                {
                    MessageBox.Show(rm.GetString("strErrorAnttime"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorAnttime"));
                return;
            }
            if (int.TryParse(comboBoxWT2.Text, out dt[1]))
            {
                if (dt[1] < 50 || dt[1] > 10000)
                {
                    MessageBox.Show(rm.GetString("strErrorAnttime"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorAnttime"));
                return;
            }
            if (int.TryParse(comboBoxWT3.Text, out dt[2]))
            {
                if (dt[2] < 50 || dt[2] > 10000)
                {
                    MessageBox.Show(rm.GetString("strErrorAnttime"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorAnttime"));
                return;
            }
            if (int.TryParse(comboBoxWT4.Text, out dt[3]))
            {
                if (dt[3] < 50 || dt[3] > 10000)
                {
                    MessageBox.Show(rm.GetString("strErrorAnttime"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorAnttime"));
                return;
            }
            if (int.TryParse(comboBoxPower1.Text, out pwr[0]))
            {
                if (pwr[0] > 33)
                {
                    MessageBox.Show(rm.GetString("strErrorPower"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorPower"));
                return;
            }
            if (int.TryParse(comboBoxPower2.Text, out pwr[1]))
            {
                if (pwr[1] > 33)
                {
                    MessageBox.Show(rm.GetString("strErrorPower"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorPower"));
                return;
            }
            if (int.TryParse(comboBoxPower3.Text, out pwr[2]))
            {
                if (pwr[2] > 33)
                {
                    MessageBox.Show(rm.GetString("strErrorPower"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorPower"));
                return;
            }
            if (int.TryParse(comboBoxPower4.Text, out pwr[3]))
            {
                if (pwr[3] > 33)
                {
                    MessageBox.Show(rm.GetString("strErrorPower"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorPower"));
                return;
            }

            /* if (0 == NR2k.SetAnt(currentDevice.hDev, ref AntCfg))
             {
                 labAnt.Text = "天线参数设置成功  " + strT;
             }
             else
             {
                 labAnt.Text = "天线参数设置失败  " + strT;
             }*/
            try
            {
                AntInfo antnfo = new AntInfo(4);
                antnfo.antEnable[0] = (byte)(cbAnt1.Checked ? 1 : 0);
                antnfo.antEnable[1] = (byte)(cbAnt2.Checked ? 1 : 0);
                antnfo.antEnable[2] = (byte)(cbAnt3.Checked ? 1 : 0);
                antnfo.antEnable[3] = (byte)(cbAnt4.Checked ? 1 : 0);
                for (int i = 0; i < 4; ++i)
                {
                    antnfo.dwell_time[i] = dt[i];
                    antnfo.power[i] = pwr[i] * 10;
                }
                if (currentClient != null)
                {
                    reader.SetAnt(currentClient, antnfo);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void GetAntStateBt_Click(object sender, EventArgs e)
        {
            try
            {
                if (currentClient != null)
                {
                    reader.GetAntState(currentClient);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        internal void UpdateAntInfo(AntInfo antInfo)
        {
            cbAnt1.Checked = (antInfo.antEnable[0] == 1);
            cbAnt2.Checked = (antInfo.antEnable[1] == 1);
            cbAnt3.Checked = (antInfo.antEnable[2] == 1);
            cbAnt4.Checked = (antInfo.antEnable[3] == 1);
            comboBoxWT1.Text = antInfo.dwell_time[0].ToString();
            comboBoxWT2.Text = antInfo.dwell_time[1].ToString();
            comboBoxWT3.Text = antInfo.dwell_time[2].ToString();
            comboBoxWT4.Text = antInfo.dwell_time[3].ToString();
            comboBoxPower1.Text = (antInfo.power[0] / 10).ToString();
            comboBoxPower2.Text = (antInfo.power[1] / 10).ToString();
            comboBoxPower3.Text = (antInfo.power[2] / 10).ToString();
            comboBoxPower4.Text = (antInfo.power[3] / 10).ToString();
        }

        private void checkBox95_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox95.Checked)
            {
                for (int index = 0; index < 8; index++)
                {
                    multiAntCB[index].Checked = true;
                }
            }
            else
            {
                for (int index = 0; index < 8; index++)
                {
                    multiAntCB[index].Checked = false;
                }
            }
        }

        private void checkBox91_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox91.Checked)
            {
                for (int index = 8; index < 16; index++)
                {
                    multiAntCB[index].Checked = true;
                }
            }
            else
            {
                for (int index = 8; index < 16; index++)
                {
                    multiAntCB[index].Checked = false;
                }
            }
        }

        private void checkBox90_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox90.Checked)
            {
                for (int index = 16; index < 24; index++)
                {
                    multiAntCB[index].Checked = true;
                }
            }
            else
            {
                for (int index = 16; index < 24; index++)
                {
                    multiAntCB[index].Checked = false;
                }
            }
        }

        private void checkBox89_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox89.Checked)
            {
                for (int index = 24; index < 32; index++)
                {
                    multiAntCB[index].Checked = true;
                }
            }
            else
            {
                for (int index = 24; index < 32; index++)
                {
                    multiAntCB[index].Checked = false;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace UR2k_CS
{
    public struct PANT_CFG
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] antEnable;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public UInt32[] dwell_time;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public UInt32[] power;
    }
    public class NR2k
    {
        public const int R_OK = 0;
        public const int R_FAIL = -1;
        // 回调函数类型
        public delegate void HANDLE_FUN(int hDev, int cmdID, IntPtr pData, int length, IntPtr pHost);
        // 导入动态库函数
        [DllImport("NR2k.dll")]
        public static extern int ConnectDev(ref int hDev, byte[] host );

        [DllImport("NR2k.dll")]
        public static extern int DisconnectDev(ref int  hDev);

        [DllImport("NR2k.dll")]
        public static extern int GetDevVersion(int hDev, byte[] pVer);

        [DllImport("NR2k.dll")]
        public static extern int BeginInv(int hDev, HANDLE_FUN f);

        [DllImport("NR2k.dll")]
        public static extern int StopInv(int hDev);

        [DllImport("NR2k.dll")]
        public static extern int InvOnce(int hDev, HANDLE_FUN f);

        [DllImport("NR2k.dll")]
        public static extern int GetAnt(int hDev, out PANT_CFG antcfg);

        [DllImport("NR2k.dll")]
        public static extern int SetAnt(int hDev, ref  PANT_CFG antcfg);

       [DllImport("NR2k.dll")]
        public static extern int SetDO(int hDev,  byte port,  byte state);

       [DllImport("NR2k.dll")]
       public static extern int GetDI(int hDev, byte[] state);

       [DllImport("NR2k.dll")]
        public static extern int SetDeviceNo(int hDev, byte deviceNo);

        [DllImport("NR2k.dll")]
        public static extern int GetDeviceNo(int hDev, out byte deviceNo);

        [DllImport("NR2k.dll")]
        public static extern int GetNeighJudge(int hDev, out byte Enabled, out byte Time);

        [DllImport("NR2k.dll")]
        public static extern int SetNeighJudge(int  hDev, byte  time);

        [DllImport("NR2k.dll")]
        public static extern int ReadTagData(int hDev, byte bank, byte begin, byte size, byte[] OutData, byte[] Password);

        [DllImport("NR2k.dll")]
        public static extern int WriteTagData(int hDev, byte bank, byte begin, byte size, byte[] Data, byte[] Password);

        [DllImport("NR2k.dll")]
        public static extern int LockTag(int hDev, byte opcode, byte block, byte[] Password);

        [DllImport("NR2k.dll")]
        public static extern int KillTag(int hDev, byte[] Kill_pwd, byte[] Access_pwd);

        [DllImport("NR2k.dll")]
        public static extern int  SetBuzzer(int hDev, byte status);

         [DllImport("NR2k.dll")]
        public static extern int  GetBuzzer(int hDev, out byte status);

         [DllImport("NR2k.dll")]
        public static extern int GetWorkMode(int hDev, out byte mode);

        [DllImport("NR2k.dll")]
        public static extern int SetWorkMode(int hDev, byte mode);

        [DllImport("NR2k.dll")]
        public static extern int  GetTrigModeDelayTime(int hDev, out byte time);

        [DllImport("NR2k.dll")]
        public static extern int SetTrigModeDelayTime(int hDev, byte time);

        [DllImport("NR2k.dll")]
        public static extern int SetClock(int hDev, byte[] clock);

         [DllImport("NR2k.dll")]
        public static extern int GetClock(int hDev, byte[] clock);

        [DllImport("NR2k.dll")]
        public static extern int  GetReadZone(int hDev, out byte zone);

         [DllImport("NR2k.dll")]
        public static extern int SetReadZone(int hDev, byte Zone);

         [DllImport("NR2k.dll")]
        public static extern int GetReadZonePara(int hDev, out byte bank, out byte begin, out byte length);

        [DllImport("NR2k.dll")]
        public static extern int SetReadZonePara(int hDev, byte bank, byte begin, byte length);

        [DllImport("NR2k.dll")]
        public static extern int  ResetTagBuffer(int hDev);

        [DllImport("NR2k.dll")]
        public static extern int ReadTagBuffer(int hDev, HANDLE_FUN f, int  readtime = 2000);
    }
}

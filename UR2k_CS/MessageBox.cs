﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using UHFReader;
using UHFReader.Model;
using static UHFReader.Filter;

namespace UR2k_CS
{
    public partial class ProcessBox : Form
    {
        private Reader reader;
        private Client currentClient;
        private ResourceManager rm;


        public ProcessBox()
        {
            InitializeComponent();
        }

        public ProcessBox(Reader reader, Client currentClient, ResourceManager rm)
        {
            InitializeComponent();

            this.reader = reader;
            this.currentClient = currentClient;
            this.rm = rm;
            this.FormClosing += CloseMessage;

            progressBar1.Maximum = 200;
            progressBar1.Value = 0;
            progressBar1.Step = 1;

            language(rm);
        }

        private void CloseMessage(object sender, FormClosingEventArgs e)
        {
            if (isCancle)
            {
                if (MessageBox.Show(rm.GetString("strExit"), rm.GetString("strExitTips"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    e.Cancel = true;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            reader.IntelligentDetectionStop(currentClient);
            this.Close();
        }

        bool isCancle = false;
        private void button2_Click(object sender, EventArgs e)
        {
            reader.IntelligentDetectionStop(currentClient);
            Thread.Sleep(10);
            this.Close();
            isCancle = true;
        }

        internal void UpdateMessage(intelligentDetectionEventArgs result)
        {
            showMessageLv.Items.Add(result.result);
            this.showMessageLv.TopIndex = this.showMessageLv.Items.Count - (int)(this.showMessageLv.Height / this.showMessageLv.ItemHeight);
            if (result.step >= 100)
            {
                progressBar1.Value = 200;
            }
            else
            {
                if(progressBar1.Value<200)
                progressBar1.Value++;
            }
            if (result.step == 0xFF)            //有错误
            {
                reader.IntelligentDetectionStop(currentClient);
                MessageBox.Show("请接入读写器，并重检测");
            }
            if (progressBar1.Value == progressBar1.Maximum)
            {
                button1.Enabled = true;
            }
        }

        private void showMessageLv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.C | e.Control)
            {
                string str = "";
                for (int i = 0; i < showMessageLv.Items.Count; i++)
                {
                    str += showMessageLv.Items[i].ToString() + "\r\n";
                }
                Clipboard.SetDataObject(str);
            }
        }

        internal void language(ResourceManager rm)
        {
            this.rm = rm;
            button1.Text = rm.GetString("strSure");
            button2.Text = rm.GetString("Close");
            label1.Text = rm.GetString("strMatchPointsProgress");

        }
    }
}
 
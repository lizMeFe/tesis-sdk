﻿using System;

namespace UR2k_CS
{
    partial class MainWindow
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TagAccessPage = new System.Windows.Forms.TabPage();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.查询标签ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.寻卡模式ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.频点ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.天线ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.数据显示格式ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button15 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.labelTagTIDCount = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labReadClock = new System.Windows.Forms.Label();
            this.readSpeedLB = new System.Windows.Forms.Label();
            this.TotalTagLB = new System.Windows.Forms.Label();
            this.ReadSpeedSLb = new System.Windows.Forms.Label();
            this.readCountsLB = new System.Windows.Forms.Label();
            this.labelTagCount = new System.Windows.Forms.Label();
            this.ReadCountLB = new System.Windows.Forms.Label();
            this.IsSingleDeviceCB = new System.Windows.Forms.CheckBox();
            this.RefreshTimeSetBtn = new System.Windows.Forms.Button();
            this.labVersion = new System.Windows.Forms.Label();
            this.btnClearBuffer = new System.Windows.Forms.Button();
            this.btnReadBuffer = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnStopInv = new System.Windows.Forms.Button();
            this.btnStartInv = new System.Windows.Forms.Button();
            this.btnInvOnce = new System.Windows.Forms.Button();
            this.KillPWDTB = new System.Windows.Forms.TabPage();
            this.gbIOOpr = new System.Windows.Forms.GroupBox();
            this.btnSetOutPort = new System.Windows.Forms.Button();
            this.btnGetDI = new System.Windows.Forms.Button();
            this.cbOut2 = new System.Windows.Forms.CheckBox();
            this.cbOut1 = new System.Windows.Forms.CheckBox();
            this.cbIn2 = new System.Windows.Forms.CheckBox();
            this.cbIn1 = new System.Windows.Forms.CheckBox();
            this.labResult = new System.Windows.Forms.Label();
            this.gbLockTag = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.KillTagPWDTB = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.btnLockTag = new System.Windows.Forms.Button();
            this.labLockType = new System.Windows.Forms.Label();
            this.labLockBank = new System.Windows.Forms.Label();
            this.labLockAccessPwd = new System.Windows.Forms.Label();
            this.tbLockAccessPwd = new System.Windows.Forms.TextBox();
            this.cbbLockType = new System.Windows.Forms.ComboBox();
            this.cbbLockBank = new System.Windows.Forms.ComboBox();
            this.gbRWData = new System.Windows.Forms.GroupBox();
            this.labRWAccessPwd = new System.Windows.Forms.Label();
            this.labData = new System.Windows.Forms.Label();
            this.labLength = new System.Windows.Forms.Label();
            this.labStartAdd = new System.Windows.Forms.Label();
            this.labOprBank = new System.Windows.Forms.Label();
            this.tbRWAccessPwd = new System.Windows.Forms.TextBox();
            this.btnWriteData = new System.Windows.Forms.Button();
            this.btnClearData = new System.Windows.Forms.Button();
            this.btnReadData = new System.Windows.Forms.Button();
            this.tbRWData = new System.Windows.Forms.TextBox();
            this.cbbLength = new System.Windows.Forms.ComboBox();
            this.cbbStartAdd = new System.Windows.Forms.ComboBox();
            this.cbbRWBank = new System.Windows.Forms.ComboBox();
            this.DevParams = new System.Windows.Forms.TabPage();
            this.gbDevParams = new System.Windows.Forms.GroupBox();
            this.frequncyGB = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.ConstantfrequencyTB = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.LBFrequencypoint = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.checkBox45 = new System.Windows.Forms.CheckBox();
            this.frequency50 = new System.Windows.Forms.CheckBox();
            this.frequency49 = new System.Windows.Forms.CheckBox();
            this.frequency48 = new System.Windows.Forms.CheckBox();
            this.frequency47 = new System.Windows.Forms.CheckBox();
            this.frequency46 = new System.Windows.Forms.CheckBox();
            this.frequency45 = new System.Windows.Forms.CheckBox();
            this.frequency44 = new System.Windows.Forms.CheckBox();
            this.frequency43 = new System.Windows.Forms.CheckBox();
            this.frequency42 = new System.Windows.Forms.CheckBox();
            this.frequency41 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.frequency40 = new System.Windows.Forms.CheckBox();
            this.frequency39 = new System.Windows.Forms.CheckBox();
            this.frequency38 = new System.Windows.Forms.CheckBox();
            this.frequency37 = new System.Windows.Forms.CheckBox();
            this.frequency36 = new System.Windows.Forms.CheckBox();
            this.frequency35 = new System.Windows.Forms.CheckBox();
            this.frequency34 = new System.Windows.Forms.CheckBox();
            this.frequency33 = new System.Windows.Forms.CheckBox();
            this.frequency32 = new System.Windows.Forms.CheckBox();
            this.frequency31 = new System.Windows.Forms.CheckBox();
            this.checkBox34 = new System.Windows.Forms.CheckBox();
            this.frequency30 = new System.Windows.Forms.CheckBox();
            this.frequency29 = new System.Windows.Forms.CheckBox();
            this.frequency28 = new System.Windows.Forms.CheckBox();
            this.frequency27 = new System.Windows.Forms.CheckBox();
            this.frequency26 = new System.Windows.Forms.CheckBox();
            this.frequency25 = new System.Windows.Forms.CheckBox();
            this.frequency24 = new System.Windows.Forms.CheckBox();
            this.frequency23 = new System.Windows.Forms.CheckBox();
            this.frequency22 = new System.Windows.Forms.CheckBox();
            this.frequency21 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.frequency20 = new System.Windows.Forms.CheckBox();
            this.frequency19 = new System.Windows.Forms.CheckBox();
            this.frequency18 = new System.Windows.Forms.CheckBox();
            this.frequency17 = new System.Windows.Forms.CheckBox();
            this.frequency16 = new System.Windows.Forms.CheckBox();
            this.frequency15 = new System.Windows.Forms.CheckBox();
            this.frequency14 = new System.Windows.Forms.CheckBox();
            this.frequency13 = new System.Windows.Forms.CheckBox();
            this.frequency12 = new System.Windows.Forms.CheckBox();
            this.frequency11 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.frequency10 = new System.Windows.Forms.CheckBox();
            this.frequency9 = new System.Windows.Forms.CheckBox();
            this.frequency8 = new System.Windows.Forms.CheckBox();
            this.frequency7 = new System.Windows.Forms.CheckBox();
            this.frequency6 = new System.Windows.Forms.CheckBox();
            this.frequency5 = new System.Windows.Forms.CheckBox();
            this.frequency4 = new System.Windows.Forms.CheckBox();
            this.frequency3 = new System.Windows.Forms.CheckBox();
            this.frequency2 = new System.Windows.Forms.CheckBox();
            this.BtnSetFrequency = new System.Windows.Forms.Button();
            this.BtnGetFrequency = new System.Windows.Forms.Button();
            this.frequency1 = new System.Windows.Forms.CheckBox();
            this.FrequencyAreaCB = new System.Windows.Forms.ComboBox();
            this.LBFRequency = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.factoryBtn = new System.Windows.Forms.Button();
            this.LoginBtn = new System.Windows.Forms.Button();
            this.SpecialFunTabTB = new System.Windows.Forms.TextBox();
            this.SpecalFunLB = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.Rb4Channel = new System.Windows.Forms.RadioButton();
            this.GetAntStateBt = new System.Windows.Forms.Button();
            this.btnSetAnts = new System.Windows.Forms.Button();
            this.rb32Ant = new System.Windows.Forms.RadioButton();
            this.rb16Ant = new System.Windows.Forms.RadioButton();
            this.MultiAntPowerTB = new System.Windows.Forms.ComboBox();
            this.comboBoxWT4 = new System.Windows.Forms.ComboBox();
            this.MultiAntTime = new System.Windows.Forms.ComboBox();
            this.comboBoxPower4 = new System.Windows.Forms.ComboBox();
            this.MultiAntPower = new System.Windows.Forms.Label();
            this.comboBoxWT3 = new System.Windows.Forms.ComboBox();
            this.MultiAntWorktime = new System.Windows.Forms.Label();
            this.comboBoxPower3 = new System.Windows.Forms.ComboBox();
            this.checkBox89 = new System.Windows.Forms.CheckBox();
            this.comboBoxWT2 = new System.Windows.Forms.ComboBox();
            this.checkBox90 = new System.Windows.Forms.CheckBox();
            this.comboBoxPower2 = new System.Windows.Forms.ComboBox();
            this.checkBox91 = new System.Windows.Forms.CheckBox();
            this.comboBoxWT1 = new System.Windows.Forms.ComboBox();
            this.checkBox95 = new System.Windows.Forms.CheckBox();
            this.comboBoxPower1 = new System.Windows.Forms.ComboBox();
            this.button6 = new System.Windows.Forms.Button();
            this.cbAnt4 = new System.Windows.Forms.CheckBox();
            this.button4 = new System.Windows.Forms.Button();
            this.cbAnt3 = new System.Windows.Forms.CheckBox();
            this.Ant32 = new System.Windows.Forms.CheckBox();
            this.cbAnt2 = new System.Windows.Forms.CheckBox();
            this.Ant31 = new System.Windows.Forms.CheckBox();
            this.cbAnt1 = new System.Windows.Forms.CheckBox();
            this.Ant30 = new System.Windows.Forms.CheckBox();
            this.Ant29 = new System.Windows.Forms.CheckBox();
            this.Ant28 = new System.Windows.Forms.CheckBox();
            this.Ant27 = new System.Windows.Forms.CheckBox();
            this.Ant26 = new System.Windows.Forms.CheckBox();
            this.Ant25 = new System.Windows.Forms.CheckBox();
            this.Ant24 = new System.Windows.Forms.CheckBox();
            this.Ant23 = new System.Windows.Forms.CheckBox();
            this.Ant22 = new System.Windows.Forms.CheckBox();
            this.Ant21 = new System.Windows.Forms.CheckBox();
            this.Ant20 = new System.Windows.Forms.CheckBox();
            this.Ant19 = new System.Windows.Forms.CheckBox();
            this.Ant18 = new System.Windows.Forms.CheckBox();
            this.Ant17 = new System.Windows.Forms.CheckBox();
            this.Ant16 = new System.Windows.Forms.CheckBox();
            this.Ant15 = new System.Windows.Forms.CheckBox();
            this.Ant14 = new System.Windows.Forms.CheckBox();
            this.Ant13 = new System.Windows.Forms.CheckBox();
            this.Ant12 = new System.Windows.Forms.CheckBox();
            this.Ant11 = new System.Windows.Forms.CheckBox();
            this.Ant10 = new System.Windows.Forms.CheckBox();
            this.Ant09 = new System.Windows.Forms.CheckBox();
            this.Ant08 = new System.Windows.Forms.CheckBox();
            this.Ant07 = new System.Windows.Forms.CheckBox();
            this.Ant06 = new System.Windows.Forms.CheckBox();
            this.Ant05 = new System.Windows.Forms.CheckBox();
            this.Ant04 = new System.Windows.Forms.CheckBox();
            this.Ant03 = new System.Windows.Forms.CheckBox();
            this.Ant02 = new System.Windows.Forms.CheckBox();
            this.Ant01 = new System.Windows.Forms.CheckBox();
            this.WorkModeGB = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.TriggerAlarmBtn = new System.Windows.Forms.Button();
            this.TriggerAlarmTimeTB = new System.Windows.Forms.TextBox();
            this.TriggerDelay = new System.Windows.Forms.Label();
            this.DataChannelSetBtn = new System.Windows.Forms.Button();
            this.DataChannelCB = new System.Windows.Forms.ComboBox();
            this.DataTransMode = new System.Windows.Forms.Label();
            this.TriggerDelayLB = new System.Windows.Forms.Label();
            this.tbDelayTime = new System.Windows.Forms.TextBox();
            this.btnSetDelayTime = new System.Windows.Forms.Button();
            this.btnGetDelayTime = new System.Windows.Forms.Button();
            this.WorkModeLB = new System.Windows.Forms.Label();
            this.comboBoxWorkMode = new System.Windows.Forms.ComboBox();
            this.btnSetWorkMode = new System.Windows.Forms.Button();
            this.btnGetWorkMode = new System.Windows.Forms.Button();
            this.labDevNo = new System.Windows.Forms.Label();
            this.tbDevNo = new System.Windows.Forms.TextBox();
            this.btnSetDevNo = new System.Windows.Forms.Button();
            this.btnGetDevNo = new System.Windows.Forms.Button();
            this.labNeightJudge = new System.Windows.Forms.Label();
            this.tbNeighJudge = new System.Windows.Forms.TextBox();
            this.btnSetNeighJudge = new System.Windows.Forms.Button();
            this.btnGetNeighJudge = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.rdoBuzzerOn = new System.Windows.Forms.RadioButton();
            this.rdoBuzzerOff = new System.Windows.Forms.RadioButton();
            this.btnSetBuzzer = new System.Windows.Forms.Button();
            this.btnGetBuzzer = new System.Windows.Forms.Button();
            this.labelResult = new System.Windows.Forms.Label();
            this.NetParams = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.HeartBeatOffRB = new System.Windows.Forms.RadioButton();
            this.HeartBeatOnRB = new System.Windows.Forms.RadioButton();
            this.gbSPParams = new System.Windows.Forms.GroupBox();
            this.labDataBits = new System.Windows.Forms.Label();
            this.labCheckBits = new System.Windows.Forms.Label();
            this.labBaudRate = new System.Windows.Forms.Label();
            this.comboBoxBaudRate = new System.Windows.Forms.ComboBox();
            this.comboBoxCheckBits = new System.Windows.Forms.ComboBox();
            this.comboBoxDataBits = new System.Windows.Forms.ComboBox();
            this.gbNetParams = new System.Windows.Forms.GroupBox();
            this.textBoxDestIP = new System.Windows.Forms.ComboBox();
            this.labPromotion = new System.Windows.Forms.Label();
            this.labDestPort = new System.Windows.Forms.Label();
            this.btnSetParams = new System.Windows.Forms.Button();
            this.labDestIP = new System.Windows.Forms.Label();
            this.labGateway = new System.Windows.Forms.Label();
            this.labPort = new System.Windows.Forms.Label();
            this.labMask = new System.Windows.Forms.Label();
            this.labIPAdd = new System.Windows.Forms.Label();
            this.labIPMode = new System.Windows.Forms.Label();
            this.labNetMode = new System.Windows.Forms.Label();
            this.textBoxDestPort = new System.Windows.Forms.TextBox();
            this.textBoxGateway = new System.Windows.Forms.TextBox();
            this.textBoxPortNo = new System.Windows.Forms.TextBox();
            this.textBoxNetMask = new System.Windows.Forms.TextBox();
            this.textBoxIPAdd = new System.Windows.Forms.TextBox();
            this.comboBoxIPMode = new System.Windows.Forms.ComboBox();
            this.comboBoxNetMode = new System.Windows.Forms.ComboBox();
            this.btnSearchDev = new System.Windows.Forms.Button();
            this.btnDefaultParams = new System.Windows.Forms.Button();
            this.lvZl = new System.Windows.Forms.ListView();
            this.columnHeaderNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderIPAdd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPort = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderMAC = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabSpecialFun = new System.Windows.Forms.TabPage();
            this.AutoMatchGb = new System.Windows.Forms.GroupBox();
            this.StartMatchBtn = new System.Windows.Forms.Button();
            this.DataFormateGB = new System.Windows.Forms.GroupBox();
            this.button13 = new System.Windows.Forms.Button();
            this.haveChannelCB = new System.Windows.Forms.CheckBox();
            this.button14 = new System.Windows.Forms.Button();
            this.haveRssiCB = new System.Windows.Forms.CheckBox();
            this.haveDevCB = new System.Windows.Forms.CheckBox();
            this.haveAntCB = new System.Windows.Forms.CheckBox();
            this.ReadCardModeGB = new System.Windows.Forms.GroupBox();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.valueABCB = new System.Windows.Forms.ComboBox();
            this.tagFocusCB = new System.Windows.Forms.ComboBox();
            this.ValueQCB = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.sessionCB = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.AutoGetConnBT = new System.Windows.Forms.Button();
            this.ConnectSerialCB = new System.Windows.Forms.GroupBox();
            this.comOpenBtn = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SerialNoLB = new System.Windows.Forms.Label();
            this.serialPortCB = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.ClientInfoLV = new System.Windows.Forms.ListView();
            this.Num = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.IP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.port = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.state = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button1 = new System.Windows.Forms.Button();
            this.Version = new System.Windows.Forms.Label();
            this.NetWorkModeCB = new System.Windows.Forms.GroupBox();
            this.ServerIPTB = new System.Windows.Forms.ComboBox();
            this.ServerStartBtn = new System.Windows.Forms.Button();
            this.ConnectSeverIPLB = new System.Windows.Forms.Label();
            this.ConnectSeverPortLB = new System.Windows.Forms.Label();
            this.ServerPortTB = new System.Windows.Forms.TextBox();
            this.ReadClock1 = new System.Windows.Forms.Timer(this.components);
            this.readonceClock = new System.Windows.Forms.Timer(this.components);
            this.GetClientsInfoTimer = new System.Windows.Forms.Timer(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.CurrentClientLB = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.languageSelectCB = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.AutoConnect = new System.Windows.Forms.GroupBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.tabControl1.SuspendLayout();
            this.TagAccessPage.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.KillPWDTB.SuspendLayout();
            this.gbIOOpr.SuspendLayout();
            this.gbLockTag.SuspendLayout();
            this.gbRWData.SuspendLayout();
            this.DevParams.SuspendLayout();
            this.gbDevParams.SuspendLayout();
            this.frequncyGB.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.WorkModeGB.SuspendLayout();
            this.NetParams.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.gbSPParams.SuspendLayout();
            this.gbNetParams.SuspendLayout();
            this.tabSpecialFun.SuspendLayout();
            this.AutoMatchGb.SuspendLayout();
            this.DataFormateGB.SuspendLayout();
            this.ReadCardModeGB.SuspendLayout();
            this.ConnectSerialCB.SuspendLayout();
            this.NetWorkModeCB.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.AutoConnect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.TagAccessPage);
            this.tabControl1.Controls.Add(this.KillPWDTB);
            this.tabControl1.Controls.Add(this.DevParams);
            this.tabControl1.Controls.Add(this.NetParams);
            this.tabControl1.Controls.Add(this.tabSpecialFun);
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // TagAccessPage
            // 
            this.TagAccessPage.BackColor = System.Drawing.Color.White;
            this.TagAccessPage.ContextMenuStrip = this.contextMenuStrip2;
            this.TagAccessPage.Controls.Add(this.button15);
            this.TagAccessPage.Controls.Add(this.panel3);
            this.TagAccessPage.Controls.Add(this.IsSingleDeviceCB);
            this.TagAccessPage.Controls.Add(this.RefreshTimeSetBtn);
            this.TagAccessPage.Controls.Add(this.labVersion);
            this.TagAccessPage.Controls.Add(this.btnClearBuffer);
            this.TagAccessPage.Controls.Add(this.btnReadBuffer);
            this.TagAccessPage.Controls.Add(this.btnClear);
            this.TagAccessPage.Controls.Add(this.btnStopInv);
            this.TagAccessPage.Controls.Add(this.btnStartInv);
            this.TagAccessPage.Controls.Add(this.btnInvOnce);
            resources.ApplyResources(this.TagAccessPage, "TagAccessPage");
            this.TagAccessPage.Name = "TagAccessPage";
            this.TagAccessPage.Click += new System.EventHandler(this.TagAccessPage_Click);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.查询标签ToolStripMenuItem,
            this.寻卡模式ToolStripMenuItem,
            this.频点ToolStripMenuItem,
            this.天线ToolStripMenuItem,
            this.数据显示格式ToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            resources.ApplyResources(this.contextMenuStrip2, "contextMenuStrip2");
            // 
            // 查询标签ToolStripMenuItem
            // 
            resources.ApplyResources(this.查询标签ToolStripMenuItem, "查询标签ToolStripMenuItem");
            this.查询标签ToolStripMenuItem.Name = "查询标签ToolStripMenuItem";
            this.查询标签ToolStripMenuItem.Click += new System.EventHandler(this.查询标签ToolStripMenuItem_Click);
            // 
            // 寻卡模式ToolStripMenuItem
            // 
            resources.ApplyResources(this.寻卡模式ToolStripMenuItem, "寻卡模式ToolStripMenuItem");
            this.寻卡模式ToolStripMenuItem.Name = "寻卡模式ToolStripMenuItem";
            this.寻卡模式ToolStripMenuItem.Click += new System.EventHandler(this.寻卡模式ToolStripMenuItem_Click);
            // 
            // 频点ToolStripMenuItem
            // 
            resources.ApplyResources(this.频点ToolStripMenuItem, "频点ToolStripMenuItem");
            this.频点ToolStripMenuItem.Name = "频点ToolStripMenuItem";
            this.频点ToolStripMenuItem.Click += new System.EventHandler(this.频点ToolStripMenuItem_Click);
            // 
            // 天线ToolStripMenuItem
            // 
            resources.ApplyResources(this.天线ToolStripMenuItem, "天线ToolStripMenuItem");
            this.天线ToolStripMenuItem.Name = "天线ToolStripMenuItem";
            this.天线ToolStripMenuItem.Click += new System.EventHandler(this.天线ToolStripMenuItem_Click);
            // 
            // 数据显示格式ToolStripMenuItem
            // 
            resources.ApplyResources(this.数据显示格式ToolStripMenuItem, "数据显示格式ToolStripMenuItem");
            this.数据显示格式ToolStripMenuItem.Name = "数据显示格式ToolStripMenuItem";
            this.数据显示格式ToolStripMenuItem.Click += new System.EventHandler(this.数据显示格式ToolStripMenuItem_Click);
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.Transparent;
            this.button15.BackgroundImage = global::UR2k_CS.Properties.Resources.fanhui_1;
            resources.ApplyResources(this.button15, "button15");
            this.button15.FlatAppearance.BorderSize = 0;
            this.button15.Name = "button15";
            this.toolTip1.SetToolTip(this.button15, resources.GetString("button15.ToolTip"));
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Gainsboro;
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.labelTagTIDCount);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.labReadClock);
            this.panel3.Controls.Add(this.readSpeedLB);
            this.panel3.Controls.Add(this.TotalTagLB);
            this.panel3.Controls.Add(this.ReadSpeedSLb);
            this.panel3.Controls.Add(this.readCountsLB);
            this.panel3.Controls.Add(this.labelTagCount);
            this.panel3.Controls.Add(this.ReadCountLB);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // labelTagTIDCount
            // 
            resources.ApplyResources(this.labelTagTIDCount, "labelTagTIDCount");
            this.labelTagTIDCount.ForeColor = System.Drawing.Color.Blue;
            this.labelTagTIDCount.Name = "labelTagTIDCount";
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // labReadClock
            // 
            resources.ApplyResources(this.labReadClock, "labReadClock");
            this.labReadClock.ForeColor = System.Drawing.Color.Blue;
            this.labReadClock.Name = "labReadClock";
            // 
            // readSpeedLB
            // 
            resources.ApplyResources(this.readSpeedLB, "readSpeedLB");
            this.readSpeedLB.ForeColor = System.Drawing.Color.Blue;
            this.readSpeedLB.Name = "readSpeedLB";
            // 
            // TotalTagLB
            // 
            resources.ApplyResources(this.TotalTagLB, "TotalTagLB");
            this.TotalTagLB.Name = "TotalTagLB";
            this.TotalTagLB.Click += new System.EventHandler(this.label2_Click);
            // 
            // ReadSpeedSLb
            // 
            resources.ApplyResources(this.ReadSpeedSLb, "ReadSpeedSLb");
            this.ReadSpeedSLb.Name = "ReadSpeedSLb";
            // 
            // readCountsLB
            // 
            resources.ApplyResources(this.readCountsLB, "readCountsLB");
            this.readCountsLB.ForeColor = System.Drawing.Color.Blue;
            this.readCountsLB.Name = "readCountsLB";
            // 
            // labelTagCount
            // 
            resources.ApplyResources(this.labelTagCount, "labelTagCount");
            this.labelTagCount.ForeColor = System.Drawing.Color.Blue;
            this.labelTagCount.Name = "labelTagCount";
            // 
            // ReadCountLB
            // 
            resources.ApplyResources(this.ReadCountLB, "ReadCountLB");
            this.ReadCountLB.Name = "ReadCountLB";
            // 
            // IsSingleDeviceCB
            // 
            resources.ApplyResources(this.IsSingleDeviceCB, "IsSingleDeviceCB");
            this.IsSingleDeviceCB.Name = "IsSingleDeviceCB";
            this.IsSingleDeviceCB.UseVisualStyleBackColor = true;
            // 
            // RefreshTimeSetBtn
            // 
            resources.ApplyResources(this.RefreshTimeSetBtn, "RefreshTimeSetBtn");
            this.RefreshTimeSetBtn.Name = "RefreshTimeSetBtn";
            this.RefreshTimeSetBtn.UseVisualStyleBackColor = true;
            this.RefreshTimeSetBtn.Click += new System.EventHandler(this.RefreshTimeSetBtn_Click);
            // 
            // labVersion
            // 
            resources.ApplyResources(this.labVersion, "labVersion");
            this.labVersion.Name = "labVersion";
            // 
            // btnClearBuffer
            // 
            resources.ApplyResources(this.btnClearBuffer, "btnClearBuffer");
            this.btnClearBuffer.Name = "btnClearBuffer";
            this.btnClearBuffer.UseVisualStyleBackColor = true;
            this.btnClearBuffer.Click += new System.EventHandler(this.btnClearBuffer_Click);
            // 
            // btnReadBuffer
            // 
            resources.ApplyResources(this.btnReadBuffer, "btnReadBuffer");
            this.btnReadBuffer.Name = "btnReadBuffer";
            this.btnReadBuffer.UseVisualStyleBackColor = true;
            this.btnReadBuffer.Click += new System.EventHandler(this.btnReadBuffer_Click);
            // 
            // btnClear
            // 
            resources.ApplyResources(this.btnClear, "btnClear");
            this.btnClear.Name = "btnClear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnStopInv
            // 
            resources.ApplyResources(this.btnStopInv, "btnStopInv");
            this.btnStopInv.Name = "btnStopInv";
            this.btnStopInv.UseVisualStyleBackColor = true;
            this.btnStopInv.Click += new System.EventHandler(this.btnStopInv_Click);
            // 
            // btnStartInv
            // 
            resources.ApplyResources(this.btnStartInv, "btnStartInv");
            this.btnStartInv.Name = "btnStartInv";
            this.btnStartInv.UseVisualStyleBackColor = true;
            this.btnStartInv.Click += new System.EventHandler(this.btnStartInv_Click);
            // 
            // btnInvOnce
            // 
            resources.ApplyResources(this.btnInvOnce, "btnInvOnce");
            this.btnInvOnce.Name = "btnInvOnce";
            this.btnInvOnce.UseVisualStyleBackColor = true;
            this.btnInvOnce.Click += new System.EventHandler(this.btnInvOnce_Click);
            // 
            // KillPWDTB
            // 
            this.KillPWDTB.BackColor = System.Drawing.Color.White;
            this.KillPWDTB.Controls.Add(this.gbIOOpr);
            this.KillPWDTB.Controls.Add(this.labResult);
            this.KillPWDTB.Controls.Add(this.gbLockTag);
            this.KillPWDTB.Controls.Add(this.gbRWData);
            resources.ApplyResources(this.KillPWDTB, "KillPWDTB");
            this.KillPWDTB.Name = "KillPWDTB";
            // 
            // gbIOOpr
            // 
            this.gbIOOpr.Controls.Add(this.btnSetOutPort);
            this.gbIOOpr.Controls.Add(this.btnGetDI);
            this.gbIOOpr.Controls.Add(this.cbOut2);
            this.gbIOOpr.Controls.Add(this.cbOut1);
            this.gbIOOpr.Controls.Add(this.cbIn2);
            this.gbIOOpr.Controls.Add(this.cbIn1);
            resources.ApplyResources(this.gbIOOpr, "gbIOOpr");
            this.gbIOOpr.Name = "gbIOOpr";
            this.gbIOOpr.TabStop = false;
            // 
            // btnSetOutPort
            // 
            resources.ApplyResources(this.btnSetOutPort, "btnSetOutPort");
            this.btnSetOutPort.Name = "btnSetOutPort";
            this.btnSetOutPort.UseVisualStyleBackColor = true;
            this.btnSetOutPort.Click += new System.EventHandler(this.btnSetOutPort_Click);
            // 
            // btnGetDI
            // 
            resources.ApplyResources(this.btnGetDI, "btnGetDI");
            this.btnGetDI.Name = "btnGetDI";
            this.btnGetDI.UseVisualStyleBackColor = true;
            this.btnGetDI.Click += new System.EventHandler(this.btnGetDI_Click);
            // 
            // cbOut2
            // 
            resources.ApplyResources(this.cbOut2, "cbOut2");
            this.cbOut2.Name = "cbOut2";
            this.cbOut2.UseVisualStyleBackColor = true;
            // 
            // cbOut1
            // 
            resources.ApplyResources(this.cbOut1, "cbOut1");
            this.cbOut1.Name = "cbOut1";
            this.cbOut1.UseVisualStyleBackColor = true;
            // 
            // cbIn2
            // 
            resources.ApplyResources(this.cbIn2, "cbIn2");
            this.cbIn2.Name = "cbIn2";
            this.cbIn2.UseVisualStyleBackColor = true;
            // 
            // cbIn1
            // 
            resources.ApplyResources(this.cbIn1, "cbIn1");
            this.cbIn1.Name = "cbIn1";
            this.cbIn1.UseVisualStyleBackColor = true;
            // 
            // labResult
            // 
            resources.ApplyResources(this.labResult, "labResult");
            this.labResult.Name = "labResult";
            // 
            // gbLockTag
            // 
            this.gbLockTag.Controls.Add(this.label15);
            this.gbLockTag.Controls.Add(this.KillTagPWDTB);
            this.gbLockTag.Controls.Add(this.label14);
            this.gbLockTag.Controls.Add(this.btnLockTag);
            this.gbLockTag.Controls.Add(this.labLockType);
            this.gbLockTag.Controls.Add(this.labLockBank);
            this.gbLockTag.Controls.Add(this.labLockAccessPwd);
            this.gbLockTag.Controls.Add(this.tbLockAccessPwd);
            this.gbLockTag.Controls.Add(this.cbbLockType);
            this.gbLockTag.Controls.Add(this.cbbLockBank);
            resources.ApplyResources(this.gbLockTag, "gbLockTag");
            this.gbLockTag.Name = "gbLockTag";
            this.gbLockTag.TabStop = false;
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // KillTagPWDTB
            // 
            resources.ApplyResources(this.KillTagPWDTB, "KillTagPWDTB");
            this.KillTagPWDTB.Name = "KillTagPWDTB";
            this.KillTagPWDTB.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // btnLockTag
            // 
            resources.ApplyResources(this.btnLockTag, "btnLockTag");
            this.btnLockTag.Name = "btnLockTag";
            this.btnLockTag.UseVisualStyleBackColor = true;
            this.btnLockTag.Click += new System.EventHandler(this.btnLockTag_Click);
            // 
            // labLockType
            // 
            resources.ApplyResources(this.labLockType, "labLockType");
            this.labLockType.Name = "labLockType";
            // 
            // labLockBank
            // 
            resources.ApplyResources(this.labLockBank, "labLockBank");
            this.labLockBank.Name = "labLockBank";
            // 
            // labLockAccessPwd
            // 
            resources.ApplyResources(this.labLockAccessPwd, "labLockAccessPwd");
            this.labLockAccessPwd.Name = "labLockAccessPwd";
            // 
            // tbLockAccessPwd
            // 
            resources.ApplyResources(this.tbLockAccessPwd, "tbLockAccessPwd");
            this.tbLockAccessPwd.Name = "tbLockAccessPwd";
            // 
            // cbbLockType
            // 
            this.cbbLockType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbLockType.FormattingEnabled = true;
            resources.ApplyResources(this.cbbLockType, "cbbLockType");
            this.cbbLockType.Name = "cbbLockType";
            // 
            // cbbLockBank
            // 
            this.cbbLockBank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbLockBank.FormattingEnabled = true;
            resources.ApplyResources(this.cbbLockBank, "cbbLockBank");
            this.cbbLockBank.Name = "cbbLockBank";
            // 
            // gbRWData
            // 
            this.gbRWData.Controls.Add(this.labRWAccessPwd);
            this.gbRWData.Controls.Add(this.labData);
            this.gbRWData.Controls.Add(this.labLength);
            this.gbRWData.Controls.Add(this.labStartAdd);
            this.gbRWData.Controls.Add(this.labOprBank);
            this.gbRWData.Controls.Add(this.tbRWAccessPwd);
            this.gbRWData.Controls.Add(this.btnWriteData);
            this.gbRWData.Controls.Add(this.btnClearData);
            this.gbRWData.Controls.Add(this.btnReadData);
            this.gbRWData.Controls.Add(this.tbRWData);
            this.gbRWData.Controls.Add(this.cbbLength);
            this.gbRWData.Controls.Add(this.cbbStartAdd);
            this.gbRWData.Controls.Add(this.cbbRWBank);
            resources.ApplyResources(this.gbRWData, "gbRWData");
            this.gbRWData.Name = "gbRWData";
            this.gbRWData.TabStop = false;
            // 
            // labRWAccessPwd
            // 
            resources.ApplyResources(this.labRWAccessPwd, "labRWAccessPwd");
            this.labRWAccessPwd.Name = "labRWAccessPwd";
            // 
            // labData
            // 
            resources.ApplyResources(this.labData, "labData");
            this.labData.Name = "labData";
            // 
            // labLength
            // 
            resources.ApplyResources(this.labLength, "labLength");
            this.labLength.Name = "labLength";
            // 
            // labStartAdd
            // 
            resources.ApplyResources(this.labStartAdd, "labStartAdd");
            this.labStartAdd.Name = "labStartAdd";
            // 
            // labOprBank
            // 
            resources.ApplyResources(this.labOprBank, "labOprBank");
            this.labOprBank.Name = "labOprBank";
            // 
            // tbRWAccessPwd
            // 
            resources.ApplyResources(this.tbRWAccessPwd, "tbRWAccessPwd");
            this.tbRWAccessPwd.Name = "tbRWAccessPwd";
            // 
            // btnWriteData
            // 
            resources.ApplyResources(this.btnWriteData, "btnWriteData");
            this.btnWriteData.Name = "btnWriteData";
            this.btnWriteData.UseVisualStyleBackColor = true;
            this.btnWriteData.Click += new System.EventHandler(this.btnWriteData_Click);
            // 
            // btnClearData
            // 
            resources.ApplyResources(this.btnClearData, "btnClearData");
            this.btnClearData.Name = "btnClearData";
            this.btnClearData.UseVisualStyleBackColor = true;
            this.btnClearData.Click += new System.EventHandler(this.btnClearData_Click);
            // 
            // btnReadData
            // 
            resources.ApplyResources(this.btnReadData, "btnReadData");
            this.btnReadData.Name = "btnReadData";
            this.btnReadData.UseVisualStyleBackColor = true;
            this.btnReadData.Click += new System.EventHandler(this.btnReadData_Click);
            // 
            // tbRWData
            // 
            resources.ApplyResources(this.tbRWData, "tbRWData");
            this.tbRWData.Name = "tbRWData";
            // 
            // cbbLength
            // 
            this.cbbLength.FormattingEnabled = true;
            resources.ApplyResources(this.cbbLength, "cbbLength");
            this.cbbLength.Name = "cbbLength";
            // 
            // cbbStartAdd
            // 
            this.cbbStartAdd.FormattingEnabled = true;
            resources.ApplyResources(this.cbbStartAdd, "cbbStartAdd");
            this.cbbStartAdd.Name = "cbbStartAdd";
            this.cbbStartAdd.SelectedIndexChanged += new System.EventHandler(this.cbbStartAdd_SelectedIndexChanged);
            // 
            // cbbRWBank
            // 
            this.cbbRWBank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbRWBank.FormattingEnabled = true;
            resources.ApplyResources(this.cbbRWBank, "cbbRWBank");
            this.cbbRWBank.Name = "cbbRWBank";
            this.cbbRWBank.SelectedIndexChanged += new System.EventHandler(this.cbbRWBank_SelectedIndexChanged);
            // 
            // DevParams
            // 
            this.DevParams.BackColor = System.Drawing.Color.White;
            this.DevParams.Controls.Add(this.gbDevParams);
            this.DevParams.Controls.Add(this.labelResult);
            resources.ApplyResources(this.DevParams, "DevParams");
            this.DevParams.Name = "DevParams";
            // 
            // gbDevParams
            // 
            this.gbDevParams.BackColor = System.Drawing.Color.White;
            this.gbDevParams.Controls.Add(this.frequncyGB);
            this.gbDevParams.Controls.Add(this.groupBox7);
            resources.ApplyResources(this.gbDevParams, "gbDevParams");
            this.gbDevParams.Name = "gbDevParams";
            this.gbDevParams.TabStop = false;
            this.gbDevParams.Enter += new System.EventHandler(this.gbDevParams_Enter);
            // 
            // frequncyGB
            // 
            this.frequncyGB.Controls.Add(this.panel2);
            this.frequncyGB.Controls.Add(this.ConstantfrequencyTB);
            this.frequncyGB.Controls.Add(this.label4);
            this.frequncyGB.Controls.Add(this.LBFrequencypoint);
            this.frequncyGB.Controls.Add(this.label17);
            this.frequncyGB.Controls.Add(this.checkBox45);
            this.frequncyGB.Controls.Add(this.frequency50);
            this.frequncyGB.Controls.Add(this.frequency49);
            this.frequncyGB.Controls.Add(this.frequency48);
            this.frequncyGB.Controls.Add(this.frequency47);
            this.frequncyGB.Controls.Add(this.frequency46);
            this.frequncyGB.Controls.Add(this.frequency45);
            this.frequncyGB.Controls.Add(this.frequency44);
            this.frequncyGB.Controls.Add(this.frequency43);
            this.frequncyGB.Controls.Add(this.frequency42);
            this.frequncyGB.Controls.Add(this.frequency41);
            this.frequncyGB.Controls.Add(this.checkBox23);
            this.frequncyGB.Controls.Add(this.frequency40);
            this.frequncyGB.Controls.Add(this.frequency39);
            this.frequncyGB.Controls.Add(this.frequency38);
            this.frequncyGB.Controls.Add(this.frequency37);
            this.frequncyGB.Controls.Add(this.frequency36);
            this.frequncyGB.Controls.Add(this.frequency35);
            this.frequncyGB.Controls.Add(this.frequency34);
            this.frequncyGB.Controls.Add(this.frequency33);
            this.frequncyGB.Controls.Add(this.frequency32);
            this.frequncyGB.Controls.Add(this.frequency31);
            this.frequncyGB.Controls.Add(this.checkBox34);
            this.frequncyGB.Controls.Add(this.frequency30);
            this.frequncyGB.Controls.Add(this.frequency29);
            this.frequncyGB.Controls.Add(this.frequency28);
            this.frequncyGB.Controls.Add(this.frequency27);
            this.frequncyGB.Controls.Add(this.frequency26);
            this.frequncyGB.Controls.Add(this.frequency25);
            this.frequncyGB.Controls.Add(this.frequency24);
            this.frequncyGB.Controls.Add(this.frequency23);
            this.frequncyGB.Controls.Add(this.frequency22);
            this.frequncyGB.Controls.Add(this.frequency21);
            this.frequncyGB.Controls.Add(this.checkBox12);
            this.frequncyGB.Controls.Add(this.frequency20);
            this.frequncyGB.Controls.Add(this.frequency19);
            this.frequncyGB.Controls.Add(this.frequency18);
            this.frequncyGB.Controls.Add(this.frequency17);
            this.frequncyGB.Controls.Add(this.frequency16);
            this.frequncyGB.Controls.Add(this.frequency15);
            this.frequncyGB.Controls.Add(this.frequency14);
            this.frequncyGB.Controls.Add(this.frequency13);
            this.frequncyGB.Controls.Add(this.frequency12);
            this.frequncyGB.Controls.Add(this.frequency11);
            this.frequncyGB.Controls.Add(this.checkBox11);
            this.frequncyGB.Controls.Add(this.frequency10);
            this.frequncyGB.Controls.Add(this.frequency9);
            this.frequncyGB.Controls.Add(this.frequency8);
            this.frequncyGB.Controls.Add(this.frequency7);
            this.frequncyGB.Controls.Add(this.frequency6);
            this.frequncyGB.Controls.Add(this.frequency5);
            this.frequncyGB.Controls.Add(this.frequency4);
            this.frequncyGB.Controls.Add(this.frequency3);
            this.frequncyGB.Controls.Add(this.frequency2);
            this.frequncyGB.Controls.Add(this.BtnSetFrequency);
            this.frequncyGB.Controls.Add(this.BtnGetFrequency);
            this.frequncyGB.Controls.Add(this.frequency1);
            this.frequncyGB.Controls.Add(this.FrequencyAreaCB);
            this.frequncyGB.Controls.Add(this.LBFRequency);
            resources.ApplyResources(this.frequncyGB, "frequncyGB");
            this.frequncyGB.Name = "frequncyGB";
            this.frequncyGB.TabStop = false;
            this.frequncyGB.Enter += new System.EventHandler(this.frequncyGB_Enter);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button8);
            this.panel2.Controls.Add(this.button9);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // button8
            // 
            this.button8.BackgroundImage = global::UR2k_CS.Properties.Resources.down;
            resources.ApplyResources(this.button8, "button8");
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.Name = "button8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click_2);
            // 
            // button9
            // 
            this.button9.BackgroundImage = global::UR2k_CS.Properties.Resources.up;
            resources.ApplyResources(this.button9, "button9");
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.Name = "button9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click_2);
            // 
            // ConstantfrequencyTB
            // 
            this.ConstantfrequencyTB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ConstantfrequencyTB.FormattingEnabled = true;
            this.ConstantfrequencyTB.Items.AddRange(new object[] {
            resources.GetString("ConstantfrequencyTB.Items"),
            resources.GetString("ConstantfrequencyTB.Items1"),
            resources.GetString("ConstantfrequencyTB.Items2"),
            resources.GetString("ConstantfrequencyTB.Items3"),
            resources.GetString("ConstantfrequencyTB.Items4"),
            resources.GetString("ConstantfrequencyTB.Items5"),
            resources.GetString("ConstantfrequencyTB.Items6"),
            resources.GetString("ConstantfrequencyTB.Items7"),
            resources.GetString("ConstantfrequencyTB.Items8"),
            resources.GetString("ConstantfrequencyTB.Items9"),
            resources.GetString("ConstantfrequencyTB.Items10"),
            resources.GetString("ConstantfrequencyTB.Items11"),
            resources.GetString("ConstantfrequencyTB.Items12"),
            resources.GetString("ConstantfrequencyTB.Items13"),
            resources.GetString("ConstantfrequencyTB.Items14"),
            resources.GetString("ConstantfrequencyTB.Items15"),
            resources.GetString("ConstantfrequencyTB.Items16"),
            resources.GetString("ConstantfrequencyTB.Items17"),
            resources.GetString("ConstantfrequencyTB.Items18"),
            resources.GetString("ConstantfrequencyTB.Items19"),
            resources.GetString("ConstantfrequencyTB.Items20"),
            resources.GetString("ConstantfrequencyTB.Items21"),
            resources.GetString("ConstantfrequencyTB.Items22"),
            resources.GetString("ConstantfrequencyTB.Items23"),
            resources.GetString("ConstantfrequencyTB.Items24"),
            resources.GetString("ConstantfrequencyTB.Items25"),
            resources.GetString("ConstantfrequencyTB.Items26"),
            resources.GetString("ConstantfrequencyTB.Items27"),
            resources.GetString("ConstantfrequencyTB.Items28"),
            resources.GetString("ConstantfrequencyTB.Items29"),
            resources.GetString("ConstantfrequencyTB.Items30"),
            resources.GetString("ConstantfrequencyTB.Items31"),
            resources.GetString("ConstantfrequencyTB.Items32"),
            resources.GetString("ConstantfrequencyTB.Items33"),
            resources.GetString("ConstantfrequencyTB.Items34"),
            resources.GetString("ConstantfrequencyTB.Items35"),
            resources.GetString("ConstantfrequencyTB.Items36"),
            resources.GetString("ConstantfrequencyTB.Items37"),
            resources.GetString("ConstantfrequencyTB.Items38"),
            resources.GetString("ConstantfrequencyTB.Items39"),
            resources.GetString("ConstantfrequencyTB.Items40"),
            resources.GetString("ConstantfrequencyTB.Items41"),
            resources.GetString("ConstantfrequencyTB.Items42"),
            resources.GetString("ConstantfrequencyTB.Items43"),
            resources.GetString("ConstantfrequencyTB.Items44"),
            resources.GetString("ConstantfrequencyTB.Items45"),
            resources.GetString("ConstantfrequencyTB.Items46"),
            resources.GetString("ConstantfrequencyTB.Items47")});
            resources.ApplyResources(this.ConstantfrequencyTB, "ConstantfrequencyTB");
            this.ConstantfrequencyTB.Name = "ConstantfrequencyTB";
            this.ConstantfrequencyTB.TextUpdate += new System.EventHandler(this.ConstantfrequencyTB_TextUpdate);
            this.ConstantfrequencyTB.Click += new System.EventHandler(this.ConstantfrequencyTB_Click);
            this.ConstantfrequencyTB.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ConstantfrequencyTB_KeyUp);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // LBFrequencypoint
            // 
            resources.ApplyResources(this.LBFrequencypoint, "LBFrequencypoint");
            this.LBFrequencypoint.Name = "LBFrequencypoint";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // checkBox45
            // 
            resources.ApplyResources(this.checkBox45, "checkBox45");
            this.checkBox45.Name = "checkBox45";
            this.checkBox45.UseVisualStyleBackColor = true;
            this.checkBox45.CheckedChanged += new System.EventHandler(this.checkBox45_CheckedChanged);
            // 
            // frequency50
            // 
            resources.ApplyResources(this.frequency50, "frequency50");
            this.frequency50.Name = "frequency50";
            this.frequency50.UseVisualStyleBackColor = true;
            this.frequency50.CheckedChanged += new System.EventHandler(this.frequency50_CheckedChanged);
            // 
            // frequency49
            // 
            resources.ApplyResources(this.frequency49, "frequency49");
            this.frequency49.Name = "frequency49";
            this.frequency49.UseVisualStyleBackColor = true;
            // 
            // frequency48
            // 
            resources.ApplyResources(this.frequency48, "frequency48");
            this.frequency48.Name = "frequency48";
            this.frequency48.UseVisualStyleBackColor = true;
            // 
            // frequency47
            // 
            resources.ApplyResources(this.frequency47, "frequency47");
            this.frequency47.Name = "frequency47";
            this.frequency47.UseVisualStyleBackColor = true;
            // 
            // frequency46
            // 
            resources.ApplyResources(this.frequency46, "frequency46");
            this.frequency46.Name = "frequency46";
            this.frequency46.UseVisualStyleBackColor = true;
            // 
            // frequency45
            // 
            resources.ApplyResources(this.frequency45, "frequency45");
            this.frequency45.Name = "frequency45";
            this.frequency45.UseVisualStyleBackColor = true;
            // 
            // frequency44
            // 
            resources.ApplyResources(this.frequency44, "frequency44");
            this.frequency44.Name = "frequency44";
            this.frequency44.UseVisualStyleBackColor = true;
            // 
            // frequency43
            // 
            resources.ApplyResources(this.frequency43, "frequency43");
            this.frequency43.Name = "frequency43";
            this.frequency43.UseVisualStyleBackColor = true;
            // 
            // frequency42
            // 
            resources.ApplyResources(this.frequency42, "frequency42");
            this.frequency42.Name = "frequency42";
            this.frequency42.UseVisualStyleBackColor = true;
            // 
            // frequency41
            // 
            resources.ApplyResources(this.frequency41, "frequency41");
            this.frequency41.Name = "frequency41";
            this.frequency41.UseVisualStyleBackColor = true;
            // 
            // checkBox23
            // 
            resources.ApplyResources(this.checkBox23, "checkBox23");
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.UseVisualStyleBackColor = true;
            this.checkBox23.CheckedChanged += new System.EventHandler(this.checkBox23_CheckedChanged);
            // 
            // frequency40
            // 
            resources.ApplyResources(this.frequency40, "frequency40");
            this.frequency40.Name = "frequency40";
            this.frequency40.UseVisualStyleBackColor = true;
            // 
            // frequency39
            // 
            resources.ApplyResources(this.frequency39, "frequency39");
            this.frequency39.Name = "frequency39";
            this.frequency39.UseVisualStyleBackColor = true;
            // 
            // frequency38
            // 
            resources.ApplyResources(this.frequency38, "frequency38");
            this.frequency38.Name = "frequency38";
            this.frequency38.UseVisualStyleBackColor = true;
            // 
            // frequency37
            // 
            resources.ApplyResources(this.frequency37, "frequency37");
            this.frequency37.Name = "frequency37";
            this.frequency37.UseVisualStyleBackColor = true;
            // 
            // frequency36
            // 
            resources.ApplyResources(this.frequency36, "frequency36");
            this.frequency36.Name = "frequency36";
            this.frequency36.UseVisualStyleBackColor = true;
            // 
            // frequency35
            // 
            resources.ApplyResources(this.frequency35, "frequency35");
            this.frequency35.Name = "frequency35";
            this.frequency35.UseVisualStyleBackColor = true;
            // 
            // frequency34
            // 
            resources.ApplyResources(this.frequency34, "frequency34");
            this.frequency34.Name = "frequency34";
            this.frequency34.UseVisualStyleBackColor = true;
            // 
            // frequency33
            // 
            resources.ApplyResources(this.frequency33, "frequency33");
            this.frequency33.Name = "frequency33";
            this.frequency33.UseVisualStyleBackColor = true;
            // 
            // frequency32
            // 
            resources.ApplyResources(this.frequency32, "frequency32");
            this.frequency32.Name = "frequency32";
            this.frequency32.UseVisualStyleBackColor = true;
            // 
            // frequency31
            // 
            resources.ApplyResources(this.frequency31, "frequency31");
            this.frequency31.Name = "frequency31";
            this.frequency31.UseVisualStyleBackColor = true;
            // 
            // checkBox34
            // 
            resources.ApplyResources(this.checkBox34, "checkBox34");
            this.checkBox34.Name = "checkBox34";
            this.checkBox34.UseVisualStyleBackColor = true;
            this.checkBox34.CheckedChanged += new System.EventHandler(this.checkBox34_CheckedChanged);
            // 
            // frequency30
            // 
            resources.ApplyResources(this.frequency30, "frequency30");
            this.frequency30.Name = "frequency30";
            this.frequency30.UseVisualStyleBackColor = true;
            // 
            // frequency29
            // 
            resources.ApplyResources(this.frequency29, "frequency29");
            this.frequency29.Name = "frequency29";
            this.frequency29.UseVisualStyleBackColor = true;
            // 
            // frequency28
            // 
            resources.ApplyResources(this.frequency28, "frequency28");
            this.frequency28.Name = "frequency28";
            this.frequency28.UseVisualStyleBackColor = true;
            // 
            // frequency27
            // 
            resources.ApplyResources(this.frequency27, "frequency27");
            this.frequency27.Name = "frequency27";
            this.frequency27.UseVisualStyleBackColor = true;
            // 
            // frequency26
            // 
            resources.ApplyResources(this.frequency26, "frequency26");
            this.frequency26.Name = "frequency26";
            this.frequency26.UseVisualStyleBackColor = true;
            // 
            // frequency25
            // 
            resources.ApplyResources(this.frequency25, "frequency25");
            this.frequency25.Name = "frequency25";
            this.frequency25.UseVisualStyleBackColor = true;
            // 
            // frequency24
            // 
            resources.ApplyResources(this.frequency24, "frequency24");
            this.frequency24.Name = "frequency24";
            this.frequency24.UseVisualStyleBackColor = true;
            // 
            // frequency23
            // 
            resources.ApplyResources(this.frequency23, "frequency23");
            this.frequency23.Name = "frequency23";
            this.frequency23.UseVisualStyleBackColor = true;
            // 
            // frequency22
            // 
            resources.ApplyResources(this.frequency22, "frequency22");
            this.frequency22.Name = "frequency22";
            this.frequency22.UseVisualStyleBackColor = true;
            // 
            // frequency21
            // 
            resources.ApplyResources(this.frequency21, "frequency21");
            this.frequency21.Name = "frequency21";
            this.frequency21.UseVisualStyleBackColor = true;
            // 
            // checkBox12
            // 
            resources.ApplyResources(this.checkBox12, "checkBox12");
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.checkBox12_CheckedChanged);
            // 
            // frequency20
            // 
            resources.ApplyResources(this.frequency20, "frequency20");
            this.frequency20.Name = "frequency20";
            this.frequency20.UseVisualStyleBackColor = true;
            // 
            // frequency19
            // 
            resources.ApplyResources(this.frequency19, "frequency19");
            this.frequency19.Name = "frequency19";
            this.frequency19.UseVisualStyleBackColor = true;
            // 
            // frequency18
            // 
            resources.ApplyResources(this.frequency18, "frequency18");
            this.frequency18.Name = "frequency18";
            this.frequency18.UseVisualStyleBackColor = true;
            // 
            // frequency17
            // 
            resources.ApplyResources(this.frequency17, "frequency17");
            this.frequency17.Name = "frequency17";
            this.frequency17.UseVisualStyleBackColor = true;
            // 
            // frequency16
            // 
            resources.ApplyResources(this.frequency16, "frequency16");
            this.frequency16.Name = "frequency16";
            this.frequency16.UseVisualStyleBackColor = true;
            // 
            // frequency15
            // 
            resources.ApplyResources(this.frequency15, "frequency15");
            this.frequency15.Name = "frequency15";
            this.frequency15.UseVisualStyleBackColor = true;
            // 
            // frequency14
            // 
            resources.ApplyResources(this.frequency14, "frequency14");
            this.frequency14.Name = "frequency14";
            this.frequency14.UseVisualStyleBackColor = true;
            // 
            // frequency13
            // 
            resources.ApplyResources(this.frequency13, "frequency13");
            this.frequency13.Name = "frequency13";
            this.frequency13.UseVisualStyleBackColor = true;
            // 
            // frequency12
            // 
            resources.ApplyResources(this.frequency12, "frequency12");
            this.frequency12.Name = "frequency12";
            this.frequency12.UseVisualStyleBackColor = true;
            // 
            // frequency11
            // 
            resources.ApplyResources(this.frequency11, "frequency11");
            this.frequency11.Name = "frequency11";
            this.frequency11.UseVisualStyleBackColor = true;
            // 
            // checkBox11
            // 
            resources.ApplyResources(this.checkBox11, "checkBox11");
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.checkBox11_CheckedChanged);
            // 
            // frequency10
            // 
            resources.ApplyResources(this.frequency10, "frequency10");
            this.frequency10.Name = "frequency10";
            this.frequency10.UseVisualStyleBackColor = true;
            // 
            // frequency9
            // 
            resources.ApplyResources(this.frequency9, "frequency9");
            this.frequency9.Name = "frequency9";
            this.frequency9.UseVisualStyleBackColor = true;
            // 
            // frequency8
            // 
            resources.ApplyResources(this.frequency8, "frequency8");
            this.frequency8.Name = "frequency8";
            this.frequency8.UseVisualStyleBackColor = true;
            // 
            // frequency7
            // 
            resources.ApplyResources(this.frequency7, "frequency7");
            this.frequency7.Name = "frequency7";
            this.frequency7.UseVisualStyleBackColor = true;
            // 
            // frequency6
            // 
            resources.ApplyResources(this.frequency6, "frequency6");
            this.frequency6.Name = "frequency6";
            this.frequency6.UseVisualStyleBackColor = true;
            // 
            // frequency5
            // 
            resources.ApplyResources(this.frequency5, "frequency5");
            this.frequency5.Name = "frequency5";
            this.frequency5.UseVisualStyleBackColor = true;
            // 
            // frequency4
            // 
            resources.ApplyResources(this.frequency4, "frequency4");
            this.frequency4.Name = "frequency4";
            this.frequency4.UseVisualStyleBackColor = true;
            // 
            // frequency3
            // 
            resources.ApplyResources(this.frequency3, "frequency3");
            this.frequency3.Name = "frequency3";
            this.frequency3.UseVisualStyleBackColor = true;
            // 
            // frequency2
            // 
            resources.ApplyResources(this.frequency2, "frequency2");
            this.frequency2.Name = "frequency2";
            this.frequency2.UseVisualStyleBackColor = true;
            // 
            // BtnSetFrequency
            // 
            resources.ApplyResources(this.BtnSetFrequency, "BtnSetFrequency");
            this.BtnSetFrequency.Name = "BtnSetFrequency";
            this.BtnSetFrequency.UseVisualStyleBackColor = true;
            this.BtnSetFrequency.Click += new System.EventHandler(this.button8_Click);
            // 
            // BtnGetFrequency
            // 
            resources.ApplyResources(this.BtnGetFrequency, "BtnGetFrequency");
            this.BtnGetFrequency.Name = "BtnGetFrequency";
            this.BtnGetFrequency.UseVisualStyleBackColor = true;
            this.BtnGetFrequency.Click += new System.EventHandler(this.button7_Click);
            // 
            // frequency1
            // 
            resources.ApplyResources(this.frequency1, "frequency1");
            this.frequency1.Name = "frequency1";
            this.frequency1.UseVisualStyleBackColor = true;
            // 
            // FrequencyAreaCB
            // 
            this.FrequencyAreaCB.FormattingEnabled = true;
            this.FrequencyAreaCB.Items.AddRange(new object[] {
            resources.GetString("FrequencyAreaCB.Items"),
            resources.GetString("FrequencyAreaCB.Items1"),
            resources.GetString("FrequencyAreaCB.Items2"),
            resources.GetString("FrequencyAreaCB.Items3"),
            resources.GetString("FrequencyAreaCB.Items4"),
            resources.GetString("FrequencyAreaCB.Items5")});
            resources.ApplyResources(this.FrequencyAreaCB, "FrequencyAreaCB");
            this.FrequencyAreaCB.Name = "FrequencyAreaCB";
            this.FrequencyAreaCB.SelectedIndexChanged += new System.EventHandler(this.FrequencyAreaCB_SelectedIndexChanged);
            // 
            // LBFRequency
            // 
            resources.ApplyResources(this.LBFRequency, "LBFRequency");
            this.LBFRequency.Name = "LBFRequency";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.factoryBtn);
            this.groupBox7.Controls.Add(this.LoginBtn);
            this.groupBox7.Controls.Add(this.SpecialFunTabTB);
            this.groupBox7.Controls.Add(this.SpecalFunLB);
            this.groupBox7.Controls.Add(this.groupBox1);
            this.groupBox7.Controls.Add(this.WorkModeGB);
            this.groupBox7.Controls.Add(this.labDevNo);
            this.groupBox7.Controls.Add(this.tbDevNo);
            this.groupBox7.Controls.Add(this.btnSetDevNo);
            this.groupBox7.Controls.Add(this.btnGetDevNo);
            this.groupBox7.Controls.Add(this.labNeightJudge);
            this.groupBox7.Controls.Add(this.tbNeighJudge);
            this.groupBox7.Controls.Add(this.btnSetNeighJudge);
            this.groupBox7.Controls.Add(this.btnGetNeighJudge);
            this.groupBox7.Controls.Add(this.label3);
            this.groupBox7.Controls.Add(this.rdoBuzzerOn);
            this.groupBox7.Controls.Add(this.rdoBuzzerOff);
            this.groupBox7.Controls.Add(this.btnSetBuzzer);
            this.groupBox7.Controls.Add(this.btnGetBuzzer);
            resources.ApplyResources(this.groupBox7, "groupBox7");
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.TabStop = false;
            this.groupBox7.Enter += new System.EventHandler(this.groupBox7_Enter);
            // 
            // factoryBtn
            // 
            resources.ApplyResources(this.factoryBtn, "factoryBtn");
            this.factoryBtn.Name = "factoryBtn";
            this.factoryBtn.UseVisualStyleBackColor = true;
            this.factoryBtn.Click += new System.EventHandler(this.button12_Click);
            // 
            // LoginBtn
            // 
            resources.ApplyResources(this.LoginBtn, "LoginBtn");
            this.LoginBtn.Name = "LoginBtn";
            this.LoginBtn.UseVisualStyleBackColor = true;
            this.LoginBtn.Click += new System.EventHandler(this.button7_Click_1);
            // 
            // SpecialFunTabTB
            // 
            resources.ApplyResources(this.SpecialFunTabTB, "SpecialFunTabTB");
            this.SpecialFunTabTB.Name = "SpecialFunTabTB";
            // 
            // SpecalFunLB
            // 
            resources.ApplyResources(this.SpecalFunLB, "SpecalFunLB");
            this.SpecalFunLB.Name = "SpecalFunLB";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.Rb4Channel);
            this.groupBox1.Controls.Add(this.GetAntStateBt);
            this.groupBox1.Controls.Add(this.btnSetAnts);
            this.groupBox1.Controls.Add(this.rb32Ant);
            this.groupBox1.Controls.Add(this.rb16Ant);
            this.groupBox1.Controls.Add(this.MultiAntPowerTB);
            this.groupBox1.Controls.Add(this.comboBoxWT4);
            this.groupBox1.Controls.Add(this.MultiAntTime);
            this.groupBox1.Controls.Add(this.comboBoxPower4);
            this.groupBox1.Controls.Add(this.MultiAntPower);
            this.groupBox1.Controls.Add(this.comboBoxWT3);
            this.groupBox1.Controls.Add(this.MultiAntWorktime);
            this.groupBox1.Controls.Add(this.comboBoxPower3);
            this.groupBox1.Controls.Add(this.checkBox89);
            this.groupBox1.Controls.Add(this.comboBoxWT2);
            this.groupBox1.Controls.Add(this.checkBox90);
            this.groupBox1.Controls.Add(this.comboBoxPower2);
            this.groupBox1.Controls.Add(this.checkBox91);
            this.groupBox1.Controls.Add(this.comboBoxWT1);
            this.groupBox1.Controls.Add(this.checkBox95);
            this.groupBox1.Controls.Add(this.comboBoxPower1);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.cbAnt4);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.cbAnt3);
            this.groupBox1.Controls.Add(this.Ant32);
            this.groupBox1.Controls.Add(this.cbAnt2);
            this.groupBox1.Controls.Add(this.Ant31);
            this.groupBox1.Controls.Add(this.cbAnt1);
            this.groupBox1.Controls.Add(this.Ant30);
            this.groupBox1.Controls.Add(this.Ant29);
            this.groupBox1.Controls.Add(this.Ant28);
            this.groupBox1.Controls.Add(this.Ant27);
            this.groupBox1.Controls.Add(this.Ant26);
            this.groupBox1.Controls.Add(this.Ant25);
            this.groupBox1.Controls.Add(this.Ant24);
            this.groupBox1.Controls.Add(this.Ant23);
            this.groupBox1.Controls.Add(this.Ant22);
            this.groupBox1.Controls.Add(this.Ant21);
            this.groupBox1.Controls.Add(this.Ant20);
            this.groupBox1.Controls.Add(this.Ant19);
            this.groupBox1.Controls.Add(this.Ant18);
            this.groupBox1.Controls.Add(this.Ant17);
            this.groupBox1.Controls.Add(this.Ant16);
            this.groupBox1.Controls.Add(this.Ant15);
            this.groupBox1.Controls.Add(this.Ant14);
            this.groupBox1.Controls.Add(this.Ant13);
            this.groupBox1.Controls.Add(this.Ant12);
            this.groupBox1.Controls.Add(this.Ant11);
            this.groupBox1.Controls.Add(this.Ant10);
            this.groupBox1.Controls.Add(this.Ant09);
            this.groupBox1.Controls.Add(this.Ant08);
            this.groupBox1.Controls.Add(this.Ant07);
            this.groupBox1.Controls.Add(this.Ant06);
            this.groupBox1.Controls.Add(this.Ant05);
            this.groupBox1.Controls.Add(this.Ant04);
            this.groupBox1.Controls.Add(this.Ant03);
            this.groupBox1.Controls.Add(this.Ant02);
            this.groupBox1.Controls.Add(this.Ant01);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label27
            // 
            resources.ApplyResources(this.label27, "label27");
            this.label27.Name = "label27";
            // 
            // Rb4Channel
            // 
            resources.ApplyResources(this.Rb4Channel, "Rb4Channel");
            this.Rb4Channel.Name = "Rb4Channel";
            this.Rb4Channel.TabStop = true;
            this.Rb4Channel.UseVisualStyleBackColor = true;
            this.Rb4Channel.CheckedChanged += new System.EventHandler(this.Rb4Channel_CheckedChanged);
            // 
            // GetAntStateBt
            // 
            resources.ApplyResources(this.GetAntStateBt, "GetAntStateBt");
            this.GetAntStateBt.Name = "GetAntStateBt";
            this.GetAntStateBt.UseVisualStyleBackColor = true;
            this.GetAntStateBt.Click += new System.EventHandler(this.GetAntStateBt_Click);
            // 
            // btnSetAnts
            // 
            resources.ApplyResources(this.btnSetAnts, "btnSetAnts");
            this.btnSetAnts.Name = "btnSetAnts";
            this.btnSetAnts.UseVisualStyleBackColor = true;
            this.btnSetAnts.Click += new System.EventHandler(this.btnSetAnts_Click);
            // 
            // rb32Ant
            // 
            resources.ApplyResources(this.rb32Ant, "rb32Ant");
            this.rb32Ant.Checked = true;
            this.rb32Ant.Name = "rb32Ant";
            this.rb32Ant.TabStop = true;
            this.rb32Ant.UseVisualStyleBackColor = true;
            this.rb32Ant.CheckedChanged += new System.EventHandler(this.rb32Ant_CheckedChanged);
            // 
            // rb16Ant
            // 
            resources.ApplyResources(this.rb16Ant, "rb16Ant");
            this.rb16Ant.Name = "rb16Ant";
            this.rb16Ant.UseVisualStyleBackColor = true;
            this.rb16Ant.CheckedChanged += new System.EventHandler(this.rb16Ant_CheckedChanged);
            // 
            // MultiAntPowerTB
            // 
            this.MultiAntPowerTB.FormattingEnabled = true;
            resources.ApplyResources(this.MultiAntPowerTB, "MultiAntPowerTB");
            this.MultiAntPowerTB.Name = "MultiAntPowerTB";
            // 
            // comboBoxWT4
            // 
            this.comboBoxWT4.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxWT4, "comboBoxWT4");
            this.comboBoxWT4.Name = "comboBoxWT4";
            // 
            // MultiAntTime
            // 
            this.MultiAntTime.FormattingEnabled = true;
            resources.ApplyResources(this.MultiAntTime, "MultiAntTime");
            this.MultiAntTime.Name = "MultiAntTime";
            // 
            // comboBoxPower4
            // 
            this.comboBoxPower4.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxPower4, "comboBoxPower4");
            this.comboBoxPower4.Name = "comboBoxPower4";
            // 
            // MultiAntPower
            // 
            resources.ApplyResources(this.MultiAntPower, "MultiAntPower");
            this.MultiAntPower.Name = "MultiAntPower";
            // 
            // comboBoxWT3
            // 
            this.comboBoxWT3.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxWT3, "comboBoxWT3");
            this.comboBoxWT3.Name = "comboBoxWT3";
            // 
            // MultiAntWorktime
            // 
            resources.ApplyResources(this.MultiAntWorktime, "MultiAntWorktime");
            this.MultiAntWorktime.Name = "MultiAntWorktime";
            // 
            // comboBoxPower3
            // 
            this.comboBoxPower3.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxPower3, "comboBoxPower3");
            this.comboBoxPower3.Name = "comboBoxPower3";
            // 
            // checkBox89
            // 
            resources.ApplyResources(this.checkBox89, "checkBox89");
            this.checkBox89.Name = "checkBox89";
            this.checkBox89.UseVisualStyleBackColor = true;
            this.checkBox89.CheckedChanged += new System.EventHandler(this.checkBox89_CheckedChanged);
            // 
            // comboBoxWT2
            // 
            this.comboBoxWT2.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxWT2, "comboBoxWT2");
            this.comboBoxWT2.Name = "comboBoxWT2";
            // 
            // checkBox90
            // 
            resources.ApplyResources(this.checkBox90, "checkBox90");
            this.checkBox90.Name = "checkBox90";
            this.checkBox90.UseVisualStyleBackColor = true;
            this.checkBox90.CheckedChanged += new System.EventHandler(this.checkBox90_CheckedChanged);
            // 
            // comboBoxPower2
            // 
            this.comboBoxPower2.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxPower2, "comboBoxPower2");
            this.comboBoxPower2.Name = "comboBoxPower2";
            // 
            // checkBox91
            // 
            resources.ApplyResources(this.checkBox91, "checkBox91");
            this.checkBox91.Name = "checkBox91";
            this.checkBox91.UseVisualStyleBackColor = true;
            this.checkBox91.CheckedChanged += new System.EventHandler(this.checkBox91_CheckedChanged);
            // 
            // comboBoxWT1
            // 
            this.comboBoxWT1.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxWT1, "comboBoxWT1");
            this.comboBoxWT1.Name = "comboBoxWT1";
            // 
            // checkBox95
            // 
            resources.ApplyResources(this.checkBox95, "checkBox95");
            this.checkBox95.Name = "checkBox95";
            this.checkBox95.UseVisualStyleBackColor = true;
            this.checkBox95.CheckedChanged += new System.EventHandler(this.checkBox95_CheckedChanged);
            // 
            // comboBoxPower1
            // 
            this.comboBoxPower1.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxPower1, "comboBoxPower1");
            this.comboBoxPower1.Name = "comboBoxPower1";
            // 
            // button6
            // 
            resources.ApplyResources(this.button6, "button6");
            this.button6.Name = "button6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // cbAnt4
            // 
            resources.ApplyResources(this.cbAnt4, "cbAnt4");
            this.cbAnt4.Name = "cbAnt4";
            this.cbAnt4.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            resources.ApplyResources(this.button4, "button4");
            this.button4.Name = "button4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_2);
            // 
            // cbAnt3
            // 
            resources.ApplyResources(this.cbAnt3, "cbAnt3");
            this.cbAnt3.Name = "cbAnt3";
            this.cbAnt3.UseVisualStyleBackColor = true;
            // 
            // Ant32
            // 
            resources.ApplyResources(this.Ant32, "Ant32");
            this.Ant32.Name = "Ant32";
            this.Ant32.UseVisualStyleBackColor = true;
            // 
            // cbAnt2
            // 
            resources.ApplyResources(this.cbAnt2, "cbAnt2");
            this.cbAnt2.Name = "cbAnt2";
            this.cbAnt2.UseVisualStyleBackColor = true;
            // 
            // Ant31
            // 
            resources.ApplyResources(this.Ant31, "Ant31");
            this.Ant31.Name = "Ant31";
            this.Ant31.UseVisualStyleBackColor = true;
            // 
            // cbAnt1
            // 
            resources.ApplyResources(this.cbAnt1, "cbAnt1");
            this.cbAnt1.Name = "cbAnt1";
            this.cbAnt1.UseVisualStyleBackColor = true;
            // 
            // Ant30
            // 
            resources.ApplyResources(this.Ant30, "Ant30");
            this.Ant30.Name = "Ant30";
            this.Ant30.UseVisualStyleBackColor = true;
            // 
            // Ant29
            // 
            resources.ApplyResources(this.Ant29, "Ant29");
            this.Ant29.Name = "Ant29";
            this.Ant29.UseVisualStyleBackColor = true;
            // 
            // Ant28
            // 
            resources.ApplyResources(this.Ant28, "Ant28");
            this.Ant28.Name = "Ant28";
            this.Ant28.UseVisualStyleBackColor = true;
            // 
            // Ant27
            // 
            resources.ApplyResources(this.Ant27, "Ant27");
            this.Ant27.Name = "Ant27";
            this.Ant27.UseVisualStyleBackColor = true;
            // 
            // Ant26
            // 
            resources.ApplyResources(this.Ant26, "Ant26");
            this.Ant26.Name = "Ant26";
            this.Ant26.UseVisualStyleBackColor = true;
            // 
            // Ant25
            // 
            resources.ApplyResources(this.Ant25, "Ant25");
            this.Ant25.Name = "Ant25";
            this.Ant25.UseVisualStyleBackColor = true;
            // 
            // Ant24
            // 
            resources.ApplyResources(this.Ant24, "Ant24");
            this.Ant24.Name = "Ant24";
            this.Ant24.UseVisualStyleBackColor = true;
            // 
            // Ant23
            // 
            resources.ApplyResources(this.Ant23, "Ant23");
            this.Ant23.Name = "Ant23";
            this.Ant23.UseVisualStyleBackColor = true;
            // 
            // Ant22
            // 
            resources.ApplyResources(this.Ant22, "Ant22");
            this.Ant22.Name = "Ant22";
            this.Ant22.UseVisualStyleBackColor = true;
            // 
            // Ant21
            // 
            resources.ApplyResources(this.Ant21, "Ant21");
            this.Ant21.Name = "Ant21";
            this.Ant21.UseVisualStyleBackColor = true;
            // 
            // Ant20
            // 
            resources.ApplyResources(this.Ant20, "Ant20");
            this.Ant20.Name = "Ant20";
            this.Ant20.UseVisualStyleBackColor = true;
            // 
            // Ant19
            // 
            resources.ApplyResources(this.Ant19, "Ant19");
            this.Ant19.Name = "Ant19";
            this.Ant19.UseVisualStyleBackColor = true;
            // 
            // Ant18
            // 
            resources.ApplyResources(this.Ant18, "Ant18");
            this.Ant18.Name = "Ant18";
            this.Ant18.UseVisualStyleBackColor = true;
            // 
            // Ant17
            // 
            resources.ApplyResources(this.Ant17, "Ant17");
            this.Ant17.Name = "Ant17";
            this.Ant17.UseVisualStyleBackColor = true;
            // 
            // Ant16
            // 
            resources.ApplyResources(this.Ant16, "Ant16");
            this.Ant16.Name = "Ant16";
            this.Ant16.UseVisualStyleBackColor = true;
            // 
            // Ant15
            // 
            resources.ApplyResources(this.Ant15, "Ant15");
            this.Ant15.Name = "Ant15";
            this.Ant15.UseVisualStyleBackColor = true;
            // 
            // Ant14
            // 
            resources.ApplyResources(this.Ant14, "Ant14");
            this.Ant14.Name = "Ant14";
            this.Ant14.UseVisualStyleBackColor = true;
            // 
            // Ant13
            // 
            resources.ApplyResources(this.Ant13, "Ant13");
            this.Ant13.Name = "Ant13";
            this.Ant13.UseVisualStyleBackColor = true;
            // 
            // Ant12
            // 
            resources.ApplyResources(this.Ant12, "Ant12");
            this.Ant12.Name = "Ant12";
            this.Ant12.UseVisualStyleBackColor = true;
            // 
            // Ant11
            // 
            resources.ApplyResources(this.Ant11, "Ant11");
            this.Ant11.Name = "Ant11";
            this.Ant11.UseVisualStyleBackColor = true;
            // 
            // Ant10
            // 
            resources.ApplyResources(this.Ant10, "Ant10");
            this.Ant10.Name = "Ant10";
            this.Ant10.UseVisualStyleBackColor = true;
            // 
            // Ant09
            // 
            resources.ApplyResources(this.Ant09, "Ant09");
            this.Ant09.Name = "Ant09";
            this.Ant09.UseVisualStyleBackColor = true;
            // 
            // Ant08
            // 
            resources.ApplyResources(this.Ant08, "Ant08");
            this.Ant08.Name = "Ant08";
            this.Ant08.UseVisualStyleBackColor = true;
            this.Ant08.CheckedChanged += new System.EventHandler(this.checkBox60_CheckedChanged);
            // 
            // Ant07
            // 
            resources.ApplyResources(this.Ant07, "Ant07");
            this.Ant07.Name = "Ant07";
            this.Ant07.UseVisualStyleBackColor = true;
            // 
            // Ant06
            // 
            resources.ApplyResources(this.Ant06, "Ant06");
            this.Ant06.Name = "Ant06";
            this.Ant06.UseVisualStyleBackColor = true;
            // 
            // Ant05
            // 
            resources.ApplyResources(this.Ant05, "Ant05");
            this.Ant05.Name = "Ant05";
            this.Ant05.UseVisualStyleBackColor = true;
            // 
            // Ant04
            // 
            resources.ApplyResources(this.Ant04, "Ant04");
            this.Ant04.Name = "Ant04";
            this.Ant04.UseVisualStyleBackColor = true;
            // 
            // Ant03
            // 
            resources.ApplyResources(this.Ant03, "Ant03");
            this.Ant03.Name = "Ant03";
            this.Ant03.UseVisualStyleBackColor = true;
            // 
            // Ant02
            // 
            resources.ApplyResources(this.Ant02, "Ant02");
            this.Ant02.Name = "Ant02";
            this.Ant02.UseVisualStyleBackColor = true;
            // 
            // Ant01
            // 
            resources.ApplyResources(this.Ant01, "Ant01");
            this.Ant01.Name = "Ant01";
            this.Ant01.UseVisualStyleBackColor = true;
            // 
            // WorkModeGB
            // 
            this.WorkModeGB.Controls.Add(this.label1);
            this.WorkModeGB.Controls.Add(this.label20);
            this.WorkModeGB.Controls.Add(this.TriggerAlarmBtn);
            this.WorkModeGB.Controls.Add(this.TriggerAlarmTimeTB);
            this.WorkModeGB.Controls.Add(this.TriggerDelay);
            this.WorkModeGB.Controls.Add(this.DataChannelSetBtn);
            this.WorkModeGB.Controls.Add(this.DataChannelCB);
            this.WorkModeGB.Controls.Add(this.DataTransMode);
            this.WorkModeGB.Controls.Add(this.TriggerDelayLB);
            this.WorkModeGB.Controls.Add(this.tbDelayTime);
            this.WorkModeGB.Controls.Add(this.btnSetDelayTime);
            this.WorkModeGB.Controls.Add(this.btnGetDelayTime);
            this.WorkModeGB.Controls.Add(this.WorkModeLB);
            this.WorkModeGB.Controls.Add(this.comboBoxWorkMode);
            this.WorkModeGB.Controls.Add(this.btnSetWorkMode);
            this.WorkModeGB.Controls.Add(this.btnGetWorkMode);
            resources.ApplyResources(this.WorkModeGB, "WorkModeGB");
            this.WorkModeGB.Name = "WorkModeGB";
            this.WorkModeGB.TabStop = false;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.Name = "label20";
            // 
            // TriggerAlarmBtn
            // 
            resources.ApplyResources(this.TriggerAlarmBtn, "TriggerAlarmBtn");
            this.TriggerAlarmBtn.Name = "TriggerAlarmBtn";
            this.TriggerAlarmBtn.UseVisualStyleBackColor = true;
            this.TriggerAlarmBtn.Click += new System.EventHandler(this.TriggerAlarmBtn_Click);
            // 
            // TriggerAlarmTimeTB
            // 
            resources.ApplyResources(this.TriggerAlarmTimeTB, "TriggerAlarmTimeTB");
            this.TriggerAlarmTimeTB.Name = "TriggerAlarmTimeTB";
            // 
            // TriggerDelay
            // 
            resources.ApplyResources(this.TriggerDelay, "TriggerDelay");
            this.TriggerDelay.Name = "TriggerDelay";
            // 
            // DataChannelSetBtn
            // 
            resources.ApplyResources(this.DataChannelSetBtn, "DataChannelSetBtn");
            this.DataChannelSetBtn.Name = "DataChannelSetBtn";
            this.DataChannelSetBtn.UseVisualStyleBackColor = true;
            this.DataChannelSetBtn.Click += new System.EventHandler(this.button9_Click);
            // 
            // DataChannelCB
            // 
            this.DataChannelCB.FormattingEnabled = true;
            this.DataChannelCB.Items.AddRange(new object[] {
            resources.GetString("DataChannelCB.Items"),
            resources.GetString("DataChannelCB.Items1"),
            resources.GetString("DataChannelCB.Items2"),
            resources.GetString("DataChannelCB.Items3")});
            resources.ApplyResources(this.DataChannelCB, "DataChannelCB");
            this.DataChannelCB.Name = "DataChannelCB";
            // 
            // DataTransMode
            // 
            resources.ApplyResources(this.DataTransMode, "DataTransMode");
            this.DataTransMode.Name = "DataTransMode";
            // 
            // TriggerDelayLB
            // 
            resources.ApplyResources(this.TriggerDelayLB, "TriggerDelayLB");
            this.TriggerDelayLB.Name = "TriggerDelayLB";
            // 
            // tbDelayTime
            // 
            resources.ApplyResources(this.tbDelayTime, "tbDelayTime");
            this.tbDelayTime.Name = "tbDelayTime";
            // 
            // btnSetDelayTime
            // 
            resources.ApplyResources(this.btnSetDelayTime, "btnSetDelayTime");
            this.btnSetDelayTime.Name = "btnSetDelayTime";
            this.btnSetDelayTime.UseVisualStyleBackColor = true;
            this.btnSetDelayTime.Click += new System.EventHandler(this.btnSetDelayTime_Click);
            // 
            // btnGetDelayTime
            // 
            resources.ApplyResources(this.btnGetDelayTime, "btnGetDelayTime");
            this.btnGetDelayTime.Name = "btnGetDelayTime";
            this.btnGetDelayTime.UseVisualStyleBackColor = true;
            this.btnGetDelayTime.Click += new System.EventHandler(this.btnGetDelayTime_Click);
            // 
            // WorkModeLB
            // 
            resources.ApplyResources(this.WorkModeLB, "WorkModeLB");
            this.WorkModeLB.Name = "WorkModeLB";
            // 
            // comboBoxWorkMode
            // 
            this.comboBoxWorkMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxWorkMode.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxWorkMode, "comboBoxWorkMode");
            this.comboBoxWorkMode.Name = "comboBoxWorkMode";
            // 
            // btnSetWorkMode
            // 
            resources.ApplyResources(this.btnSetWorkMode, "btnSetWorkMode");
            this.btnSetWorkMode.Name = "btnSetWorkMode";
            this.btnSetWorkMode.UseVisualStyleBackColor = true;
            this.btnSetWorkMode.Click += new System.EventHandler(this.btnSetWorkMode_Click);
            // 
            // btnGetWorkMode
            // 
            resources.ApplyResources(this.btnGetWorkMode, "btnGetWorkMode");
            this.btnGetWorkMode.Name = "btnGetWorkMode";
            this.btnGetWorkMode.UseVisualStyleBackColor = true;
            this.btnGetWorkMode.Click += new System.EventHandler(this.btnGetWorkMode_Click);
            // 
            // labDevNo
            // 
            resources.ApplyResources(this.labDevNo, "labDevNo");
            this.labDevNo.Name = "labDevNo";
            // 
            // tbDevNo
            // 
            resources.ApplyResources(this.tbDevNo, "tbDevNo");
            this.tbDevNo.Name = "tbDevNo";
            // 
            // btnSetDevNo
            // 
            resources.ApplyResources(this.btnSetDevNo, "btnSetDevNo");
            this.btnSetDevNo.Name = "btnSetDevNo";
            this.btnSetDevNo.UseVisualStyleBackColor = true;
            this.btnSetDevNo.Click += new System.EventHandler(this.btnSetDevNo_Click);
            // 
            // btnGetDevNo
            // 
            this.btnGetDevNo.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnGetDevNo, "btnGetDevNo");
            this.btnGetDevNo.Name = "btnGetDevNo";
            this.btnGetDevNo.UseVisualStyleBackColor = false;
            this.btnGetDevNo.Click += new System.EventHandler(this.btnGetDevNo_Click);
            // 
            // labNeightJudge
            // 
            resources.ApplyResources(this.labNeightJudge, "labNeightJudge");
            this.labNeightJudge.Name = "labNeightJudge";
            // 
            // tbNeighJudge
            // 
            resources.ApplyResources(this.tbNeighJudge, "tbNeighJudge");
            this.tbNeighJudge.Name = "tbNeighJudge";
            // 
            // btnSetNeighJudge
            // 
            resources.ApplyResources(this.btnSetNeighJudge, "btnSetNeighJudge");
            this.btnSetNeighJudge.Name = "btnSetNeighJudge";
            this.btnSetNeighJudge.UseVisualStyleBackColor = true;
            this.btnSetNeighJudge.Click += new System.EventHandler(this.btnSetNeighJudge_Click);
            // 
            // btnGetNeighJudge
            // 
            resources.ApplyResources(this.btnGetNeighJudge, "btnGetNeighJudge");
            this.btnGetNeighJudge.Name = "btnGetNeighJudge";
            this.btnGetNeighJudge.UseVisualStyleBackColor = true;
            this.btnGetNeighJudge.Click += new System.EventHandler(this.btnGetNeighJudge_Click);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // rdoBuzzerOn
            // 
            resources.ApplyResources(this.rdoBuzzerOn, "rdoBuzzerOn");
            this.rdoBuzzerOn.Checked = true;
            this.rdoBuzzerOn.Name = "rdoBuzzerOn";
            this.rdoBuzzerOn.TabStop = true;
            this.rdoBuzzerOn.UseVisualStyleBackColor = true;
            this.rdoBuzzerOn.CheckedChanged += new System.EventHandler(this.rdoBuzzerOn_CheckedChanged);
            // 
            // rdoBuzzerOff
            // 
            resources.ApplyResources(this.rdoBuzzerOff, "rdoBuzzerOff");
            this.rdoBuzzerOff.Name = "rdoBuzzerOff";
            this.rdoBuzzerOff.UseVisualStyleBackColor = true;
            this.rdoBuzzerOff.CheckedChanged += new System.EventHandler(this.rdoBuzzerOff_CheckedChanged);
            // 
            // btnSetBuzzer
            // 
            resources.ApplyResources(this.btnSetBuzzer, "btnSetBuzzer");
            this.btnSetBuzzer.Name = "btnSetBuzzer";
            this.btnSetBuzzer.UseVisualStyleBackColor = true;
            this.btnSetBuzzer.Click += new System.EventHandler(this.btnSetBuzzer_Click);
            // 
            // btnGetBuzzer
            // 
            resources.ApplyResources(this.btnGetBuzzer, "btnGetBuzzer");
            this.btnGetBuzzer.Name = "btnGetBuzzer";
            this.btnGetBuzzer.UseVisualStyleBackColor = true;
            this.btnGetBuzzer.Click += new System.EventHandler(this.btnGetBuzzer_Click);
            // 
            // labelResult
            // 
            resources.ApplyResources(this.labelResult, "labelResult");
            this.labelResult.Name = "labelResult";
            // 
            // NetParams
            // 
            this.NetParams.BackColor = System.Drawing.Color.White;
            this.NetParams.Controls.Add(this.groupBox6);
            this.NetParams.Controls.Add(this.gbSPParams);
            this.NetParams.Controls.Add(this.gbNetParams);
            this.NetParams.Controls.Add(this.btnSearchDev);
            this.NetParams.Controls.Add(this.btnDefaultParams);
            this.NetParams.Controls.Add(this.lvZl);
            resources.ApplyResources(this.NetParams, "NetParams");
            this.NetParams.Name = "NetParams";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button5);
            this.groupBox6.Controls.Add(this.HeartBeatOffRB);
            this.groupBox6.Controls.Add(this.HeartBeatOnRB);
            resources.ApplyResources(this.groupBox6, "groupBox6");
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.TabStop = false;
            // 
            // button5
            // 
            resources.ApplyResources(this.button5, "button5");
            this.button5.Name = "button5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // HeartBeatOffRB
            // 
            resources.ApplyResources(this.HeartBeatOffRB, "HeartBeatOffRB");
            this.HeartBeatOffRB.Name = "HeartBeatOffRB";
            this.HeartBeatOffRB.TabStop = true;
            this.HeartBeatOffRB.UseVisualStyleBackColor = true;
            // 
            // HeartBeatOnRB
            // 
            resources.ApplyResources(this.HeartBeatOnRB, "HeartBeatOnRB");
            this.HeartBeatOnRB.Name = "HeartBeatOnRB";
            this.HeartBeatOnRB.TabStop = true;
            this.HeartBeatOnRB.UseVisualStyleBackColor = true;
            // 
            // gbSPParams
            // 
            this.gbSPParams.Controls.Add(this.labDataBits);
            this.gbSPParams.Controls.Add(this.labCheckBits);
            this.gbSPParams.Controls.Add(this.labBaudRate);
            this.gbSPParams.Controls.Add(this.comboBoxBaudRate);
            this.gbSPParams.Controls.Add(this.comboBoxCheckBits);
            this.gbSPParams.Controls.Add(this.comboBoxDataBits);
            resources.ApplyResources(this.gbSPParams, "gbSPParams");
            this.gbSPParams.Name = "gbSPParams";
            this.gbSPParams.TabStop = false;
            // 
            // labDataBits
            // 
            resources.ApplyResources(this.labDataBits, "labDataBits");
            this.labDataBits.Name = "labDataBits";
            // 
            // labCheckBits
            // 
            resources.ApplyResources(this.labCheckBits, "labCheckBits");
            this.labCheckBits.Name = "labCheckBits";
            // 
            // labBaudRate
            // 
            resources.ApplyResources(this.labBaudRate, "labBaudRate");
            this.labBaudRate.Name = "labBaudRate";
            // 
            // comboBoxBaudRate
            // 
            this.comboBoxBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBaudRate.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxBaudRate, "comboBoxBaudRate");
            this.comboBoxBaudRate.Name = "comboBoxBaudRate";
            // 
            // comboBoxCheckBits
            // 
            this.comboBoxCheckBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCheckBits.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxCheckBits, "comboBoxCheckBits");
            this.comboBoxCheckBits.Name = "comboBoxCheckBits";
            // 
            // comboBoxDataBits
            // 
            this.comboBoxDataBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDataBits.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxDataBits, "comboBoxDataBits");
            this.comboBoxDataBits.Name = "comboBoxDataBits";
            // 
            // gbNetParams
            // 
            this.gbNetParams.Controls.Add(this.textBoxDestIP);
            this.gbNetParams.Controls.Add(this.labPromotion);
            this.gbNetParams.Controls.Add(this.labDestPort);
            this.gbNetParams.Controls.Add(this.btnSetParams);
            this.gbNetParams.Controls.Add(this.labDestIP);
            this.gbNetParams.Controls.Add(this.labGateway);
            this.gbNetParams.Controls.Add(this.labPort);
            this.gbNetParams.Controls.Add(this.labMask);
            this.gbNetParams.Controls.Add(this.labIPAdd);
            this.gbNetParams.Controls.Add(this.labIPMode);
            this.gbNetParams.Controls.Add(this.labNetMode);
            this.gbNetParams.Controls.Add(this.textBoxDestPort);
            this.gbNetParams.Controls.Add(this.textBoxGateway);
            this.gbNetParams.Controls.Add(this.textBoxPortNo);
            this.gbNetParams.Controls.Add(this.textBoxNetMask);
            this.gbNetParams.Controls.Add(this.textBoxIPAdd);
            this.gbNetParams.Controls.Add(this.comboBoxIPMode);
            this.gbNetParams.Controls.Add(this.comboBoxNetMode);
            resources.ApplyResources(this.gbNetParams, "gbNetParams");
            this.gbNetParams.Name = "gbNetParams";
            this.gbNetParams.TabStop = false;
            // 
            // textBoxDestIP
            // 
            this.textBoxDestIP.FormattingEnabled = true;
            resources.ApplyResources(this.textBoxDestIP, "textBoxDestIP");
            this.textBoxDestIP.Name = "textBoxDestIP";
            // 
            // labPromotion
            // 
            resources.ApplyResources(this.labPromotion, "labPromotion");
            this.labPromotion.Name = "labPromotion";
            // 
            // labDestPort
            // 
            resources.ApplyResources(this.labDestPort, "labDestPort");
            this.labDestPort.Name = "labDestPort";
            // 
            // btnSetParams
            // 
            resources.ApplyResources(this.btnSetParams, "btnSetParams");
            this.btnSetParams.Name = "btnSetParams";
            this.btnSetParams.UseVisualStyleBackColor = true;
            this.btnSetParams.Click += new System.EventHandler(this.btnSetParams_Click);
            // 
            // labDestIP
            // 
            resources.ApplyResources(this.labDestIP, "labDestIP");
            this.labDestIP.Name = "labDestIP";
            // 
            // labGateway
            // 
            resources.ApplyResources(this.labGateway, "labGateway");
            this.labGateway.Name = "labGateway";
            // 
            // labPort
            // 
            resources.ApplyResources(this.labPort, "labPort");
            this.labPort.Name = "labPort";
            // 
            // labMask
            // 
            resources.ApplyResources(this.labMask, "labMask");
            this.labMask.Name = "labMask";
            // 
            // labIPAdd
            // 
            resources.ApplyResources(this.labIPAdd, "labIPAdd");
            this.labIPAdd.Name = "labIPAdd";
            // 
            // labIPMode
            // 
            resources.ApplyResources(this.labIPMode, "labIPMode");
            this.labIPMode.Name = "labIPMode";
            // 
            // labNetMode
            // 
            resources.ApplyResources(this.labNetMode, "labNetMode");
            this.labNetMode.Name = "labNetMode";
            // 
            // textBoxDestPort
            // 
            resources.ApplyResources(this.textBoxDestPort, "textBoxDestPort");
            this.textBoxDestPort.Name = "textBoxDestPort";
            // 
            // textBoxGateway
            // 
            resources.ApplyResources(this.textBoxGateway, "textBoxGateway");
            this.textBoxGateway.Name = "textBoxGateway";
            // 
            // textBoxPortNo
            // 
            resources.ApplyResources(this.textBoxPortNo, "textBoxPortNo");
            this.textBoxPortNo.Name = "textBoxPortNo";
            // 
            // textBoxNetMask
            // 
            resources.ApplyResources(this.textBoxNetMask, "textBoxNetMask");
            this.textBoxNetMask.Name = "textBoxNetMask";
            // 
            // textBoxIPAdd
            // 
            resources.ApplyResources(this.textBoxIPAdd, "textBoxIPAdd");
            this.textBoxIPAdd.Name = "textBoxIPAdd";
            // 
            // comboBoxIPMode
            // 
            this.comboBoxIPMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxIPMode.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxIPMode, "comboBoxIPMode");
            this.comboBoxIPMode.Name = "comboBoxIPMode";
            // 
            // comboBoxNetMode
            // 
            this.comboBoxNetMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxNetMode.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxNetMode, "comboBoxNetMode");
            this.comboBoxNetMode.Name = "comboBoxNetMode";
            // 
            // btnSearchDev
            // 
            resources.ApplyResources(this.btnSearchDev, "btnSearchDev");
            this.btnSearchDev.Name = "btnSearchDev";
            this.btnSearchDev.UseVisualStyleBackColor = true;
            this.btnSearchDev.Click += new System.EventHandler(this.btnSearchDev_Click);
            // 
            // btnDefaultParams
            // 
            resources.ApplyResources(this.btnDefaultParams, "btnDefaultParams");
            this.btnDefaultParams.Name = "btnDefaultParams";
            this.btnDefaultParams.UseVisualStyleBackColor = true;
            this.btnDefaultParams.Click += new System.EventHandler(this.btnDefaultParams_Click);
            // 
            // lvZl
            // 
            this.lvZl.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderNo,
            this.columnHeaderIPAdd,
            this.columnHeaderPort,
            this.columnHeaderMAC});
            this.lvZl.FullRowSelect = true;
            this.lvZl.HideSelection = false;
            resources.ApplyResources(this.lvZl, "lvZl");
            this.lvZl.MultiSelect = false;
            this.lvZl.Name = "lvZl";
            this.lvZl.UseCompatibleStateImageBehavior = false;
            this.lvZl.View = System.Windows.Forms.View.Details;
            this.lvZl.ItemActivate += new System.EventHandler(this.lvZl_ItemActivate);
            this.lvZl.SelectedIndexChanged += new System.EventHandler(this.lvZl_SelectedIndexChanged);
            // 
            // columnHeaderNo
            // 
            resources.ApplyResources(this.columnHeaderNo, "columnHeaderNo");
            // 
            // columnHeaderIPAdd
            // 
            resources.ApplyResources(this.columnHeaderIPAdd, "columnHeaderIPAdd");
            // 
            // columnHeaderPort
            // 
            resources.ApplyResources(this.columnHeaderPort, "columnHeaderPort");
            // 
            // columnHeaderMAC
            // 
            resources.ApplyResources(this.columnHeaderMAC, "columnHeaderMAC");
            // 
            // tabSpecialFun
            // 
            this.tabSpecialFun.Controls.Add(this.AutoMatchGb);
            this.tabSpecialFun.Controls.Add(this.DataFormateGB);
            this.tabSpecialFun.Controls.Add(this.ReadCardModeGB);
            resources.ApplyResources(this.tabSpecialFun, "tabSpecialFun");
            this.tabSpecialFun.Name = "tabSpecialFun";
            this.tabSpecialFun.UseVisualStyleBackColor = true;
            this.tabSpecialFun.Click += new System.EventHandler(this.tabSpecialFun_Click);
            // 
            // AutoMatchGb
            // 
            this.AutoMatchGb.Controls.Add(this.StartMatchBtn);
            resources.ApplyResources(this.AutoMatchGb, "AutoMatchGb");
            this.AutoMatchGb.Name = "AutoMatchGb";
            this.AutoMatchGb.TabStop = false;
            // 
            // StartMatchBtn
            // 
            resources.ApplyResources(this.StartMatchBtn, "StartMatchBtn");
            this.StartMatchBtn.Name = "StartMatchBtn";
            this.StartMatchBtn.UseVisualStyleBackColor = true;
            this.StartMatchBtn.Click += new System.EventHandler(this.button7_Click_3);
            // 
            // DataFormateGB
            // 
            this.DataFormateGB.Controls.Add(this.button13);
            this.DataFormateGB.Controls.Add(this.haveChannelCB);
            this.DataFormateGB.Controls.Add(this.button14);
            this.DataFormateGB.Controls.Add(this.haveRssiCB);
            this.DataFormateGB.Controls.Add(this.haveDevCB);
            this.DataFormateGB.Controls.Add(this.haveAntCB);
            resources.ApplyResources(this.DataFormateGB, "DataFormateGB");
            this.DataFormateGB.Name = "DataFormateGB";
            this.DataFormateGB.TabStop = false;
            // 
            // button13
            // 
            resources.ApplyResources(this.button13, "button13");
            this.button13.Name = "button13";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // haveChannelCB
            // 
            resources.ApplyResources(this.haveChannelCB, "haveChannelCB");
            this.haveChannelCB.Name = "haveChannelCB";
            this.haveChannelCB.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            resources.ApplyResources(this.button14, "button14");
            this.button14.Name = "button14";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // haveRssiCB
            // 
            resources.ApplyResources(this.haveRssiCB, "haveRssiCB");
            this.haveRssiCB.Name = "haveRssiCB";
            this.haveRssiCB.UseVisualStyleBackColor = true;
            // 
            // haveDevCB
            // 
            resources.ApplyResources(this.haveDevCB, "haveDevCB");
            this.haveDevCB.Name = "haveDevCB";
            this.haveDevCB.UseVisualStyleBackColor = true;
            // 
            // haveAntCB
            // 
            resources.ApplyResources(this.haveAntCB, "haveAntCB");
            this.haveAntCB.Name = "haveAntCB";
            this.haveAntCB.UseVisualStyleBackColor = true;
            // 
            // ReadCardModeGB
            // 
            this.ReadCardModeGB.Controls.Add(this.button11);
            this.ReadCardModeGB.Controls.Add(this.button10);
            this.ReadCardModeGB.Controls.Add(this.valueABCB);
            this.ReadCardModeGB.Controls.Add(this.tagFocusCB);
            this.ReadCardModeGB.Controls.Add(this.ValueQCB);
            this.ReadCardModeGB.Controls.Add(this.label16);
            this.ReadCardModeGB.Controls.Add(this.label7);
            this.ReadCardModeGB.Controls.Add(this.label5);
            this.ReadCardModeGB.Controls.Add(this.sessionCB);
            this.ReadCardModeGB.Controls.Add(this.label2);
            resources.ApplyResources(this.ReadCardModeGB, "ReadCardModeGB");
            this.ReadCardModeGB.Name = "ReadCardModeGB";
            this.ReadCardModeGB.TabStop = false;
            // 
            // button11
            // 
            resources.ApplyResources(this.button11, "button11");
            this.button11.Name = "button11";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            resources.ApplyResources(this.button10, "button10");
            this.button10.Name = "button10";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // valueABCB
            // 
            this.valueABCB.FormattingEnabled = true;
            this.valueABCB.Items.AddRange(new object[] {
            resources.GetString("valueABCB.Items"),
            resources.GetString("valueABCB.Items1")});
            resources.ApplyResources(this.valueABCB, "valueABCB");
            this.valueABCB.Name = "valueABCB";
            // 
            // tagFocusCB
            // 
            this.tagFocusCB.FormattingEnabled = true;
            this.tagFocusCB.Items.AddRange(new object[] {
            resources.GetString("tagFocusCB.Items"),
            resources.GetString("tagFocusCB.Items1")});
            resources.ApplyResources(this.tagFocusCB, "tagFocusCB");
            this.tagFocusCB.Name = "tagFocusCB";
            // 
            // ValueQCB
            // 
            this.ValueQCB.FormattingEnabled = true;
            this.ValueQCB.Items.AddRange(new object[] {
            resources.GetString("ValueQCB.Items"),
            resources.GetString("ValueQCB.Items1"),
            resources.GetString("ValueQCB.Items2"),
            resources.GetString("ValueQCB.Items3"),
            resources.GetString("ValueQCB.Items4"),
            resources.GetString("ValueQCB.Items5"),
            resources.GetString("ValueQCB.Items6"),
            resources.GetString("ValueQCB.Items7"),
            resources.GetString("ValueQCB.Items8"),
            resources.GetString("ValueQCB.Items9"),
            resources.GetString("ValueQCB.Items10"),
            resources.GetString("ValueQCB.Items11"),
            resources.GetString("ValueQCB.Items12"),
            resources.GetString("ValueQCB.Items13"),
            resources.GetString("ValueQCB.Items14"),
            resources.GetString("ValueQCB.Items15")});
            resources.ApplyResources(this.ValueQCB, "ValueQCB");
            this.ValueQCB.Name = "ValueQCB";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // sessionCB
            // 
            this.sessionCB.FormattingEnabled = true;
            this.sessionCB.Items.AddRange(new object[] {
            resources.GetString("sessionCB.Items"),
            resources.GetString("sessionCB.Items1"),
            resources.GetString("sessionCB.Items2"),
            resources.GetString("sessionCB.Items3")});
            resources.ApplyResources(this.sessionCB, "sessionCB");
            this.sessionCB.Name = "sessionCB";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // AutoGetConnBT
            // 
            resources.ApplyResources(this.AutoGetConnBT, "AutoGetConnBT");
            this.AutoGetConnBT.Name = "AutoGetConnBT";
            this.AutoGetConnBT.UseVisualStyleBackColor = true;
            this.AutoGetConnBT.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // ConnectSerialCB
            // 
            this.ConnectSerialCB.Controls.Add(this.comOpenBtn);
            this.ConnectSerialCB.Controls.Add(this.button3);
            this.ConnectSerialCB.Controls.Add(this.SerialNoLB);
            this.ConnectSerialCB.Controls.Add(this.serialPortCB);
            resources.ApplyResources(this.ConnectSerialCB, "ConnectSerialCB");
            this.ConnectSerialCB.Name = "ConnectSerialCB";
            this.ConnectSerialCB.TabStop = false;
            // 
            // comOpenBtn
            // 
            resources.ApplyResources(this.comOpenBtn, "comOpenBtn");
            this.comOpenBtn.Name = "comOpenBtn";
            this.comOpenBtn.UseVisualStyleBackColor = true;
            this.comOpenBtn.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            resources.ApplyResources(this.button3, "button3");
            this.button3.Name = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // SerialNoLB
            // 
            resources.ApplyResources(this.SerialNoLB, "SerialNoLB");
            this.SerialNoLB.Name = "SerialNoLB";
            // 
            // serialPortCB
            // 
            this.serialPortCB.FormattingEnabled = true;
            resources.ApplyResources(this.serialPortCB, "serialPortCB");
            this.serialPortCB.Name = "serialPortCB";
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ClientInfoLV
            // 
            this.ClientInfoLV.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Num,
            this.IP,
            this.port,
            this.state,
            this.ID});
            this.ClientInfoLV.FullRowSelect = true;
            this.ClientInfoLV.GridLines = true;
            this.ClientInfoLV.HideSelection = false;
            resources.ApplyResources(this.ClientInfoLV, "ClientInfoLV");
            this.ClientInfoLV.MultiSelect = false;
            this.ClientInfoLV.Name = "ClientInfoLV";
            this.ClientInfoLV.UseCompatibleStateImageBehavior = false;
            this.ClientInfoLV.View = System.Windows.Forms.View.Details;
            this.ClientInfoLV.SelectedIndexChanged += new System.EventHandler(this.ClientInfoLV_SelectedIndexChanged);
            this.ClientInfoLV.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ClientInfoLV_MouseDoubleClick);
            // 
            // Num
            // 
            resources.ApplyResources(this.Num, "Num");
            // 
            // IP
            // 
            resources.ApplyResources(this.IP, "IP");
            // 
            // port
            // 
            resources.ApplyResources(this.port, "port");
            // 
            // state
            // 
            resources.ApplyResources(this.state, "state");
            // 
            // ID
            // 
            resources.ApplyResources(this.ID, "ID");
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Version
            // 
            resources.ApplyResources(this.Version, "Version");
            this.Version.Name = "Version";
            // 
            // NetWorkModeCB
            // 
            this.NetWorkModeCB.Controls.Add(this.ServerIPTB);
            this.NetWorkModeCB.Controls.Add(this.ServerStartBtn);
            this.NetWorkModeCB.Controls.Add(this.ConnectSeverIPLB);
            this.NetWorkModeCB.Controls.Add(this.ConnectSeverPortLB);
            this.NetWorkModeCB.Controls.Add(this.ServerPortTB);
            resources.ApplyResources(this.NetWorkModeCB, "NetWorkModeCB");
            this.NetWorkModeCB.Name = "NetWorkModeCB";
            this.NetWorkModeCB.TabStop = false;
            // 
            // ServerIPTB
            // 
            this.ServerIPTB.FormattingEnabled = true;
            resources.ApplyResources(this.ServerIPTB, "ServerIPTB");
            this.ServerIPTB.Name = "ServerIPTB";
            // 
            // ServerStartBtn
            // 
            resources.ApplyResources(this.ServerStartBtn, "ServerStartBtn");
            this.ServerStartBtn.Name = "ServerStartBtn";
            this.ServerStartBtn.UseVisualStyleBackColor = true;
            this.ServerStartBtn.Click += new System.EventHandler(this.ServerStartBtn_Click);
            // 
            // ConnectSeverIPLB
            // 
            resources.ApplyResources(this.ConnectSeverIPLB, "ConnectSeverIPLB");
            this.ConnectSeverIPLB.Name = "ConnectSeverIPLB";
            // 
            // ConnectSeverPortLB
            // 
            resources.ApplyResources(this.ConnectSeverPortLB, "ConnectSeverPortLB");
            this.ConnectSeverPortLB.Name = "ConnectSeverPortLB";
            // 
            // ServerPortTB
            // 
            resources.ApplyResources(this.ServerPortTB, "ServerPortTB");
            this.ServerPortTB.Name = "ServerPortTB";
            // 
            // ReadClock1
            // 
            this.ReadClock1.Enabled = true;
            this.ReadClock1.Interval = 50;
            this.ReadClock1.Tick += new System.EventHandler(this.ReadClock1_Tick);
            // 
            // readonceClock
            // 
            this.readonceClock.Tick += new System.EventHandler(this.readonceClock_Tick);
            // 
            // GetClientsInfoTimer
            // 
            this.GetClientsInfoTimer.Enabled = true;
            this.GetClientsInfoTimer.Interval = 1000;
            this.GetClientsInfoTimer.Tick += new System.EventHandler(this.GetClientsInfoTimer_Tick);
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // CurrentClientLB
            // 
            resources.ApplyResources(this.CurrentClientLB, "CurrentClientLB");
            this.CurrentClientLB.ForeColor = System.Drawing.Color.Blue;
            this.CurrentClientLB.Name = "CurrentClientLB";
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.CurrentClientLB);
            this.panel1.Name = "panel1";
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Name = "label13";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // languageSelectCB
            // 
            this.languageSelectCB.FormattingEnabled = true;
            this.languageSelectCB.Items.AddRange(new object[] {
            resources.GetString("languageSelectCB.Items"),
            resources.GetString("languageSelectCB.Items1")});
            resources.ApplyResources(this.languageSelectCB, "languageSelectCB");
            this.languageSelectCB.Name = "languageSelectCB";
            this.languageSelectCB.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // toolTip1
            // 
            this.toolTip1.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTip1_Popup);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.languageSelectCB);
            resources.ApplyResources(this.groupBox10, "groupBox10");
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.TabStop = false;
            this.groupBox10.Enter += new System.EventHandler(this.groupBox10_Enter);
            // 
            // AutoConnect
            // 
            this.AutoConnect.Controls.Add(this.AutoGetConnBT);
            resources.ApplyResources(this.AutoConnect, "AutoConnect");
            this.AutoConnect.Name = "AutoConnect";
            this.AutoConnect.TabStop = false;
            // 
            // MainWindow
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.AutoConnect);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.ConnectSerialCB);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ClientInfoLV);
            this.Controls.Add(this.NetWorkModeCB);
            this.Controls.Add(this.Version);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Closed);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.tabControl1.ResumeLayout(false);
            this.TagAccessPage.ResumeLayout(false);
            this.TagAccessPage.PerformLayout();
            this.contextMenuStrip2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.KillPWDTB.ResumeLayout(false);
            this.KillPWDTB.PerformLayout();
            this.gbIOOpr.ResumeLayout(false);
            this.gbIOOpr.PerformLayout();
            this.gbLockTag.ResumeLayout(false);
            this.gbLockTag.PerformLayout();
            this.gbRWData.ResumeLayout(false);
            this.gbRWData.PerformLayout();
            this.DevParams.ResumeLayout(false);
            this.DevParams.PerformLayout();
            this.gbDevParams.ResumeLayout(false);
            this.frequncyGB.ResumeLayout(false);
            this.frequncyGB.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.WorkModeGB.ResumeLayout(false);
            this.WorkModeGB.PerformLayout();
            this.NetParams.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.gbSPParams.ResumeLayout(false);
            this.gbSPParams.PerformLayout();
            this.gbNetParams.ResumeLayout(false);
            this.gbNetParams.PerformLayout();
            this.tabSpecialFun.ResumeLayout(false);
            this.AutoMatchGb.ResumeLayout(false);
            this.DataFormateGB.ResumeLayout(false);
            this.DataFormateGB.PerformLayout();
            this.ReadCardModeGB.ResumeLayout(false);
            this.ReadCardModeGB.PerformLayout();
            this.ConnectSerialCB.ResumeLayout(false);
            this.ConnectSerialCB.PerformLayout();
            this.NetWorkModeCB.ResumeLayout(false);
            this.NetWorkModeCB.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.AutoConnect.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void btnReadBuffer_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void btnClearBuffer_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TagAccessPage;
        private System.Windows.Forms.TabPage KillPWDTB;
        private System.Windows.Forms.TabPage NetParams;
        private System.Windows.Forms.TabPage DevParams;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnStopInv;
        private System.Windows.Forms.Button btnStartInv;
        private System.Windows.Forms.Button btnInvOnce;
        private System.Windows.Forms.GroupBox NetWorkModeCB;
        private System.Windows.Forms.Label labelTagCount;
        private System.Windows.Forms.Label TotalTagLB;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.GroupBox gbDevParams;
        private System.Windows.Forms.Button btnSetNeighJudge;
        private System.Windows.Forms.Button btnSetDevNo;
        private System.Windows.Forms.Label labDevNo;
        private System.Windows.Forms.Label labNeightJudge;
        private System.Windows.Forms.TextBox tbNeighJudge;
        private System.Windows.Forms.TextBox tbDevNo;
        private System.Windows.Forms.Button btnGetNeighJudge;
        private System.Windows.Forms.Button btnGetDevNo;
        private System.Windows.Forms.Label labResult;
        private System.Windows.Forms.GroupBox gbLockTag;
        private System.Windows.Forms.Button btnLockTag;
        private System.Windows.Forms.Label labLockType;
        private System.Windows.Forms.Label labLockBank;
        private System.Windows.Forms.Label labLockAccessPwd;
        private System.Windows.Forms.TextBox tbLockAccessPwd;
        private System.Windows.Forms.ComboBox cbbLockType;
        private System.Windows.Forms.ComboBox cbbLockBank;
        private System.Windows.Forms.GroupBox gbRWData;
        private System.Windows.Forms.Label labRWAccessPwd;
        private System.Windows.Forms.Label labData;
        private System.Windows.Forms.Label labLength;
        private System.Windows.Forms.Label labStartAdd;
        private System.Windows.Forms.Label labOprBank;
        private System.Windows.Forms.TextBox tbRWAccessPwd;
        private System.Windows.Forms.Button btnWriteData;
        private System.Windows.Forms.Button btnClearData;
        private System.Windows.Forms.Button btnReadData;
        private System.Windows.Forms.TextBox tbRWData;
        private System.Windows.Forms.ComboBox cbbLength;
        private System.Windows.Forms.ComboBox cbbStartAdd;
        private System.Windows.Forms.ComboBox cbbRWBank;
        private System.Windows.Forms.GroupBox gbSPParams;
        private System.Windows.Forms.Label labDataBits;
        private System.Windows.Forms.Label labCheckBits;
        private System.Windows.Forms.Label labBaudRate;
        private System.Windows.Forms.ComboBox comboBoxBaudRate;
        private System.Windows.Forms.ComboBox comboBoxCheckBits;
        private System.Windows.Forms.ComboBox comboBoxDataBits;
        private System.Windows.Forms.GroupBox gbNetParams;
        private System.Windows.Forms.Label labPromotion;
        private System.Windows.Forms.Label labDestPort;
        private System.Windows.Forms.Label labDestIP;
        private System.Windows.Forms.Label labGateway;
        private System.Windows.Forms.Label labPort;
        private System.Windows.Forms.Label labMask;
        private System.Windows.Forms.Label labIPAdd;
        private System.Windows.Forms.Label labIPMode;
        private System.Windows.Forms.Label labNetMode;
        private System.Windows.Forms.TextBox textBoxDestPort;
        private System.Windows.Forms.TextBox textBoxGateway;
        private System.Windows.Forms.TextBox textBoxPortNo;
        private System.Windows.Forms.TextBox textBoxNetMask;
        private System.Windows.Forms.TextBox textBoxIPAdd;
        private System.Windows.Forms.ComboBox comboBoxIPMode;
        private System.Windows.Forms.ComboBox comboBoxNetMode;
        private System.Windows.Forms.Button btnSetParams;
        private System.Windows.Forms.Button btnDefaultParams;
        private System.Windows.Forms.Button btnSearchDev;
        private System.Windows.Forms.ListView lvZl;
        private System.Windows.Forms.ColumnHeader columnHeaderNo;
        private System.Windows.Forms.ColumnHeader columnHeaderIPAdd;
        private System.Windows.Forms.ColumnHeader columnHeaderPort;
        private System.Windows.Forms.ColumnHeader columnHeaderMAC;
        private System.Windows.Forms.Button btnGetBuzzer;
        private System.Windows.Forms.Button btnSetBuzzer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rdoBuzzerOff;
        private System.Windows.Forms.RadioButton rdoBuzzerOn;
        private System.Windows.Forms.ComboBox comboBoxWorkMode;
        private System.Windows.Forms.Label WorkModeLB;
        private System.Windows.Forms.Button btnGetWorkMode;
        private System.Windows.Forms.Button btnSetWorkMode;
        private System.Windows.Forms.Button btnGetDelayTime;
        private System.Windows.Forms.Button btnSetDelayTime;
        private System.Windows.Forms.TextBox tbDelayTime;
        private System.Windows.Forms.Label TriggerDelayLB;
        private System.Windows.Forms.GroupBox gbIOOpr;
        private System.Windows.Forms.Button btnSetOutPort;
        private System.Windows.Forms.Button btnGetDI;
        private System.Windows.Forms.CheckBox cbOut2;
        private System.Windows.Forms.CheckBox cbOut1;
        private System.Windows.Forms.CheckBox cbIn2;
        private System.Windows.Forms.CheckBox cbIn1;
        private System.Windows.Forms.Button btnClearBuffer;
        private System.Windows.Forms.Button btnReadBuffer;
        private System.Windows.Forms.Label labVersion;
        private System.Windows.Forms.Label Version;
        private System.Windows.Forms.Timer ReadClock1;
        private System.Windows.Forms.Label labReadClock;
        private System.Windows.Forms.Timer readonceClock;
        private System.Windows.Forms.Button RefreshTimeSetBtn;
        private System.Windows.Forms.Button ServerStartBtn;
        private System.Windows.Forms.Label ConnectSeverPortLB;
        private System.Windows.Forms.TextBox ServerPortTB;
        private System.Windows.Forms.Label ConnectSeverIPLB;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListView ClientInfoLV;
        private System.Windows.Forms.ComboBox comboBoxWT4;
        private System.Windows.Forms.ComboBox comboBoxPower4;
        private System.Windows.Forms.ComboBox comboBoxWT3;
        private System.Windows.Forms.ComboBox comboBoxPower3;
        private System.Windows.Forms.ComboBox comboBoxWT2;
        private System.Windows.Forms.ComboBox comboBoxPower2;
        private System.Windows.Forms.ComboBox comboBoxWT1;
        private System.Windows.Forms.ComboBox comboBoxPower1;
        private System.Windows.Forms.CheckBox cbAnt4;
        private System.Windows.Forms.CheckBox cbAnt3;
        private System.Windows.Forms.CheckBox cbAnt2;
        private System.Windows.Forms.CheckBox cbAnt1;
        private System.Windows.Forms.Timer GetClientsInfoTimer;
        private System.Windows.Forms.ColumnHeader Num;
        private System.Windows.Forms.ColumnHeader IP;
        private System.Windows.Forms.ColumnHeader port;
        private System.Windows.Forms.ColumnHeader state;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label CurrentClientLB;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.CheckBox IsSingleDeviceCB;
        private System.Windows.Forms.GroupBox ConnectSerialCB;
        private System.Windows.Forms.Button comOpenBtn;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label SerialNoLB;
        private System.Windows.Forms.ComboBox serialPortCB;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox KillTagPWDTB;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button AutoGetConnBT;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.RadioButton HeartBeatOffRB;
        private System.Windows.Forms.RadioButton HeartBeatOnRB;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox frequncyGB;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox checkBox45;
        private System.Windows.Forms.CheckBox frequency50;
        private System.Windows.Forms.CheckBox frequency49;
        private System.Windows.Forms.CheckBox frequency48;
        private System.Windows.Forms.CheckBox frequency47;
        private System.Windows.Forms.CheckBox frequency46;
        private System.Windows.Forms.CheckBox frequency45;
        private System.Windows.Forms.CheckBox frequency44;
        private System.Windows.Forms.CheckBox frequency43;
        private System.Windows.Forms.CheckBox frequency42;
        private System.Windows.Forms.CheckBox frequency41;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.CheckBox frequency40;
        private System.Windows.Forms.CheckBox frequency39;
        private System.Windows.Forms.CheckBox frequency38;
        private System.Windows.Forms.CheckBox frequency37;
        private System.Windows.Forms.CheckBox frequency36;
        private System.Windows.Forms.CheckBox frequency35;
        private System.Windows.Forms.CheckBox frequency34;
        private System.Windows.Forms.CheckBox frequency33;
        private System.Windows.Forms.CheckBox frequency32;
        private System.Windows.Forms.CheckBox frequency31;
        private System.Windows.Forms.CheckBox checkBox34;
        private System.Windows.Forms.CheckBox frequency30;
        private System.Windows.Forms.CheckBox frequency29;
        private System.Windows.Forms.CheckBox frequency28;
        private System.Windows.Forms.CheckBox frequency27;
        private System.Windows.Forms.CheckBox frequency26;
        private System.Windows.Forms.CheckBox frequency25;
        private System.Windows.Forms.CheckBox frequency24;
        private System.Windows.Forms.CheckBox frequency23;
        private System.Windows.Forms.CheckBox frequency22;
        private System.Windows.Forms.CheckBox frequency21;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox frequency20;
        private System.Windows.Forms.CheckBox frequency19;
        private System.Windows.Forms.CheckBox frequency18;
        private System.Windows.Forms.CheckBox frequency17;
        private System.Windows.Forms.CheckBox frequency16;
        private System.Windows.Forms.CheckBox frequency15;
        private System.Windows.Forms.CheckBox frequency14;
        private System.Windows.Forms.CheckBox frequency13;
        private System.Windows.Forms.CheckBox frequency12;
        private System.Windows.Forms.CheckBox frequency11;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox frequency10;
        private System.Windows.Forms.CheckBox frequency9;
        private System.Windows.Forms.CheckBox frequency8;
        private System.Windows.Forms.CheckBox frequency7;
        private System.Windows.Forms.CheckBox frequency6;
        private System.Windows.Forms.CheckBox frequency5;
        private System.Windows.Forms.CheckBox frequency4;
        private System.Windows.Forms.CheckBox frequency3;
        private System.Windows.Forms.CheckBox frequency2;
        private System.Windows.Forms.Button BtnSetFrequency;
        private System.Windows.Forms.Button BtnGetFrequency;
        private System.Windows.Forms.CheckBox frequency1;
        private System.Windows.Forms.ComboBox FrequencyAreaCB;
        private System.Windows.Forms.Label LBFRequency;
        private System.Windows.Forms.GroupBox WorkModeGB;
        private System.Windows.Forms.Button DataChannelSetBtn;
        private System.Windows.Forms.ComboBox DataChannelCB;
        private System.Windows.Forms.Label DataTransMode;
        private System.Windows.Forms.Button TriggerAlarmBtn;
        private System.Windows.Forms.TextBox TriggerAlarmTimeTB;
        private System.Windows.Forms.Label TriggerDelay;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox languageSelectCB;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox Ant08;
        private System.Windows.Forms.CheckBox Ant07;
        private System.Windows.Forms.CheckBox Ant06;
        private System.Windows.Forms.CheckBox Ant05;
        private System.Windows.Forms.CheckBox Ant04;
        private System.Windows.Forms.CheckBox Ant03;
        private System.Windows.Forms.CheckBox Ant02;
        private System.Windows.Forms.CheckBox Ant01;
        private System.Windows.Forms.CheckBox Ant32;
        private System.Windows.Forms.CheckBox Ant31;
        private System.Windows.Forms.CheckBox Ant30;
        private System.Windows.Forms.CheckBox Ant29;
        private System.Windows.Forms.CheckBox Ant28;
        private System.Windows.Forms.CheckBox Ant27;
        private System.Windows.Forms.CheckBox Ant26;
        private System.Windows.Forms.CheckBox Ant25;
        private System.Windows.Forms.CheckBox Ant24;
        private System.Windows.Forms.CheckBox Ant23;
        private System.Windows.Forms.CheckBox Ant22;
        private System.Windows.Forms.CheckBox Ant21;
        private System.Windows.Forms.CheckBox Ant20;
        private System.Windows.Forms.CheckBox Ant19;
        private System.Windows.Forms.CheckBox Ant18;
        private System.Windows.Forms.CheckBox Ant17;
        private System.Windows.Forms.CheckBox Ant16;
        private System.Windows.Forms.CheckBox Ant15;
        private System.Windows.Forms.CheckBox Ant14;
        private System.Windows.Forms.CheckBox Ant13;
        private System.Windows.Forms.CheckBox Ant12;
        private System.Windows.Forms.CheckBox Ant11;
        private System.Windows.Forms.CheckBox Ant10;
        private System.Windows.Forms.CheckBox Ant09;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.CheckBox checkBox89;
        private System.Windows.Forms.CheckBox checkBox90;
        private System.Windows.Forms.CheckBox checkBox91;
        private System.Windows.Forms.CheckBox checkBox95;
        private System.Windows.Forms.ComboBox MultiAntPowerTB;
        private System.Windows.Forms.ComboBox MultiAntTime;
        private System.Windows.Forms.Label MultiAntPower;
        private System.Windows.Forms.Label MultiAntWorktime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rb32Ant;
        private System.Windows.Forms.RadioButton rb16Ant;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LBFrequencypoint;
        private System.Windows.Forms.Button GetAntStateBt;
        private System.Windows.Forms.Label ReadSpeedSLb;
        private System.Windows.Forms.Label ReadCountLB;
        private System.Windows.Forms.TabPage tabSpecialFun;
        private System.Windows.Forms.Button LoginBtn;
        private System.Windows.Forms.TextBox SpecialFunTabTB;
        private System.Windows.Forms.Label SpecalFunLB;
        private System.Windows.Forms.Label readSpeedLB;
        private System.Windows.Forms.Label readCountsLB;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox ReadCardModeGB;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.ComboBox valueABCB;
        private System.Windows.Forms.ComboBox tagFocusCB;
        private System.Windows.Forms.ComboBox ValueQCB;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox sessionCB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button factoryBtn;
        private System.Windows.Forms.GroupBox DataFormateGB;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.CheckBox haveChannelCB;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.CheckBox haveRssiCB;
        private System.Windows.Forms.CheckBox haveDevCB;
        private System.Windows.Forms.CheckBox haveAntCB;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label labelTagTIDCount;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem 查询标签ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 寻卡模式ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 频点ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 天线ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 数据显示格式ToolStripMenuItem;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox ServerIPTB;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Button btnSetAnts;
        private System.Windows.Forms.RadioButton Rb4Channel;
        private System.Windows.Forms.GroupBox AutoConnect;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.ComboBox ConstantfrequencyTB;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button StartMatchBtn;
        private System.Windows.Forms.GroupBox AutoMatchGb;
        private System.Windows.Forms.ComboBox textBoxDestIP;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}


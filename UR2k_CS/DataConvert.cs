﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UR2k_CS
{
    public class DataConvert
    {
        internal static string ConvertToString(byte[] bytes, int startIndex, int length)
        {
            string result = string.Empty;
            for (int index = startIndex; index < length && index<bytes.Length; index++)
            {
                result += bytes[index].ToString("X2");
            }
            return result;
        }
    }
}

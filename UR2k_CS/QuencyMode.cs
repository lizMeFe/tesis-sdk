﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Windows.Forms;
using UHFReader;
using UHFReader.Model;

namespace UR2k_CS
{
    public partial class QuencyMode : Form
    {


        Reader reader;
        Client currentClient;
        ResourceManager rm;
        string noDeviceChoose;
        public QuencyMode()
        {
            InitializeComponent();
        }



        public QuencyMode(Reader reader, Client currentClien, ResourceManager rm)
        {
            InitializeComponent();
            this.reader = reader;
            this.currentClient = currentClien;
            this.rm = rm;
            noDeviceChoose = rm.GetString("strChooseDevice");
        }

        private void ValueQCB_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void sessionCB_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                if (currentClient != null)
                {
                    QuencyPara quencyPara = new QuencyPara
                    {
                        session = byte.Parse(sessionCB.Text),
                        valueQ = byte.Parse(ValueQCB.Text),
                        tagfocus = byte.Parse(tagFocusCB.Text),
                        valueAB = byte.Parse(valueABCB.Text),
                    };
                    reader.SetQuencyPara(currentClient, quencyPara);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                if (currentClient != null)
                {
                    reader.GetQuencyPara(currentClient, 0x00);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        internal void UpdateQuencyPara(QuencyPara quencyPara)
        {
            sessionCB.Text = quencyPara.session.ToString();
            ValueQCB.Text = quencyPara.valueQ.ToString();
            tagFocusCB.Text = quencyPara.tagfocus.ToString();
            valueABCB.Text = quencyPara.valueAB.ToString();
        }
    }
}

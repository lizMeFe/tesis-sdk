﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using UHFReader.Model;

namespace UR2k_CS.Mock
{
    public class TagMock
    {
        public static List<Tag> GetData()
        {
            //var jsonString = System.IO.File.ReadAllText(@"C:\Users\Public\TestFolder\WriteText.txt");
            var jsonString = Properties.Resources.tag_data;
            return JsonConvert.DeserializeObject<List<Tag>>(jsonString);
        }
    }
}

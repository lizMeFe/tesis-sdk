﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UR2k_CS.Models
{
    public enum EnumSDKActions
    {
        Open,
        Close,
        ReadOne,
        KeepReading
    }
}

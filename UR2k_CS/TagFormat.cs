﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Windows.Forms;
using UHFReader;
using UHFReader.Model;

namespace UR2k_CS
{
    public partial class TagFormat : Form
    {

        Reader reader;
        Client currentClient;
        ResourceManager rm;
        string noDeviceChoose;
        public TagFormat()
        {
            InitializeComponent();
        }

        public TagFormat(Reader reader, Client currentClien, ResourceManager rm)
        {
            InitializeComponent();
            this.reader = reader;
            this.currentClient = currentClien;
            this.rm = rm;
            noDeviceChoose = rm.GetString("strChooseDevice");
        }

        private void button14_Click(object sender, EventArgs e)
        {
            try
            {
                if (currentClient != null)
                {
                    reader.GetFormartTagsData(currentClient, 0x00);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            byte type = 0x00;
            if (haveAntCB.Checked)
                type |= 0x80;
            if (haveRssiCB.Checked)
                type |= 0x40;
            if (haveDevCB.Checked)
                type |= 0x20;
            if (haveChannelCB.Checked)
                type |= 0x10;
            try
            {
                if (currentClient != null)
                {
                    reader.SetFormartTagsData(currentClient, type);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }


        internal void UpdateTagDataFormat(TagDataFormat tagDataFormat)
        {
            if ((tagDataFormat.type & 0x80) != 0)
                haveAntCB.Checked = true;
            else
                haveAntCB.Checked = false;
            if ((tagDataFormat.type & 0x40) != 0)
                haveRssiCB.Checked = true;
            else
                haveRssiCB.Checked = false;
            if ((tagDataFormat.type & 0x20) != 0)
                haveDevCB.Checked = true;
            else
                haveDevCB.Checked = false;
            if ((tagDataFormat.type & 0x10) != 0)
                haveChannelCB.Checked = true;
            else
                haveChannelCB.Checked = false;
        }
    }
}

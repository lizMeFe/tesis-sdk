﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using UHFReader;
using UHFReader.Model;

namespace UR2k_CS
{
    public partial class FrequencyPoints : Form
    {
        private Reader reader;
        private Client currentClient;
        private ResourceManager rm;
        private string noDeviceChoose;
        private CheckBox[] frequencysCB;

        public FrequencyPoints()
        {
            InitializeComponent();
        }

        public FrequencyPoints(Reader reader, Client currentClient, ResourceManager rm)
        {
            InitializeComponent();
            this.reader = reader;
            this.currentClient = currentClient;
            this.rm = rm;
            noDeviceChoose = rm.GetString("strChooseDevice");
            frequencysCB = new CheckBox[] { checkBox1, checkBox2, checkBox3, checkBox4, checkBox5, checkBox6, checkBox7, checkBox8, checkBox9, checkBox10,
                                            checkBox13, checkBox14, checkBox15, checkBox16, checkBox17, checkBox18, checkBox19, checkBox20, checkBox21, checkBox22,
                                            checkBox24, checkBox25, checkBox26, checkBox27, checkBox28, checkBox29, checkBox30, checkBox31, checkBox32, checkBox33,
                                            checkBox35, checkBox36, checkBox37, checkBox38, checkBox39, checkBox40, checkBox41, checkBox42, checkBox43, checkBox44,
                                            checkBox46, checkBox47, checkBox48, checkBox49, checkBox50, checkBox51, checkBox52, checkBox53, checkBox54, checkBox55,
            };
        }

        private void checkBox12_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox12.Checked)
            {
                checkBox13.Checked = true; checkBox14.Checked = true; checkBox15.Checked = true; checkBox16.Checked = true; checkBox17.Checked = true;
                checkBox18.Checked = true; checkBox19.Checked = true; checkBox20.Checked = true; checkBox21.Checked = true; checkBox22.Checked = true;
            }
            else
            {
                checkBox13.Checked = false; checkBox14.Checked = false; checkBox15.Checked = false; checkBox16.Checked = false; checkBox17.Checked = false;
                checkBox18.Checked = false; checkBox19.Checked = false; checkBox20.Checked = false; checkBox21.Checked = false; checkBox22.Checked = false;
            }
        }

        private void checkBox38_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox27_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox25_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox51_CheckedChanged(object sender, EventArgs e)
        {

        }

        internal void UpdateeFrequencyArea(FrequencyArea frequencyArea)
        {
            FrequencyAreaCB.SelectedIndex = frequencyArea.area - 1;
        }

        internal void UpdateecustomFrequcney(CustomFrequcney customFrequcney)
        {
            if (customFrequcney.type == 0x01)
            {
                ConstantfrequencyTB.Text = customFrequcney.point.ToString();
            }
            else if (customFrequcney.type == 0x00)
            {
                for (int j = 0; j < frequencysCB.Length; j++)
                {
                    frequencysCB[j].Checked = false;
                }
                for (int i = 0; i < customFrequcney.points.Count; i++)
                {
                    frequencysCB[customFrequcney.points[i]].Checked = true;
                }
            }
        }

        private void BtnGetFrequency_Click(object sender, EventArgs e)
        {
            try
            {
                if (currentClient != null)
                {
                    reader.GetFrequencyArea(currentClient);
                    Thread.Sleep(100);
                    reader.GetFrequency(currentClient);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void BtnSetFrequency_Click(object sender, EventArgs e)
        {
            try
            {
                if (currentClient != null)
                {
                    FrequencyArea area = new FrequencyArea
                    {
                        area = (byte)(FrequencyAreaCB.SelectedIndex + 1),
                    };
                    byte showArea = area.area;
                    if (area.area == 6) { area.area = 5; }
                    reader.SetFrequencyArea(currentClient, area);
                    if (showArea == 0x05)                 //自定义频点
                    {
                        CustomFrequcney frequency = new CustomFrequcney();
                        frequency.points = new List<byte>();
                        frequency.type = 0x00;
                        for (int index = 0; index < 50; index++)
                        {
                            if (frequencysCB[index].Checked == true)
                            {
                                frequency.points.Add((byte)index);
                            }
                        }
                        Thread.Sleep(100);
                        reader.SetFrequency(currentClient, frequency);
                    }
                    if (showArea == 0x06)                      //定频
                    {
                        CustomFrequcney frequency = new CustomFrequcney();
                        frequency.points = new List<byte>();
                        frequency.type = 0x01;
                        frequency.point = int.Parse(ConstantfrequencyTB.Text);
                        Thread.Sleep(100);
                        reader.SetFrequency(currentClient, frequency);
                    }
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void checkBox11_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox11.Checked)
            {
                checkBox1.Checked = true; checkBox2.Checked = true; checkBox3.Checked = true; checkBox4.Checked = true; checkBox5.Checked = true;
                checkBox6.Checked = true; checkBox7.Checked = true; checkBox8.Checked = true; checkBox9.Checked = true; checkBox10.Checked = true;
            }
            else
            {
                checkBox1.Checked = false; checkBox2.Checked = false; checkBox3.Checked = false; checkBox4.Checked = false; checkBox5.Checked = false;
                checkBox6.Checked = false; checkBox7.Checked = false; checkBox8.Checked = false; checkBox9.Checked = false; checkBox10.Checked = false;
            }
        }

        private void checkBox45_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox45.Checked)
            {
                checkBox46.Checked = true; checkBox47.Checked = true; checkBox48.Checked = true; checkBox49.Checked = true; checkBox50.Checked = true;
                checkBox51.Checked = true; checkBox52.Checked = true; checkBox53.Checked = true; checkBox54.Checked = true; checkBox55.Checked = true;
            }
            else
            {
                checkBox46.Checked = false; checkBox47.Checked = false; checkBox48.Checked = false; checkBox49.Checked = false; checkBox50.Checked = false;
                checkBox51.Checked = false; checkBox52.Checked = false; checkBox53.Checked = false; checkBox54.Checked = false; checkBox55.Checked = false;
            }
        }

        private void checkBox34_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox34.Checked)
            {
                checkBox35.Checked = true; checkBox36.Checked = true; checkBox37.Checked = true; checkBox38.Checked = true; checkBox39.Checked = true;
                checkBox40.Checked = true; checkBox41.Checked = true; checkBox42.Checked = true; checkBox43.Checked = true; checkBox44.Checked = true;
            }
            else
            {
                checkBox35.Checked = false; checkBox36.Checked = false; checkBox37.Checked = false; checkBox38.Checked = false; checkBox39.Checked = false;
                checkBox40.Checked = false; checkBox41.Checked = false; checkBox42.Checked = false; checkBox43.Checked = false; checkBox44.Checked = false;
            }
        }

        private void checkBox23_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox23.Checked)
            {
                checkBox24.Checked = true; checkBox25.Checked = true; checkBox26.Checked = true; checkBox27.Checked = true; checkBox28.Checked = true;
                checkBox29.Checked = true; checkBox30.Checked = true; checkBox31.Checked = true; checkBox32.Checked = true; checkBox33.Checked = true;
            }
            else
            {
                checkBox24.Checked = false; checkBox25.Checked = false; checkBox26.Checked = false; checkBox27.Checked = false; checkBox28.Checked = false;
                checkBox29.Checked = false; checkBox30.Checked = false; checkBox31.Checked = false; checkBox32.Checked = false; checkBox33.Checked = false;
            }
        }
    }
}

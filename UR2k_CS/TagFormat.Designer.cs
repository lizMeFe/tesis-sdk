﻿namespace UR2k_CS
{
    partial class TagFormat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.haveChannelCB = new System.Windows.Forms.CheckBox();
            this.haveRssiCB = new System.Windows.Forms.CheckBox();
            this.haveDevCB = new System.Windows.Forms.CheckBox();
            this.haveAntCB = new System.Windows.Forms.CheckBox();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // haveChannelCB
            // 
            this.haveChannelCB.AutoSize = true;
            this.haveChannelCB.Location = new System.Drawing.Point(21, 160);
            this.haveChannelCB.Name = "haveChannelCB";
            this.haveChannelCB.Size = new System.Drawing.Size(108, 16);
            this.haveChannelCB.TabIndex = 1;
            this.haveChannelCB.Text = "通道门进出方向";
            this.haveChannelCB.UseVisualStyleBackColor = true;
            // 
            // haveRssiCB
            // 
            this.haveRssiCB.AutoSize = true;
            this.haveRssiCB.Checked = true;
            this.haveRssiCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.haveRssiCB.Location = new System.Drawing.Point(21, 70);
            this.haveRssiCB.Name = "haveRssiCB";
            this.haveRssiCB.Size = new System.Drawing.Size(60, 16);
            this.haveRssiCB.TabIndex = 1;
            this.haveRssiCB.Text = "rsii值";
            this.haveRssiCB.UseVisualStyleBackColor = true;
            // 
            // haveDevCB
            // 
            this.haveDevCB.AutoSize = true;
            this.haveDevCB.Location = new System.Drawing.Point(21, 115);
            this.haveDevCB.Name = "haveDevCB";
            this.haveDevCB.Size = new System.Drawing.Size(60, 16);
            this.haveDevCB.TabIndex = 0;
            this.haveDevCB.Text = "设备号";
            this.haveDevCB.UseVisualStyleBackColor = true;
            // 
            // haveAntCB
            // 
            this.haveAntCB.AutoSize = true;
            this.haveAntCB.Checked = true;
            this.haveAntCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.haveAntCB.Location = new System.Drawing.Point(21, 25);
            this.haveAntCB.Name = "haveAntCB";
            this.haveAntCB.Size = new System.Drawing.Size(60, 16);
            this.haveAntCB.TabIndex = 0;
            this.haveAntCB.Text = "天线号";
            this.haveAntCB.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(49, 232);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(64, 32);
            this.button13.TabIndex = 9;
            this.button13.Text = "设置";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button14
            // 
            this.button14.ForeColor = System.Drawing.Color.Black;
            this.button14.Location = new System.Drawing.Point(50, 189);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(64, 32);
            this.button14.TabIndex = 8;
            this.button14.Text = "查询";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // TagFormat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(164, 268);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.haveChannelCB);
            this.Controls.Add(this.haveAntCB);
            this.Controls.Add(this.haveDevCB);
            this.Controls.Add(this.haveRssiCB);
            this.ForeColor = System.Drawing.Color.Black;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagFormat";
            this.ShowIcon = false;
            this.Text = "盘存标签数据格式";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox haveChannelCB;
        private System.Windows.Forms.CheckBox haveRssiCB;
        private System.Windows.Forms.CheckBox haveDevCB;
        private System.Windows.Forms.CheckBox haveAntCB;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
    }
}
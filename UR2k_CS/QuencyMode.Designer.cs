﻿namespace UR2k_CS
{
    partial class QuencyMode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.valueABCB = new System.Windows.Forms.ComboBox();
            this.tagFocusCB = new System.Windows.Forms.ComboBox();
            this.ValueQCB = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.sessionCB = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(48, 203);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(64, 32);
            this.button11.TabIndex = 5;
            this.button11.Text = "设置";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(48, 153);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(64, 32);
            this.button10.TabIndex = 4;
            this.button10.Text = "查询";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // valueABCB
            // 
            this.valueABCB.FormattingEnabled = true;
            this.valueABCB.Items.AddRange(new object[] {
            "0",
            "1"});
            this.valueABCB.Location = new System.Drawing.Point(93, 121);
            this.valueABCB.Name = "valueABCB";
            this.valueABCB.Size = new System.Drawing.Size(68, 20);
            this.valueABCB.TabIndex = 3;
            // 
            // tagFocusCB
            // 
            this.tagFocusCB.FormattingEnabled = true;
            this.tagFocusCB.Items.AddRange(new object[] {
            "0",
            "1"});
            this.tagFocusCB.Location = new System.Drawing.Point(93, 84);
            this.tagFocusCB.Name = "tagFocusCB";
            this.tagFocusCB.Size = new System.Drawing.Size(68, 20);
            this.tagFocusCB.TabIndex = 3;
            // 
            // ValueQCB
            // 
            this.ValueQCB.FormattingEnabled = true;
            this.ValueQCB.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.ValueQCB.Location = new System.Drawing.Point(93, 46);
            this.ValueQCB.Name = "ValueQCB";
            this.ValueQCB.Size = new System.Drawing.Size(68, 20);
            this.ValueQCB.TabIndex = 3;
            this.ValueQCB.SelectedIndexChanged += new System.EventHandler(this.ValueQCB_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 124);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 12);
            this.label16.TabIndex = 2;
            this.label16.Text = "AB值：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 87);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 2;
            this.label7.Text = "Tagfocus：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "Q值：";
            // 
            // sessionCB
            // 
            this.sessionCB.FormattingEnabled = true;
            this.sessionCB.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.sessionCB.Location = new System.Drawing.Point(93, 6);
            this.sessionCB.Name = "sessionCB";
            this.sessionCB.Size = new System.Drawing.Size(68, 20);
            this.sessionCB.TabIndex = 1;
            this.sessionCB.SelectedIndexChanged += new System.EventHandler(this.sessionCB_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "Session:";
            // 
            // QuencyMode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(167, 242);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.valueABCB);
            this.Controls.Add(this.sessionCB);
            this.Controls.Add(this.tagFocusCB);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ValueQCB);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label16);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "QuencyMode";
            this.Text = "寻卡模式";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.ComboBox valueABCB;
        private System.Windows.Forms.ComboBox tagFocusCB;
        private System.Windows.Forms.ComboBox ValueQCB;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox sessionCB;
        private System.Windows.Forms.Label label2;
    }
}
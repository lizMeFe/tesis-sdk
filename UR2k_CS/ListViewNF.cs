﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UR2k_CS
{
    /// <summary>
    /// 添加listview排序
    /// </summary>
    public class ListViewItemComparer : System.Collections.IComparer
    {
        private int col;
        private bool sort;

        public ListViewItemComparer()
        {
            ;
        }

        public ListViewItemComparer(int col,bool sort)
        {
            this.col = col;
            this.sort = sort;
        }

        public int Compare(object x, object y)
        {
            int returnVal = -1;
            if (sort)
            {
                returnVal = String.Compare(((ListViewItem)x).SubItems[col].Text,
                                  ((ListViewItem)y).SubItems[col].Text);
            }
            else
            {
                returnVal = String.Compare(((ListViewItem)y).SubItems[col].Text,
                  ((ListViewItem)x).SubItems[col].Text);
            }
            return returnVal;
        }
    }

    // 解决插入数据表格闪烁的问题
    class ListViewNF : System.Windows.Forms.ListView
    {
        bool sort = false;
        public ListViewNF()
        {
           
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.EnableNotifyMessage, true);
            this.ColumnClick += ListViewNF_ColumnClick;
        }

            
        private void ListViewNF_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            this.ListViewItemSorter = new ListViewItemComparer(e.Column, sort);
            this.Sort();
            sort = !sort;
            for (int i = 0; i < this.Items.Count; i++)
            {
                this.Items[i].SubItems[0].Text = (i + 1).ToString();
            }
        }

        protected override void OnNotifyMessage(Message m)
        {
            if (m.Msg != 0x14)
            {
                base.OnNotifyMessage(m);
            }
        }

        internal void DefaultSort()
        {
            this.ListViewItemSorter = null;
        }
    }
}

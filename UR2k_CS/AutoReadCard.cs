﻿namespace UR2k_CS
{
    public class AutoReadCard
    {
        public byte mode { get; set; }
        public int time { get; set; }
        public int counts { get; set; }
        public byte save { get; set; }
    }
}
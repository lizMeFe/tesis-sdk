﻿namespace UR2k_CS
{
    partial class AntForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbAntParams = new System.Windows.Forms.GroupBox();
            this.GetAntStateBt = new System.Windows.Forms.Button();
            this.labAnt = new System.Windows.Forms.Label();
            this.btnSetAnts = new System.Windows.Forms.Button();
            this.btnGetAnts = new System.Windows.Forms.Button();
            this.comboBoxWT4 = new System.Windows.Forms.ComboBox();
            this.comboBoxPower4 = new System.Windows.Forms.ComboBox();
            this.comboBoxWT3 = new System.Windows.Forms.ComboBox();
            this.comboBoxPower3 = new System.Windows.Forms.ComboBox();
            this.comboBoxWT2 = new System.Windows.Forms.ComboBox();
            this.comboBoxPower2 = new System.Windows.Forms.ComboBox();
            this.comboBoxWT1 = new System.Windows.Forms.ComboBox();
            this.comboBoxPower1 = new System.Windows.Forms.ComboBox();
            this.cbAnt4 = new System.Windows.Forms.CheckBox();
            this.cbAnt3 = new System.Windows.Forms.CheckBox();
            this.cbAnt2 = new System.Windows.Forms.CheckBox();
            this.cbAnt1 = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rb8Ant = new System.Windows.Forms.RadioButton();
            this.rb32Ant = new System.Windows.Forms.RadioButton();
            this.rb16Ant = new System.Windows.Forms.RadioButton();
            this.MultiAntPowerTB = new System.Windows.Forms.ComboBox();
            this.MultiAntTime = new System.Windows.Forms.ComboBox();
            this.MultiAntPower = new System.Windows.Forms.Label();
            this.MultiAntWorktime = new System.Windows.Forms.Label();
            this.checkBox89 = new System.Windows.Forms.CheckBox();
            this.checkBox90 = new System.Windows.Forms.CheckBox();
            this.checkBox91 = new System.Windows.Forms.CheckBox();
            this.checkBox95 = new System.Windows.Forms.CheckBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.checkBox64 = new System.Windows.Forms.CheckBox();
            this.checkBox65 = new System.Windows.Forms.CheckBox();
            this.checkBox66 = new System.Windows.Forms.CheckBox();
            this.checkBox67 = new System.Windows.Forms.CheckBox();
            this.checkBox68 = new System.Windows.Forms.CheckBox();
            this.checkBox69 = new System.Windows.Forms.CheckBox();
            this.checkBox70 = new System.Windows.Forms.CheckBox();
            this.checkBox71 = new System.Windows.Forms.CheckBox();
            this.checkBox80 = new System.Windows.Forms.CheckBox();
            this.checkBox81 = new System.Windows.Forms.CheckBox();
            this.checkBox82 = new System.Windows.Forms.CheckBox();
            this.checkBox83 = new System.Windows.Forms.CheckBox();
            this.checkBox84 = new System.Windows.Forms.CheckBox();
            this.checkBox85 = new System.Windows.Forms.CheckBox();
            this.checkBox86 = new System.Windows.Forms.CheckBox();
            this.checkBox87 = new System.Windows.Forms.CheckBox();
            this.checkBox72 = new System.Windows.Forms.CheckBox();
            this.checkBox73 = new System.Windows.Forms.CheckBox();
            this.checkBox74 = new System.Windows.Forms.CheckBox();
            this.checkBox75 = new System.Windows.Forms.CheckBox();
            this.checkBox76 = new System.Windows.Forms.CheckBox();
            this.checkBox77 = new System.Windows.Forms.CheckBox();
            this.checkBox78 = new System.Windows.Forms.CheckBox();
            this.checkBox79 = new System.Windows.Forms.CheckBox();
            this.checkBox60 = new System.Windows.Forms.CheckBox();
            this.checkBox61 = new System.Windows.Forms.CheckBox();
            this.checkBox62 = new System.Windows.Forms.CheckBox();
            this.checkBox63 = new System.Windows.Forms.CheckBox();
            this.checkBox58 = new System.Windows.Forms.CheckBox();
            this.checkBox59 = new System.Windows.Forms.CheckBox();
            this.checkBox57 = new System.Windows.Forms.CheckBox();
            this.checkBox56 = new System.Windows.Forms.CheckBox();
            this.gbAntParams.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbAntParams
            // 
            this.gbAntParams.Controls.Add(this.GetAntStateBt);
            this.gbAntParams.Controls.Add(this.labAnt);
            this.gbAntParams.Controls.Add(this.btnSetAnts);
            this.gbAntParams.Controls.Add(this.btnGetAnts);
            this.gbAntParams.Controls.Add(this.comboBoxWT4);
            this.gbAntParams.Controls.Add(this.comboBoxPower4);
            this.gbAntParams.Controls.Add(this.comboBoxWT3);
            this.gbAntParams.Controls.Add(this.comboBoxPower3);
            this.gbAntParams.Controls.Add(this.comboBoxWT2);
            this.gbAntParams.Controls.Add(this.comboBoxPower2);
            this.gbAntParams.Controls.Add(this.comboBoxWT1);
            this.gbAntParams.Controls.Add(this.comboBoxPower1);
            this.gbAntParams.Controls.Add(this.cbAnt4);
            this.gbAntParams.Controls.Add(this.cbAnt3);
            this.gbAntParams.Controls.Add(this.cbAnt2);
            this.gbAntParams.Controls.Add(this.cbAnt1);
            this.gbAntParams.Location = new System.Drawing.Point(3, 12);
            this.gbAntParams.Name = "gbAntParams";
            this.gbAntParams.Size = new System.Drawing.Size(275, 194);
            this.gbAntParams.TabIndex = 48;
            this.gbAntParams.TabStop = false;
            this.gbAntParams.Text = "4通道天线 工作时间(ms) 功率(db)  ";
            // 
            // GetAntStateBt
            // 
            this.GetAntStateBt.Location = new System.Drawing.Point(144, 151);
            this.GetAntStateBt.Name = "GetAntStateBt";
            this.GetAntStateBt.Size = new System.Drawing.Size(114, 31);
            this.GetAntStateBt.TabIndex = 31;
            this.GetAntStateBt.Text = "检测天线状态";
            this.GetAntStateBt.UseVisualStyleBackColor = true;
            this.GetAntStateBt.Click += new System.EventHandler(this.GetAntStateBt_Click);
            // 
            // labAnt
            // 
            this.labAnt.AutoSize = true;
            this.labAnt.Location = new System.Drawing.Point(13, 194);
            this.labAnt.Name = "labAnt";
            this.labAnt.Size = new System.Drawing.Size(0, 12);
            this.labAnt.TabIndex = 30;
            // 
            // btnSetAnts
            // 
            this.btnSetAnts.Location = new System.Drawing.Point(81, 151);
            this.btnSetAnts.Name = "btnSetAnts";
            this.btnSetAnts.Size = new System.Drawing.Size(55, 30);
            this.btnSetAnts.TabIndex = 26;
            this.btnSetAnts.Text = "设置";
            this.btnSetAnts.UseVisualStyleBackColor = true;
            this.btnSetAnts.Click += new System.EventHandler(this.btnSetAnts_Click);
            // 
            // btnGetAnts
            // 
            this.btnGetAnts.Location = new System.Drawing.Point(20, 152);
            this.btnGetAnts.Name = "btnGetAnts";
            this.btnGetAnts.Size = new System.Drawing.Size(55, 30);
            this.btnGetAnts.TabIndex = 25;
            this.btnGetAnts.Text = "读取";
            this.btnGetAnts.UseVisualStyleBackColor = true;
            this.btnGetAnts.Click += new System.EventHandler(this.btnGetAnts_Click);
            // 
            // comboBoxWT4
            // 
            this.comboBoxWT4.FormattingEnabled = true;
            this.comboBoxWT4.Location = new System.Drawing.Point(80, 116);
            this.comboBoxWT4.Name = "comboBoxWT4";
            this.comboBoxWT4.Size = new System.Drawing.Size(82, 20);
            this.comboBoxWT4.TabIndex = 22;
            // 
            // comboBoxPower4
            // 
            this.comboBoxPower4.FormattingEnabled = true;
            this.comboBoxPower4.Location = new System.Drawing.Point(191, 116);
            this.comboBoxPower4.Name = "comboBoxPower4";
            this.comboBoxPower4.Size = new System.Drawing.Size(70, 20);
            this.comboBoxPower4.TabIndex = 21;
            // 
            // comboBoxWT3
            // 
            this.comboBoxWT3.FormattingEnabled = true;
            this.comboBoxWT3.Location = new System.Drawing.Point(80, 82);
            this.comboBoxWT3.Name = "comboBoxWT3";
            this.comboBoxWT3.Size = new System.Drawing.Size(82, 20);
            this.comboBoxWT3.TabIndex = 20;
            // 
            // comboBoxPower3
            // 
            this.comboBoxPower3.FormattingEnabled = true;
            this.comboBoxPower3.Location = new System.Drawing.Point(191, 82);
            this.comboBoxPower3.Name = "comboBoxPower3";
            this.comboBoxPower3.Size = new System.Drawing.Size(70, 20);
            this.comboBoxPower3.TabIndex = 19;
            // 
            // comboBoxWT2
            // 
            this.comboBoxWT2.FormattingEnabled = true;
            this.comboBoxWT2.Location = new System.Drawing.Point(80, 49);
            this.comboBoxWT2.Name = "comboBoxWT2";
            this.comboBoxWT2.Size = new System.Drawing.Size(82, 20);
            this.comboBoxWT2.TabIndex = 18;
            // 
            // comboBoxPower2
            // 
            this.comboBoxPower2.FormattingEnabled = true;
            this.comboBoxPower2.Location = new System.Drawing.Point(191, 49);
            this.comboBoxPower2.Name = "comboBoxPower2";
            this.comboBoxPower2.Size = new System.Drawing.Size(70, 20);
            this.comboBoxPower2.TabIndex = 17;
            // 
            // comboBoxWT1
            // 
            this.comboBoxWT1.FormattingEnabled = true;
            this.comboBoxWT1.Location = new System.Drawing.Point(80, 17);
            this.comboBoxWT1.Name = "comboBoxWT1";
            this.comboBoxWT1.Size = new System.Drawing.Size(82, 20);
            this.comboBoxWT1.TabIndex = 16;
            // 
            // comboBoxPower1
            // 
            this.comboBoxPower1.FormattingEnabled = true;
            this.comboBoxPower1.Location = new System.Drawing.Point(191, 16);
            this.comboBoxPower1.Name = "comboBoxPower1";
            this.comboBoxPower1.Size = new System.Drawing.Size(70, 20);
            this.comboBoxPower1.TabIndex = 15;
            // 
            // cbAnt4
            // 
            this.cbAnt4.AutoSize = true;
            this.cbAnt4.Location = new System.Drawing.Point(8, 118);
            this.cbAnt4.Name = "cbAnt4";
            this.cbAnt4.Size = new System.Drawing.Size(42, 16);
            this.cbAnt4.TabIndex = 13;
            this.cbAnt4.Text = "4号";
            this.cbAnt4.UseVisualStyleBackColor = true;
            // 
            // cbAnt3
            // 
            this.cbAnt3.AutoSize = true;
            this.cbAnt3.Location = new System.Drawing.Point(8, 86);
            this.cbAnt3.Name = "cbAnt3";
            this.cbAnt3.Size = new System.Drawing.Size(42, 16);
            this.cbAnt3.TabIndex = 12;
            this.cbAnt3.Text = "3号";
            this.cbAnt3.UseVisualStyleBackColor = true;
            // 
            // cbAnt2
            // 
            this.cbAnt2.AutoSize = true;
            this.cbAnt2.Location = new System.Drawing.Point(8, 51);
            this.cbAnt2.Name = "cbAnt2";
            this.cbAnt2.Size = new System.Drawing.Size(42, 16);
            this.cbAnt2.TabIndex = 11;
            this.cbAnt2.Text = "2号";
            this.cbAnt2.UseVisualStyleBackColor = true;
            // 
            // cbAnt1
            // 
            this.cbAnt1.AutoSize = true;
            this.cbAnt1.Checked = true;
            this.cbAnt1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAnt1.Location = new System.Drawing.Point(8, 20);
            this.cbAnt1.Name = "cbAnt1";
            this.cbAnt1.Size = new System.Drawing.Size(42, 16);
            this.cbAnt1.TabIndex = 10;
            this.cbAnt1.Text = "1号";
            this.cbAnt1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rb8Ant);
            this.groupBox1.Controls.Add(this.rb32Ant);
            this.groupBox1.Controls.Add(this.rb16Ant);
            this.groupBox1.Controls.Add(this.MultiAntPowerTB);
            this.groupBox1.Controls.Add(this.MultiAntTime);
            this.groupBox1.Controls.Add(this.MultiAntPower);
            this.groupBox1.Controls.Add(this.MultiAntWorktime);
            this.groupBox1.Controls.Add(this.checkBox89);
            this.groupBox1.Controls.Add(this.checkBox90);
            this.groupBox1.Controls.Add(this.checkBox91);
            this.groupBox1.Controls.Add(this.checkBox95);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.checkBox64);
            this.groupBox1.Controls.Add(this.checkBox65);
            this.groupBox1.Controls.Add(this.checkBox66);
            this.groupBox1.Controls.Add(this.checkBox67);
            this.groupBox1.Controls.Add(this.checkBox68);
            this.groupBox1.Controls.Add(this.checkBox69);
            this.groupBox1.Controls.Add(this.checkBox70);
            this.groupBox1.Controls.Add(this.checkBox71);
            this.groupBox1.Controls.Add(this.checkBox80);
            this.groupBox1.Controls.Add(this.checkBox81);
            this.groupBox1.Controls.Add(this.checkBox82);
            this.groupBox1.Controls.Add(this.checkBox83);
            this.groupBox1.Controls.Add(this.checkBox84);
            this.groupBox1.Controls.Add(this.checkBox85);
            this.groupBox1.Controls.Add(this.checkBox86);
            this.groupBox1.Controls.Add(this.checkBox87);
            this.groupBox1.Controls.Add(this.checkBox72);
            this.groupBox1.Controls.Add(this.checkBox73);
            this.groupBox1.Controls.Add(this.checkBox74);
            this.groupBox1.Controls.Add(this.checkBox75);
            this.groupBox1.Controls.Add(this.checkBox76);
            this.groupBox1.Controls.Add(this.checkBox77);
            this.groupBox1.Controls.Add(this.checkBox78);
            this.groupBox1.Controls.Add(this.checkBox79);
            this.groupBox1.Controls.Add(this.checkBox60);
            this.groupBox1.Controls.Add(this.checkBox61);
            this.groupBox1.Controls.Add(this.checkBox62);
            this.groupBox1.Controls.Add(this.checkBox63);
            this.groupBox1.Controls.Add(this.checkBox58);
            this.groupBox1.Controls.Add(this.checkBox59);
            this.groupBox1.Controls.Add(this.checkBox57);
            this.groupBox1.Controls.Add(this.checkBox56);
            this.groupBox1.Location = new System.Drawing.Point(4, 212);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(313, 341);
            this.groupBox1.TabIndex = 51;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "通道天线";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // rb8Ant
            // 
            this.rb8Ant.AutoSize = true;
            this.rb8Ant.Location = new System.Drawing.Point(83, 0);
            this.rb8Ant.Name = "rb8Ant";
            this.rb8Ant.Size = new System.Drawing.Size(53, 16);
            this.rb8Ant.TabIndex = 121;
            this.rb8Ant.TabStop = true;
            this.rb8Ant.Text = "8通道";
            this.rb8Ant.UseVisualStyleBackColor = true;
            // 
            // rb32Ant
            // 
            this.rb32Ant.AutoSize = true;
            this.rb32Ant.Checked = true;
            this.rb32Ant.Location = new System.Drawing.Point(240, 1);
            this.rb32Ant.Name = "rb32Ant";
            this.rb32Ant.Size = new System.Drawing.Size(59, 16);
            this.rb32Ant.TabIndex = 120;
            this.rb32Ant.TabStop = true;
            this.rb32Ant.Text = "32通道";
            this.rb32Ant.UseVisualStyleBackColor = true;
            // 
            // rb16Ant
            // 
            this.rb16Ant.AutoSize = true;
            this.rb16Ant.Location = new System.Drawing.Point(161, 0);
            this.rb16Ant.Name = "rb16Ant";
            this.rb16Ant.Size = new System.Drawing.Size(59, 16);
            this.rb16Ant.TabIndex = 119;
            this.rb16Ant.Text = "16通道";
            this.rb16Ant.UseVisualStyleBackColor = true;
            // 
            // MultiAntPowerTB
            // 
            this.MultiAntPowerTB.FormattingEnabled = true;
            this.MultiAntPowerTB.Location = new System.Drawing.Point(99, 310);
            this.MultiAntPowerTB.Name = "MultiAntPowerTB";
            this.MultiAntPowerTB.Size = new System.Drawing.Size(54, 20);
            this.MultiAntPowerTB.TabIndex = 31;
            // 
            // MultiAntTime
            // 
            this.MultiAntTime.FormattingEnabled = true;
            this.MultiAntTime.Location = new System.Drawing.Point(99, 277);
            this.MultiAntTime.Name = "MultiAntTime";
            this.MultiAntTime.Size = new System.Drawing.Size(82, 20);
            this.MultiAntTime.TabIndex = 31;
            // 
            // MultiAntPower
            // 
            this.MultiAntPower.AutoSize = true;
            this.MultiAntPower.Location = new System.Drawing.Point(9, 316);
            this.MultiAntPower.Name = "MultiAntPower";
            this.MultiAntPower.Size = new System.Drawing.Size(29, 12);
            this.MultiAntPower.TabIndex = 118;
            this.MultiAntPower.Text = "功率";
            // 
            // MultiAntWorktime
            // 
            this.MultiAntWorktime.AutoSize = true;
            this.MultiAntWorktime.Location = new System.Drawing.Point(7, 280);
            this.MultiAntWorktime.Name = "MultiAntWorktime";
            this.MultiAntWorktime.Size = new System.Drawing.Size(53, 12);
            this.MultiAntWorktime.TabIndex = 117;
            this.MultiAntWorktime.Text = "工作时间";
            // 
            // checkBox89
            // 
            this.checkBox89.AutoSize = true;
            this.checkBox89.Location = new System.Drawing.Point(258, 210);
            this.checkBox89.Name = "checkBox89";
            this.checkBox89.Size = new System.Drawing.Size(48, 16);
            this.checkBox89.TabIndex = 116;
            this.checkBox89.Text = "全选";
            this.checkBox89.UseVisualStyleBackColor = true;
            this.checkBox89.CheckedChanged += new System.EventHandler(this.checkBox89_CheckedChanged);
            // 
            // checkBox90
            // 
            this.checkBox90.AutoSize = true;
            this.checkBox90.Location = new System.Drawing.Point(258, 150);
            this.checkBox90.Name = "checkBox90";
            this.checkBox90.Size = new System.Drawing.Size(48, 16);
            this.checkBox90.TabIndex = 115;
            this.checkBox90.Text = "全选";
            this.checkBox90.UseVisualStyleBackColor = true;
            this.checkBox90.CheckedChanged += new System.EventHandler(this.checkBox90_CheckedChanged);
            // 
            // checkBox91
            // 
            this.checkBox91.AutoSize = true;
            this.checkBox91.Location = new System.Drawing.Point(258, 90);
            this.checkBox91.Name = "checkBox91";
            this.checkBox91.Size = new System.Drawing.Size(48, 16);
            this.checkBox91.TabIndex = 114;
            this.checkBox91.Text = "全选";
            this.checkBox91.UseVisualStyleBackColor = true;
            this.checkBox91.CheckedChanged += new System.EventHandler(this.checkBox91_CheckedChanged);
            // 
            // checkBox95
            // 
            this.checkBox95.AutoSize = true;
            this.checkBox95.Location = new System.Drawing.Point(258, 30);
            this.checkBox95.Name = "checkBox95";
            this.checkBox95.Size = new System.Drawing.Size(48, 16);
            this.checkBox95.TabIndex = 110;
            this.checkBox95.Text = "全选";
            this.checkBox95.UseVisualStyleBackColor = true;
            this.checkBox95.CheckedChanged += new System.EventHandler(this.checkBox95_CheckedChanged);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(224, 304);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 30);
            this.button6.TabIndex = 109;
            this.button6.Text = "设置";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(224, 267);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 31);
            this.button4.TabIndex = 109;
            this.button4.Text = "读取";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // checkBox64
            // 
            this.checkBox64.AutoSize = true;
            this.checkBox64.Location = new System.Drawing.Point(187, 240);
            this.checkBox64.Name = "checkBox64";
            this.checkBox64.Size = new System.Drawing.Size(54, 16);
            this.checkBox64.TabIndex = 39;
            this.checkBox64.Text = "Ant32";
            this.checkBox64.UseVisualStyleBackColor = true;
            // 
            // checkBox65
            // 
            this.checkBox65.AutoSize = true;
            this.checkBox65.Location = new System.Drawing.Point(127, 240);
            this.checkBox65.Name = "checkBox65";
            this.checkBox65.Size = new System.Drawing.Size(54, 16);
            this.checkBox65.TabIndex = 38;
            this.checkBox65.Text = "Ant31";
            this.checkBox65.UseVisualStyleBackColor = true;
            // 
            // checkBox66
            // 
            this.checkBox66.AutoSize = true;
            this.checkBox66.Location = new System.Drawing.Point(67, 240);
            this.checkBox66.Name = "checkBox66";
            this.checkBox66.Size = new System.Drawing.Size(54, 16);
            this.checkBox66.TabIndex = 37;
            this.checkBox66.Text = "Ant30";
            this.checkBox66.UseVisualStyleBackColor = true;
            // 
            // checkBox67
            // 
            this.checkBox67.AutoSize = true;
            this.checkBox67.Location = new System.Drawing.Point(7, 240);
            this.checkBox67.Name = "checkBox67";
            this.checkBox67.Size = new System.Drawing.Size(54, 16);
            this.checkBox67.TabIndex = 36;
            this.checkBox67.Text = "Ant29";
            this.checkBox67.UseVisualStyleBackColor = true;
            // 
            // checkBox68
            // 
            this.checkBox68.AutoSize = true;
            this.checkBox68.Location = new System.Drawing.Point(187, 210);
            this.checkBox68.Name = "checkBox68";
            this.checkBox68.Size = new System.Drawing.Size(54, 16);
            this.checkBox68.TabIndex = 35;
            this.checkBox68.Text = "Ant28";
            this.checkBox68.UseVisualStyleBackColor = true;
            // 
            // checkBox69
            // 
            this.checkBox69.AutoSize = true;
            this.checkBox69.Location = new System.Drawing.Point(127, 210);
            this.checkBox69.Name = "checkBox69";
            this.checkBox69.Size = new System.Drawing.Size(54, 16);
            this.checkBox69.TabIndex = 34;
            this.checkBox69.Text = "Ant27";
            this.checkBox69.UseVisualStyleBackColor = true;
            // 
            // checkBox70
            // 
            this.checkBox70.AutoSize = true;
            this.checkBox70.Location = new System.Drawing.Point(67, 210);
            this.checkBox70.Name = "checkBox70";
            this.checkBox70.Size = new System.Drawing.Size(54, 16);
            this.checkBox70.TabIndex = 33;
            this.checkBox70.Text = "Ant26";
            this.checkBox70.UseVisualStyleBackColor = true;
            // 
            // checkBox71
            // 
            this.checkBox71.AutoSize = true;
            this.checkBox71.Location = new System.Drawing.Point(7, 210);
            this.checkBox71.Name = "checkBox71";
            this.checkBox71.Size = new System.Drawing.Size(54, 16);
            this.checkBox71.TabIndex = 32;
            this.checkBox71.Text = "Ant25";
            this.checkBox71.UseVisualStyleBackColor = true;
            // 
            // checkBox80
            // 
            this.checkBox80.AutoSize = true;
            this.checkBox80.Location = new System.Drawing.Point(187, 180);
            this.checkBox80.Name = "checkBox80";
            this.checkBox80.Size = new System.Drawing.Size(54, 16);
            this.checkBox80.TabIndex = 31;
            this.checkBox80.Text = "Ant24";
            this.checkBox80.UseVisualStyleBackColor = true;
            // 
            // checkBox81
            // 
            this.checkBox81.AutoSize = true;
            this.checkBox81.Location = new System.Drawing.Point(127, 180);
            this.checkBox81.Name = "checkBox81";
            this.checkBox81.Size = new System.Drawing.Size(54, 16);
            this.checkBox81.TabIndex = 30;
            this.checkBox81.Text = "Ant23";
            this.checkBox81.UseVisualStyleBackColor = true;
            // 
            // checkBox82
            // 
            this.checkBox82.AutoSize = true;
            this.checkBox82.Location = new System.Drawing.Point(67, 180);
            this.checkBox82.Name = "checkBox82";
            this.checkBox82.Size = new System.Drawing.Size(54, 16);
            this.checkBox82.TabIndex = 29;
            this.checkBox82.Text = "Ant22";
            this.checkBox82.UseVisualStyleBackColor = true;
            // 
            // checkBox83
            // 
            this.checkBox83.AutoSize = true;
            this.checkBox83.Location = new System.Drawing.Point(7, 180);
            this.checkBox83.Name = "checkBox83";
            this.checkBox83.Size = new System.Drawing.Size(54, 16);
            this.checkBox83.TabIndex = 28;
            this.checkBox83.Text = "Ant21";
            this.checkBox83.UseVisualStyleBackColor = true;
            // 
            // checkBox84
            // 
            this.checkBox84.AutoSize = true;
            this.checkBox84.Location = new System.Drawing.Point(187, 150);
            this.checkBox84.Name = "checkBox84";
            this.checkBox84.Size = new System.Drawing.Size(54, 16);
            this.checkBox84.TabIndex = 27;
            this.checkBox84.Text = "Ant20";
            this.checkBox84.UseVisualStyleBackColor = true;
            this.checkBox84.CheckedChanged += new System.EventHandler(this.checkBox84_CheckedChanged);
            // 
            // checkBox85
            // 
            this.checkBox85.AutoSize = true;
            this.checkBox85.Location = new System.Drawing.Point(127, 150);
            this.checkBox85.Name = "checkBox85";
            this.checkBox85.Size = new System.Drawing.Size(54, 16);
            this.checkBox85.TabIndex = 26;
            this.checkBox85.Text = "Ant19";
            this.checkBox85.UseVisualStyleBackColor = true;
            this.checkBox85.CheckedChanged += new System.EventHandler(this.checkBox85_CheckedChanged);
            // 
            // checkBox86
            // 
            this.checkBox86.AutoSize = true;
            this.checkBox86.Location = new System.Drawing.Point(67, 150);
            this.checkBox86.Name = "checkBox86";
            this.checkBox86.Size = new System.Drawing.Size(54, 16);
            this.checkBox86.TabIndex = 25;
            this.checkBox86.Text = "Ant18";
            this.checkBox86.UseVisualStyleBackColor = true;
            this.checkBox86.CheckedChanged += new System.EventHandler(this.checkBox86_CheckedChanged);
            // 
            // checkBox87
            // 
            this.checkBox87.AutoSize = true;
            this.checkBox87.Location = new System.Drawing.Point(7, 150);
            this.checkBox87.Name = "checkBox87";
            this.checkBox87.Size = new System.Drawing.Size(54, 16);
            this.checkBox87.TabIndex = 24;
            this.checkBox87.Text = "Ant17";
            this.checkBox87.UseVisualStyleBackColor = true;
            this.checkBox87.CheckedChanged += new System.EventHandler(this.checkBox87_CheckedChanged);
            // 
            // checkBox72
            // 
            this.checkBox72.AutoSize = true;
            this.checkBox72.Location = new System.Drawing.Point(187, 120);
            this.checkBox72.Name = "checkBox72";
            this.checkBox72.Size = new System.Drawing.Size(54, 16);
            this.checkBox72.TabIndex = 23;
            this.checkBox72.Text = "Ant16";
            this.checkBox72.UseVisualStyleBackColor = true;
            // 
            // checkBox73
            // 
            this.checkBox73.AutoSize = true;
            this.checkBox73.Location = new System.Drawing.Point(127, 120);
            this.checkBox73.Name = "checkBox73";
            this.checkBox73.Size = new System.Drawing.Size(54, 16);
            this.checkBox73.TabIndex = 22;
            this.checkBox73.Text = "Ant15";
            this.checkBox73.UseVisualStyleBackColor = true;
            // 
            // checkBox74
            // 
            this.checkBox74.AutoSize = true;
            this.checkBox74.Location = new System.Drawing.Point(67, 120);
            this.checkBox74.Name = "checkBox74";
            this.checkBox74.Size = new System.Drawing.Size(54, 16);
            this.checkBox74.TabIndex = 21;
            this.checkBox74.Text = "Ant14";
            this.checkBox74.UseVisualStyleBackColor = true;
            // 
            // checkBox75
            // 
            this.checkBox75.AutoSize = true;
            this.checkBox75.Location = new System.Drawing.Point(7, 120);
            this.checkBox75.Name = "checkBox75";
            this.checkBox75.Size = new System.Drawing.Size(54, 16);
            this.checkBox75.TabIndex = 20;
            this.checkBox75.Text = "Ant13";
            this.checkBox75.UseVisualStyleBackColor = true;
            // 
            // checkBox76
            // 
            this.checkBox76.AutoSize = true;
            this.checkBox76.Location = new System.Drawing.Point(187, 90);
            this.checkBox76.Name = "checkBox76";
            this.checkBox76.Size = new System.Drawing.Size(54, 16);
            this.checkBox76.TabIndex = 19;
            this.checkBox76.Text = "Ant12";
            this.checkBox76.UseVisualStyleBackColor = true;
            this.checkBox76.CheckedChanged += new System.EventHandler(this.checkBox76_CheckedChanged);
            // 
            // checkBox77
            // 
            this.checkBox77.AutoSize = true;
            this.checkBox77.Location = new System.Drawing.Point(127, 90);
            this.checkBox77.Name = "checkBox77";
            this.checkBox77.Size = new System.Drawing.Size(54, 16);
            this.checkBox77.TabIndex = 18;
            this.checkBox77.Text = "Ant11";
            this.checkBox77.UseVisualStyleBackColor = true;
            this.checkBox77.CheckedChanged += new System.EventHandler(this.checkBox77_CheckedChanged);
            // 
            // checkBox78
            // 
            this.checkBox78.AutoSize = true;
            this.checkBox78.Location = new System.Drawing.Point(67, 90);
            this.checkBox78.Name = "checkBox78";
            this.checkBox78.Size = new System.Drawing.Size(54, 16);
            this.checkBox78.TabIndex = 17;
            this.checkBox78.Text = "Ant10";
            this.checkBox78.UseVisualStyleBackColor = true;
            this.checkBox78.CheckedChanged += new System.EventHandler(this.checkBox78_CheckedChanged);
            // 
            // checkBox79
            // 
            this.checkBox79.AutoSize = true;
            this.checkBox79.Location = new System.Drawing.Point(7, 90);
            this.checkBox79.Name = "checkBox79";
            this.checkBox79.Size = new System.Drawing.Size(54, 16);
            this.checkBox79.TabIndex = 16;
            this.checkBox79.Text = "Ant09";
            this.checkBox79.UseVisualStyleBackColor = true;
            this.checkBox79.CheckedChanged += new System.EventHandler(this.checkBox79_CheckedChanged);
            // 
            // checkBox60
            // 
            this.checkBox60.AutoSize = true;
            this.checkBox60.Location = new System.Drawing.Point(187, 60);
            this.checkBox60.Name = "checkBox60";
            this.checkBox60.Size = new System.Drawing.Size(54, 16);
            this.checkBox60.TabIndex = 7;
            this.checkBox60.Text = "Ant08";
            this.checkBox60.UseVisualStyleBackColor = true;
            // 
            // checkBox61
            // 
            this.checkBox61.AutoSize = true;
            this.checkBox61.Location = new System.Drawing.Point(127, 60);
            this.checkBox61.Name = "checkBox61";
            this.checkBox61.Size = new System.Drawing.Size(54, 16);
            this.checkBox61.TabIndex = 6;
            this.checkBox61.Text = "Ant07";
            this.checkBox61.UseVisualStyleBackColor = true;
            // 
            // checkBox62
            // 
            this.checkBox62.AutoSize = true;
            this.checkBox62.Location = new System.Drawing.Point(67, 60);
            this.checkBox62.Name = "checkBox62";
            this.checkBox62.Size = new System.Drawing.Size(54, 16);
            this.checkBox62.TabIndex = 5;
            this.checkBox62.Text = "Ant06";
            this.checkBox62.UseVisualStyleBackColor = true;
            // 
            // checkBox63
            // 
            this.checkBox63.AutoSize = true;
            this.checkBox63.Location = new System.Drawing.Point(7, 60);
            this.checkBox63.Name = "checkBox63";
            this.checkBox63.Size = new System.Drawing.Size(54, 16);
            this.checkBox63.TabIndex = 4;
            this.checkBox63.Text = "Ant05";
            this.checkBox63.UseVisualStyleBackColor = true;
            // 
            // checkBox58
            // 
            this.checkBox58.AutoSize = true;
            this.checkBox58.Location = new System.Drawing.Point(187, 30);
            this.checkBox58.Name = "checkBox58";
            this.checkBox58.Size = new System.Drawing.Size(54, 16);
            this.checkBox58.TabIndex = 3;
            this.checkBox58.Text = "Ant04";
            this.checkBox58.UseVisualStyleBackColor = true;
            // 
            // checkBox59
            // 
            this.checkBox59.AutoSize = true;
            this.checkBox59.Location = new System.Drawing.Point(127, 30);
            this.checkBox59.Name = "checkBox59";
            this.checkBox59.Size = new System.Drawing.Size(54, 16);
            this.checkBox59.TabIndex = 2;
            this.checkBox59.Text = "Ant03";
            this.checkBox59.UseVisualStyleBackColor = true;
            // 
            // checkBox57
            // 
            this.checkBox57.AutoSize = true;
            this.checkBox57.Location = new System.Drawing.Point(67, 30);
            this.checkBox57.Name = "checkBox57";
            this.checkBox57.Size = new System.Drawing.Size(54, 16);
            this.checkBox57.TabIndex = 1;
            this.checkBox57.Text = "Ant02";
            this.checkBox57.UseVisualStyleBackColor = true;
            // 
            // checkBox56
            // 
            this.checkBox56.AutoSize = true;
            this.checkBox56.Location = new System.Drawing.Point(7, 30);
            this.checkBox56.Name = "checkBox56";
            this.checkBox56.Size = new System.Drawing.Size(54, 16);
            this.checkBox56.TabIndex = 0;
            this.checkBox56.Text = "Ant01";
            this.checkBox56.UseVisualStyleBackColor = true;
            // 
            // AntForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(322, 554);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbAntParams);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AntForm";
            this.ShowIcon = false;
            this.Text = "读写器天线参数设置";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.gbAntParams.ResumeLayout(false);
            this.gbAntParams.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbAntParams;
        private System.Windows.Forms.Button GetAntStateBt;
        private System.Windows.Forms.Label labAnt;
        private System.Windows.Forms.Button btnSetAnts;
        private System.Windows.Forms.Button btnGetAnts;
        private System.Windows.Forms.ComboBox comboBoxWT4;
        private System.Windows.Forms.ComboBox comboBoxPower4;
        private System.Windows.Forms.ComboBox comboBoxWT3;
        private System.Windows.Forms.ComboBox comboBoxPower3;
        private System.Windows.Forms.ComboBox comboBoxWT2;
        private System.Windows.Forms.ComboBox comboBoxPower2;
        private System.Windows.Forms.ComboBox comboBoxWT1;
        private System.Windows.Forms.ComboBox comboBoxPower1;
        private System.Windows.Forms.CheckBox cbAnt4;
        private System.Windows.Forms.CheckBox cbAnt3;
        private System.Windows.Forms.CheckBox cbAnt2;
        private System.Windows.Forms.CheckBox cbAnt1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rb8Ant;
        private System.Windows.Forms.RadioButton rb32Ant;
        private System.Windows.Forms.RadioButton rb16Ant;
        private System.Windows.Forms.ComboBox MultiAntPowerTB;
        private System.Windows.Forms.ComboBox MultiAntTime;
        private System.Windows.Forms.Label MultiAntPower;
        private System.Windows.Forms.Label MultiAntWorktime;
        private System.Windows.Forms.CheckBox checkBox89;
        private System.Windows.Forms.CheckBox checkBox90;
        private System.Windows.Forms.CheckBox checkBox91;
        private System.Windows.Forms.CheckBox checkBox95;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.CheckBox checkBox64;
        private System.Windows.Forms.CheckBox checkBox65;
        private System.Windows.Forms.CheckBox checkBox66;
        private System.Windows.Forms.CheckBox checkBox67;
        private System.Windows.Forms.CheckBox checkBox68;
        private System.Windows.Forms.CheckBox checkBox69;
        private System.Windows.Forms.CheckBox checkBox70;
        private System.Windows.Forms.CheckBox checkBox71;
        private System.Windows.Forms.CheckBox checkBox80;
        private System.Windows.Forms.CheckBox checkBox81;
        private System.Windows.Forms.CheckBox checkBox82;
        private System.Windows.Forms.CheckBox checkBox83;
        private System.Windows.Forms.CheckBox checkBox84;
        private System.Windows.Forms.CheckBox checkBox85;
        private System.Windows.Forms.CheckBox checkBox86;
        private System.Windows.Forms.CheckBox checkBox87;
        private System.Windows.Forms.CheckBox checkBox72;
        private System.Windows.Forms.CheckBox checkBox73;
        private System.Windows.Forms.CheckBox checkBox74;
        private System.Windows.Forms.CheckBox checkBox75;
        private System.Windows.Forms.CheckBox checkBox76;
        private System.Windows.Forms.CheckBox checkBox77;
        private System.Windows.Forms.CheckBox checkBox78;
        private System.Windows.Forms.CheckBox checkBox79;
        private System.Windows.Forms.CheckBox checkBox60;
        private System.Windows.Forms.CheckBox checkBox61;
        private System.Windows.Forms.CheckBox checkBox62;
        private System.Windows.Forms.CheckBox checkBox63;
        private System.Windows.Forms.CheckBox checkBox58;
        private System.Windows.Forms.CheckBox checkBox59;
        private System.Windows.Forms.CheckBox checkBox57;
        private System.Windows.Forms.CheckBox checkBox56;
    }
}
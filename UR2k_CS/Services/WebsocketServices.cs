﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog.Fluent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UHFReader.Model;
using UR2k_CS.Models;
using Websocket.Client;

namespace UR2k_CS.Services
{
    public class WebsocketServices
    {
        private WebsocketClient client = null;
        private string _endPoint;

        public WebsocketServices(string endPoint)
        {
            _endPoint = endPoint;

            var exitEvent = new ManualResetEvent(false);

            client = new WebsocketClient(new Uri(_endPoint));
            client.ReconnectTimeout = TimeSpan.FromSeconds(30);
            client.ReconnectionHappened.Subscribe(info =>
                Log.Info($"Reconnection happened, type: {info.Type}"));

            client.MessageReceived
                .Where(msg => msg.Text != null)
                .Where(msg => {
                    dynamic json = JValue.Parse(msg.Text);
                    
                    return !json.message.GetType().ToString().Contains("JArray");
                })
                .Subscribe(onMessageReceived);

            client.Start();
            //Task.Run(() => client.Send("{ message }"));
            //exitEvent.WaitOne();
        }

        public void Dispose()
        {
            client?.Dispose();
        }

        public Action<EnumSDKActions> OnReceive; //= (EnumSDKActions action) => { };

        public async Task NotifyToRedis(List<Tag> items)
        {
            //while (webSocket.State == WebSocketState.Open)
            //{
            //    await webSocket.SendAsync(EncodeBody(items), WebSocketMessageType.Text, true, CancellationToken.None);
            //    await Task.Delay(1000);
            //}

            await Task.Run(() => client.Send(JsonConvert.SerializeObject(items)));
        }

        private void onMessageReceived(ResponseMessage message)
        {
            var requestBody = JsonConvert.DeserializeObject<WebSocketCommunicationModel<ActionModel>>(message.Text);
            Log.Debug($"From service: {requestBody.Message.Action}");
            if(Enum.TryParse(requestBody.Message.Action, out EnumSDKActions action))
            {
                OnReceive?.Invoke(action);
            }
        }

        private ArraySegment<byte> EncodeBody(List<Tag> items)
        {
            return new ArraySegment<byte>(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(items)));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace UR2k_CS
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MainWindow UHFForm = new MainWindow();
            UHFForm.StartPosition = FormStartPosition.Manual;
            Application.Run(UHFForm);
            System.Environment.Exit(0);
        }
    }
}

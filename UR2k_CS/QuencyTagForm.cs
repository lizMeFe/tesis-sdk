﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using UHFReader;
using UHFReader.Model;

namespace UR2k_CS
{
    public partial class QuencyTagForm : Form
    {
        public QuencyTagForm()
        {
            InitializeComponent();
        }

        MainWindow  form;
        public QuencyTagForm(MainWindow form)
        {
            InitializeComponent();
            this.form = form;
        }

        public QuencyTagForm(Reader reader, MainWindow form, Client currentClient, ResourceManager rm) : this(form)
        {
            this.currentClient = currentClient;
            this.rm = rm;
            this.reader = reader;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void QuencyTagForm_Load(object sender, EventArgs e)
        {

        }

        Func<Tag, bool> whereLambda;
      //  Func<Tag, int> orderLambda;
        private void button1_Click(object sender, EventArgs e)
        {
            if (form != null)
            {
                form.StopUpdate();
            }
            lock (MainWindow.Tag_data)
            {
                switch (QuecnyObjTb.Text)
                {
                    case "TID":
                    case "EPC":
                        QuencyEpc();
                        break;           
                    case "Rssi":
                        QuencyRssi();
                        break;
                    case "Ant":
                        QuencyAnt();
                        break;
                    case "Dir":
                        QuencyDir();
                        break;
                    case "Dev":
                        QuencyDev();
                        break;
                    default:
                        break;
                }
            }
            form.UpdateQuencyResult(Quency(whereLambda));
        }

        //这部分代码可以简化  利用反射 类索引？
        private void QuencyEpc()
        {
            if (equal.Checked == true && greater.Checked == false && less.Checked == false)   //等于
            {
                whereLambda = a=>a.EPC == QuecnyCondition1Tb.Text;
            }
            else if(equal.Checked == false && greater.Checked == true && less.Checked == false)       //大于
            {
                whereLambda = a => a.EPC.CompareTo(QuecnyCondition2Tb.Text) >1?true:false;
            }
            else if (equal.Checked == false && greater.Checked == false && less.Checked == true)       //小于
            {
                whereLambda = a => a.EPC.CompareTo(QuecnyCondition3Tb.Text) < 1 ? true : false;
            }
            else if (equal.Checked == true && greater.Checked == false && less.Checked == true)       //小于等于
            {
                whereLambda = a => a.EPC.CompareTo(QuecnyCondition3Tb.Text) > 1 ? false : true;
            }
            else if (equal.Checked == true && greater.Checked == true && less.Checked == false)       //大于等于
            {
                whereLambda = a => a.EPC.CompareTo(QuecnyCondition2Tb.Text) < 1 ? false : true;
            }
            else if (equal.Checked == false && greater.Checked == true && less.Checked == true)       //不等于
            {
                whereLambda = a => a.EPC != QuecnyCondition1Tb.Text;
            }
        }

        private void QuencyDir()
        {
            if (equal.Checked == true && greater.Checked == false && less.Checked == false)   //等于
            {
                whereLambda = a => a.direction == byte.Parse(QuecnyCondition1Tb.Text);
            }
            else if (equal.Checked == false && greater.Checked == true && less.Checked == false)       //大于
            {
                whereLambda = a => a.direction > byte.Parse(QuecnyCondition2Tb.Text);
            }
            else if (equal.Checked == false && greater.Checked == false && less.Checked == true)       //小于
            {
                whereLambda = a => a.direction < byte.Parse(QuecnyCondition3Tb.Text);
            }
            else if (equal.Checked == true && greater.Checked == false && less.Checked == true)       //小于等于
            {
                whereLambda = a => a.direction <= byte.Parse(QuecnyCondition3Tb.Text);
            }
            else if (equal.Checked == true && greater.Checked == true && less.Checked == false)       //大于等于
            {
                whereLambda = a => a.direction >= byte.Parse(QuecnyCondition2Tb.Text);
            }
            else if (equal.Checked == false && greater.Checked == true && less.Checked == true)       //不等于
            {
                whereLambda = a => a.direction != byte.Parse(QuecnyCondition1Tb.Text);
            }
        }

        private void QuencyAnt()
        {
            if (equal.Checked == true && greater.Checked == false && less.Checked == false)   //等于
            {
                whereLambda = a => a.ant == byte.Parse(QuecnyCondition1Tb.Text);
            }
            else if (equal.Checked == false && greater.Checked == true && less.Checked == false)       //大于
            {
                whereLambda = a => a.ant > byte.Parse(QuecnyCondition2Tb.Text);
            }
            else if (equal.Checked == false && greater.Checked == false && less.Checked == true)       //小于
            {
                whereLambda = a => a.ant < byte.Parse(QuecnyCondition3Tb.Text);
            }
            else if (equal.Checked == true && greater.Checked == false && less.Checked == true)       //小于等于
            {
                whereLambda = a => a.ant <= byte.Parse(QuecnyCondition3Tb.Text);
            }
            else if (equal.Checked == true && greater.Checked == true && less.Checked == false)       //大于等于
            {
                whereLambda = a => a.ant >= byte.Parse(QuecnyCondition2Tb.Text);
            }
            else if (equal.Checked == false && greater.Checked == true && less.Checked == true)       //不等于
            {
                whereLambda = a => a.ant != byte.Parse(QuecnyCondition1Tb.Text);
            }
        }

        private void QuencyRssi()
        {
            if (equal.Checked == true && greater.Checked == false && less.Checked == false)   //等于
            {
                whereLambda = a => a.rssi == byte.Parse(QuecnyCondition1Tb.Text);
            }
            else if (equal.Checked == false && greater.Checked == true && less.Checked == false)       //大于
            {
                whereLambda = a => a.rssi > byte.Parse(QuecnyCondition2Tb.Text);
            }
            else if (equal.Checked == false && greater.Checked == false && less.Checked == true)       //小于
            {
                whereLambda = a => a.rssi < byte.Parse(QuecnyCondition3Tb.Text);
            }
            else if (equal.Checked == true && greater.Checked == false && less.Checked == true)       //小于等于
            {
                whereLambda = a => a.rssi <= byte.Parse(QuecnyCondition3Tb.Text);
            }
            else if (equal.Checked == true && greater.Checked == true && less.Checked == false)       //大于等于
            {
                whereLambda = a => a.rssi >= byte.Parse(QuecnyCondition2Tb.Text);
            }
            else if (equal.Checked == false && greater.Checked == true && less.Checked == true)       //不等于
            {
                whereLambda = a => a.rssi != byte.Parse(QuecnyCondition1Tb.Text);
            }
        }


        private void QuencyDev()
        {
            if (equal.Checked == true && greater.Checked == false && less.Checked == false)   //等于
            {
                whereLambda = a => a.device == byte.Parse(QuecnyCondition1Tb.Text);
            }
            else if (equal.Checked == false && greater.Checked == true && less.Checked == false)       //大于
            {
                whereLambda = a => a.device > byte.Parse(QuecnyCondition2Tb.Text);
            }
            else if (equal.Checked == false && greater.Checked == false && less.Checked == true)       //小于
            {
                whereLambda = a => a.device < byte.Parse(QuecnyCondition3Tb.Text);
            }
            else if (equal.Checked == true && greater.Checked == false && less.Checked == true)       //小于等于
            {
                whereLambda = a => a.device <= byte.Parse(QuecnyCondition3Tb.Text);
            }
            else if (equal.Checked == true && greater.Checked == true && less.Checked == false)       //大于等于
            {
                whereLambda = a => a.device >= byte.Parse(QuecnyCondition2Tb.Text);
            }
            else if (equal.Checked == false && greater.Checked == true && less.Checked == true)       //不等于
            {
                whereLambda = a => a.device != byte.Parse(QuecnyCondition1Tb.Text);
            }
        }

        private List<Tag> Quency(Func<Tag, bool> whereLambda)
        {
              return MainWindow.Tag_data.Where(whereLambda).ToList();

                 //    return MainWindow.Tag_data.Where(a => a.ant == 0).ToList();

           // return MainWindow.Tag_data.Where(whereLambda).ToList();
        }
        AutoReadCard auto;
        private bool autoModeIsFinish = true;
        private Client currentClient;
        private ResourceManager rm;
        private Reader reader;
        private int counts = 0;

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                auto  = new AutoReadCard();
                auto.mode = (byte)(comboBox1.SelectedIndex+1);
                auto.time = int.Parse(textBox1.Text);
                auto.counts = int.Parse(textBox2.Text);
                auto.save = 0x00;
                if (checkBox1.Checked)
                    auto.save |= 0x01;
                if(checkBox2.Checked)
                    auto.save |= 0x02;
                form.StartAutoReadCard(auto);
               // autoReadTimer.Interval = auto.time;
              //  autoReadTimer.Enabled = true;
                autoModeIsFinish = false;
                StartARound();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        

        private void autoReadTimer_Tick(object sender, EventArgs e)
        {
            if (auto != null)
            {
                if (auto.mode == 0x01)       //单次寻卡
                {
                    form.StartInvOnce();
                    if (counts < auto.counts)
                    {
                        reader.InvOnce(currentClient, 0x00);
                        counts++;
                        label5.Text = counts.ToString();
                    }
                    else
                    {
                        autoModeIsFinish = true;
                    }
                }
                else if (auto.mode == 0x02)    //循环寻卡
                {

                }
            }
        }

        private void QuencyTagForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!autoModeIsFinish)
            {
                if (MessageBox.Show(rm.GetString("strExit"), rm.GetString("strExitTips"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                    BackToNormalmOde();
                }
            }
            else
            {
                BackToNormalmOde();
            }
        }

        private void BackToNormalmOde()
        {
            auto = new AutoReadCard();
            auto.mode = (byte)(comboBox1.SelectedIndex + 1);
            auto.time = int.Parse(textBox1.Text);
            auto.counts = int.Parse(textBox2.Text);
            auto.save = 0x00;
            if (checkBox1.Checked)
                auto.save |= 0x01;
            if (checkBox2.Checked)
                auto.save |= 0x02;
            form.StartAutoReadCard(auto);
        }

        internal void StartARound()
        {
            Task Roundtask = new Task(new Action(NewTaskInvOne));
            Roundtask.Start();
        }

        private void NewTaskInvOne()
        {
            Thread.Sleep(auto.time);
            if (auto != null)
            {
                if (auto.mode == 0x01)       //单次寻卡
                {
                    form.StartInvOnce();
                    if (counts < auto.counts)
                    {
                        reader.InvOnce(currentClient, 0x00);
                        counts++;
                        label5.Text = counts.ToString();
                    }
                    else
                    {
                        autoModeIsFinish = true;
                    }
                }
                else if (auto.mode == 0x02)    //循环寻卡
                {

                }
            }
        }
    }
}

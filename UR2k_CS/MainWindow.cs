﻿using System;
using System.Collections.Generic;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Resources;

using System.Net;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO.Ports;
using UHFReader;
using UHFReader.Model;
using System.Configuration;
using static UHFReader.Filter;
using System.Threading.Tasks;
using UR2k_CS.Models;
using UR2k_CS.Services;
using UR2k_CS.Mock;

//using System.Diagnostics.Stopwatch;//<llp>

namespace UR2k_CS
{
    public partial class MainWindow : Form
    {
        [DllImport("ws2_32.dll")]
        public static extern Int32 WSAStartup(UInt16 wVersionRequested, ref WSADATA wsaData);
        [DllImport("ws2_32.dll")]
        public static extern Int32 WSACleanup();

        internal void StopUpdate()
        {
            this.ReadClock1.Enabled = false;
        }

        private ListViewNF lvTagData;
        public const int MAX_NUMBER_DEVICES = 20;// 可以连接的最多设备数
        Device currentDevice = new Device();



        //检测USB拔出的消息
        [StructLayout(LayoutKind.Sequential)]
        struct DEV_BROADCAST_HDR
        {
            public UInt32 dbch_size;
            public UInt32 dbch_devicetype;
            public UInt32 dbch_reserved;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct DEV_BROADCAST_VOLUME
        {
            public UInt32 dbcv_size;
            public UInt32 dbcv_devicetype;
            public UInt32 dbcv_reserved;
            public UInt32 dbcv_unitmask;
            public UInt16 dbcv_flags;
        }


        private string noDeviceChoose;
        bool NetConnectState = false;

        internal void UpdateQuencyResult(List<Tag> list)
        {
            bool isNewTag = true;
            int readCounts = 0;
            lvTagData.Items.Clear();
            lock (list)
            {
                foreach (Tag epcData in list)
                {
                    readCounts += epcData.counts;
                    lock (lvTagData.Items)
                    {
                        for (int index = 0; index < lvTagData.Items.Count; index++)
                        {
                            if (epcData.EPC == lvTagData.Items[index].SubItems[1].Text && epcData.IP == lvTagData.Items[index].SubItems[7].Text)
                            {
                                lvTagData.Items[index].SubItems[2].Text = epcData.counts.ToString();
                                if (epcData.isRssi)
                                    lvTagData.Items[index].SubItems[3].Text = epcData.rssi.ToString();
                                if (epcData.isAnt)
                                    lvTagData.Items[index].SubItems[4].Text = epcData.ant.ToString();
                                if (epcData.isDir)
                                    lvTagData.Items[index].SubItems[5].Text = epcData.direction.ToString();
                                if (epcData.isDev)
                                    lvTagData.Items[index].SubItems[6].Text = epcData.device.ToString();
                                isNewTag = false;
                                break;
                            }
                        }
                        if (isNewTag)
                        {
                            ListViewItem lvi = new ListViewItem();
                            lvi.Text = (lvTagData.Items.Count + 1).ToString();
                            lvi.SubItems.Add(epcData.EPC);
                            lvi.SubItems.Add(epcData.counts.ToString());
                            if (epcData.isRssi == false)
                                lvi.SubItems.Add("");
                            else
                                lvi.SubItems.Add(epcData.rssi.ToString());
                            if (epcData.isAnt == false)
                                lvi.SubItems.Add("");
                            else
                                lvi.SubItems.Add(epcData.ant.ToString());
                            if (epcData.isDir == false)
                                lvi.SubItems.Add("");
                            else
                                lvi.SubItems.Add(epcData.direction.ToString("X2"));
                            if (epcData.isDev == false)
                                lvi.SubItems.Add("");
                            else
                                lvi.SubItems.Add(epcData.device.ToString());
                            lvi.SubItems.Add(epcData.IP);
                            lvTagData.Items.Add(lvi);
                        }
                        isNewTag = true;
                    }
                }
                readCountsLB.Text = readCounts.ToString();
                labelTagCount.Text = list.Count.ToString();// 更新标签计数
                readLastCounts = readCounts;
            }
        }

        bool COMConnectState = false;
        /// <summary>
        /// 检测串口断开的方法 具体实现略
        /// </summary>
        /// <param name="m"></param>




        int RWBank = 0;// 记录选择的读写区域及开始、结束地址，便于更新控件
        int addStart = 0;
        int addEnd = 0;

        // 设备连接定时器
        System.Timers.Timer connectTimer = new System.Timers.Timer(2000);
        public delegate void DeleConnectDev(int i);

        public static List<Tag> Tag_data = new List<Tag>();
        static bool bNewTag = false;

        // 数据产生时，触发此事件，更新ListView控件
        public delegate void UpdateControlEventHandler();
        public static event UpdateControlEventHandler UpdateControl;

        System.Diagnostics.Stopwatch sw;//<llp>

        private Client currentClient = null;
        Reader reader = new Reader();
        List<Client> clientList = new List<Client>(10);

        CheckBox[] frequencysCB;
        CheckBox[] multiAntCB;

        static ResourceManager[] rmArray = new ResourceManager[2]{
                                                    new ResourceManager("UR2k_CS.SimpChinese", typeof(MainWindow).Assembly),
                                                    new ResourceManager("UR2k_CS.English", typeof(MainWindow).Assembly)};

        private readonly WebsocketServices services;
        public MainWindow()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;


            reader.analysis.comamndReplayEvent += DisPlayData;           //订阅读写器正常模式下数据
            reader.analysis.filter.intelligentDetectionEvent += intelligentDetectionMsg;
            //语言选择
            if (ConfigurationManager.AppSettings["Language"] == "0")
            {
                rm = rmArray[0];
                language = 0x00;
                this.Font = new Font("黑体", (float)10.0, FontStyle.Regular);
            }
            else if (ConfigurationManager.AppSettings["Language"] == "1")
            {
                rm = rmArray[1];
                language = 0x01;
                this.Font = new Font("黑体", (float)10.0, FontStyle.Regular);
            }
            else
            {
                rm = rmArray[0];
                language = 0x00;
            }                 //默认中文简体
            noDeviceChoose = rm.GetString("strChooseDevice");

            // 初始化各个页面控件
            InitGeneralControl();
            InitAccessTagControl();
            InitCommParamControl();
            InitDeviceParamControl();
            InitfrequencyCheckBox();

            UpdateView();

            //一些特殊事件
            comboBoxPower1.LostFocus += comboBoxPower1LostFocus;
            comboBoxPower2.LostFocus += comboBoxPower2LostFocus;
            comboBoxPower3.LostFocus += comboBoxPower3LostFocus;
            comboBoxPower4.LostFocus += comboBoxPower4LostFocus;


            comboBoxWT1.LostFocus += comboBoxWT1LostFocus;
            comboBoxWT2.LostFocus += comboBoxWT2LostFocus;
            comboBoxWT3.LostFocus += comboBoxWT3LostFocus;
            comboBoxWT4.LostFocus += comboBoxWT4LostFocus;


            sessionCB.LostFocus += sessionCBLostFocus;
            ValueQCB.LostFocus += ValueQCB2LostFocus;
            tagFocusCB.LostFocus += tagFocusCBLostFocus;
            valueABCB.LostFocus += valueABCBLostFocus;

            //   GetIPList(tvDevList);//<llp>
            services = new WebsocketServices(ConfigurationManager.AppSettings["RedisServer"]);
            services.OnReceive = OnReceive;
            //Tag_data = TagMock.GetData();
        }

        private void OnReceive(EnumSDKActions action)
        {
            switch (action)
            {
                case EnumSDKActions.Open:
                    btnStartInv.PerformClick();
                    break;
                case EnumSDKActions.Close:
                    btnStopInv.PerformClick();
                    break;
                case EnumSDKActions.ReadOne:
                    btnInvOnce_Click(null, null);
                    break;
                case EnumSDKActions.KeepReading:
                    btnInvOnce.PerformClick();
                    break;
            }
        }

        private void valueABCBLostFocus(object sender, EventArgs e)
        {
            if (int.Parse(valueABCB.Text) > 1)
            {
                valueABCB.Text = "1";
            }
            else if (int.Parse(valueABCB.Text) < 0)
            {
                valueABCB.Text = "0";
            }
        }

        private void tagFocusCBLostFocus(object sender, EventArgs e)
        {
            if (int.Parse(tagFocusCB.Text) > 1)
            {
                tagFocusCB.Text = "1";
            }
            else if (int.Parse(tagFocusCB.Text) < 0)
            {
                tagFocusCB.Text = "0";
            }
        }

        private void ValueQCB2LostFocus(object sender, EventArgs e)
        {
            if (int.Parse(ValueQCB.Text) > 15)
            {
                ValueQCB.Text = "15";
            }
            else if (int.Parse(ValueQCB.Text) < 0)
            {
                ValueQCB.Text = "0";
            }
        }

        private void sessionCBLostFocus(object sender, EventArgs e)
        {
            if (int.Parse(sessionCB.Text) > 3)
            {
                sessionCB.Text = "3";
            }
            else if (int.Parse(sessionCB.Text) < 0)
            {
                sessionCB.Text = "0";
            }
        }

        private void intelligentDetectionMsg(object sender, EventArgs e)
        {
            intelligentDetectionEventArgs args = e as intelligentDetectionEventArgs;
            showMsg(args);
        }

        private void showMsg(intelligentDetectionEventArgs result)
        {
            /* ListViewItem lvi = new ListViewItem();
             lvi.Text = (ShowMsgLV.Items.Count + 1).ToString();
             lvi.SubItems.Add(DateTime.Now.ToString());
             lvi.SubItems.Add(result);
             ShowMsgLV.Items.Add(lvi);*/
            if (processBox != null)
            {
                processBox.UpdateMessage(result);
            }
        }

        private void InitfrequencyCheckBox()
        {
            frequencysCB = new CheckBox[] { frequency1, frequency2, frequency3, frequency4, frequency5, frequency6, frequency7, frequency8, frequency9, frequency10,
                                            frequency11, frequency12, frequency13, frequency14, frequency15, frequency16, frequency17, frequency18, frequency19, frequency20,
                                            frequency21, frequency22, frequency23, frequency24, frequency25, frequency26, frequency27, frequency28, frequency29, frequency30,
                                            frequency31, frequency32, frequency33, frequency34, frequency35, frequency36, frequency37, frequency38, frequency39, frequency40,
                                            frequency41, frequency42, frequency43, frequency44, frequency45,frequency46, frequency47, frequency48, frequency49, frequency50,
            };
            multiAntCB = new CheckBox[] { Ant01 , Ant02, Ant03, Ant04, Ant05, Ant06, Ant07, Ant08,
                                          Ant09 , Ant10, Ant11, Ant12, Ant13, Ant14, Ant15, Ant16,
                                          Ant17 , Ant18, Ant19, Ant20, Ant21, Ant22, Ant23, Ant24,
                                          Ant25 , Ant26, Ant27, Ant28, Ant29, Ant30, Ant31, Ant32,
            };
        }

        internal void StartAutoReadCard(AutoReadCard auto)
        {
            this.auto = auto;
            if (auto.mode == 0x01)
            {
                ReadClock1.Enabled = false;
            }
        }

        private void comboBoxWT4LostFocus(object sender, EventArgs e)
        {
            if (int.Parse(comboBoxWT4.Text) > 5000)
            {
                comboBoxWT4.Text = "5000";
            }
            else if (int.Parse(comboBoxWT4.Text) < 100)
            {
                comboBoxWT4.Text = "100";
            }
        }

        internal void StartInvOnce()
        {
            if (auto != null)
            {
                if (auto.mode == 0x01)
                    btnClear_Click(null, null);
            }
        }

        private void comboBoxWT3LostFocus(object sender, EventArgs e)
        {
            if (int.Parse(comboBoxWT3.Text) > 5000)
            {
                comboBoxWT3.Text = "5000";
            }
            else if (int.Parse(comboBoxWT3.Text) < 100)
            {
                comboBoxWT3.Text = "100";
            }
        }

        private void comboBoxWT2LostFocus(object sender, EventArgs e)
        {
            if (int.Parse(comboBoxWT2.Text) > 5000)
            {
                comboBoxWT2.Text = "5000";
            }
            else if (int.Parse(comboBoxWT2.Text) < 100)
            {
                comboBoxWT2.Text = "100";
            }
        }

        private void comboBoxWT1LostFocus(object sender, EventArgs e)
        {
            if (int.Parse(comboBoxWT1.Text) > 5000)
            {
                comboBoxWT1.Text = "5000";
            }
            else if (int.Parse(comboBoxWT1.Text) < 100)
            {
                comboBoxWT1.Text = "100";
            }
        }

        private void comboBoxPower4LostFocus(object sender, EventArgs e)
        {
            if (int.Parse(comboBoxPower4.Text) > 33)
            {
                comboBoxPower4.Text = "33";
            }
            else if (int.Parse(comboBoxPower4.Text) < 5)
            {
                comboBoxPower4.Text = "5";
            }
        }

        private void comboBoxPower3LostFocus(object sender, EventArgs e)
        {
            if (int.Parse(comboBoxPower3.Text) > 33)
            {
                comboBoxPower3.Text = "33";
            }
            else if (int.Parse(comboBoxPower3.Text) < 5)
            {
                comboBoxPower3.Text = "5";
            }
        }

        private void comboBoxPower2LostFocus(object sender, EventArgs e)
        {
            if (int.Parse(comboBoxPower2.Text) > 33)
            {
                comboBoxPower2.Text = "33";
            }
            else if (int.Parse(comboBoxPower2.Text) < 5)
            {
                comboBoxPower2.Text = "5";
            }
        }

        private void comboBoxPower1LostFocus(object sender, EventArgs e)
        {
            if (int.Parse(comboBoxPower1.Text) > 33)
            {
                comboBoxPower1.Text = "33";
            }
            else if (int.Parse(comboBoxPower1.Text) < 5)
            {
                comboBoxPower1.Text = "5";
            }
        }

        private void DisPlayData(object sender, EventArgs e)
        {
            Analysis.CommandEventArgs args = e as Analysis.CommandEventArgs;
            switch (args.resultType)
            {
                case "Version":
                    UHFReader.Model.Version version = args.result as UHFReader.Model.Version;
                    UpdateVersion(version);
                    ShowResult(rm.GetString("strGetVersion"), 0x00);
                    break;
                case "Tag":
                    Tag tag = args.result as Tag;
                    UpdateTag(tag);
                    //  ShowResult("读取标签数据操作", 0x00);
                    break;
                case "Gerneral":                         //通用应答 0x00 成功  0x80失败
                    Gerneral gerneral = args.result as Gerneral;
                    UpdateGerneral(gerneral);
                    break;
                case "ReadWriteTag":
                    ReadWriteTag readWriteTag = args.result as ReadWriteTag;
                    UpdateReadWriteTag(readWriteTag);
                    ShowResult(rm.GetString("strMsgFailedReadData"), 0x00);
                    break;
                case "AntInfo":
                    AntInfo antInfo = args.result as AntInfo;
                    UpdateAntInfo(antInfo);
                    if (antForm != null)
                        antForm.UpdateAntInfo(antInfo);
                    ShowResult(rm.GetString("strMsgSucceedGetAnt"), 0x00);
                    break;
                case "GPIO":
                    GPIO gpio = args.result as GPIO;
                    UpdateGPIO(gpio);
                    ShowResult(rm.GetString("strGbIOOpr"), 0x00);
                    break;
                case "multichannelAnt":
                    multichannelAnt ant = args.result as multichannelAnt;
                    if (antForm != null)
                        antForm.UpdateMultiAnt(ant);
                    UpdateMultiAnt(ant);
                    break;
                case "Dev":
                    Dev dev = args.result as Dev;
                    UpdateDev(dev);
                    ShowResult(rm.GetString("strBtnGet") + rm.GetString("strLabDevNo"), 0x00);
                    break;
                case "AdjacentJudgment":
                    AdjacentJudgment adjacentJudgment = args.result as AdjacentJudgment;
                    UpdateadjacentJudgment(adjacentJudgment);
                    ShowResult(rm.GetString("strBtnGet") + rm.GetString("strLabNeighJudge"), 0x00);
                    break;
                case "Buzzer":
                    Buzzer buzzer = args.result as Buzzer;
                    UpdateBuzzer(buzzer);
                    ShowResult(rm.GetString("strBtnGet") + rm.GetString("strBuzzer"), 0x00);
                    break;
                case "WorkMode":
                    WorkMode workMode = args.result as WorkMode;
                    UpdateWorkMode(workMode);
                    ShowResult(rm.GetString("strBtnGet") + rm.GetString("strWormode"), 0x00);
                    break;
                case "TriggerModeDalayTime":
                    TriggerModeDalayTime triggerModeDalayTime = args.result as TriggerModeDalayTime;
                    UpdateTriggerModeDalayTime(triggerModeDalayTime);
                    ShowResult(rm.GetString("strBtnGet") + rm.GetString("strTriggerDelayLB"), 0x00);
                    break;
                case "FrequencyArea":
                    /*  FrequencyArea frequencyArea = args.result as FrequencyArea;
                      UpdateeFrequencyArea(frequencyArea);
                      if(frequencyPoints != null)
                      frequencyPoints.UpdateeFrequencyArea(frequencyArea);
                      ShowResult(rm.GetString("strBtnGet") + rm.GetString("strFrequencyArea"), 0x00);*/
                    break;
                case "CustomFrequcney":
                    CustomFrequcney customFrequcney = args.result as CustomFrequcney;
                    UpdateecustomFrequcney(customFrequcney);
                    if (frequencyPoints != null)
                        frequencyPoints.UpdateecustomFrequcney(customFrequcney);
                    ShowResult(rm.GetString("strBtnGet") + rm.GetString("strFrequency"), 0x00);
                    break;
                case "AntState":
                    AntState antState = args.result as AntState;
                    UpdateAntState(antState);
                    ShowResult(rm.GetString("strBtnGet") + rm.GetString("strAntState"), 0x00);
                    break;
                case "MultiCard":
                    MultiCard multiCard = args.result as MultiCard;
                    UpdateMultiCard(multiCard);
                    ShowResult(rm.GetString("strBtnGet") + rm.GetString("strMultiCard"), 0x00);
                    break;
                case "TagDataFormat":
                    TagDataFormat tagDataFormat = args.result as TagDataFormat;
                    UpdateTagDataFormat(tagDataFormat);
                    if (tagFormat != null)
                        tagFormat.UpdateTagDataFormat(tagDataFormat);
                    ShowResult(rm.GetString("strBtnGet") + rm.GetString("strDataFormat"), 0x00);
                    break;
                case "QuencyPara":
                    QuencyPara quencyPara = args.result as QuencyPara;
                    UpdateQuencyPara(quencyPara);
                    if (quencyMode != null)
                        quencyMode.UpdateQuencyPara(quencyPara);
                    ShowResult(rm.GetString("strBtnGet") + rm.GetString("strQuencyPara"), 0x00);
                    break;
                default:
                    break;
            }
        }

        private void UpdateQuencyPara(QuencyPara quencyPara)
        {
            sessionCB.Text = quencyPara.session.ToString();
            ValueQCB.Text = quencyPara.valueQ.ToString();
            tagFocusCB.Text = quencyPara.tagfocus.ToString();
            valueABCB.Text = quencyPara.valueAB.ToString();

        }

        private void UpdateTagDataFormat(TagDataFormat tagDataFormat)
        {
            if ((tagDataFormat.type & 0x80) != 0)
                haveAntCB.Checked = true;
            else
                haveAntCB.Checked = false;
            if ((tagDataFormat.type & 0x40) != 0)
                haveRssiCB.Checked = true;
            else
                haveRssiCB.Checked = false;
            if ((tagDataFormat.type & 0x20) != 0)
                haveDevCB.Checked = true;
            else
                haveDevCB.Checked = false;
            if ((tagDataFormat.type & 0x10) != 0)
                haveChannelCB.Checked = true;
            else
                haveChannelCB.Checked = false;
        }

        private void UpdateMultiCard(MultiCard multiCard)
        {
            /*  if (multiCard.status == 0x00)
              {
                  MultiCardStatusOffRB.Checked = true;
              }
              else
              {
                  MultiCardStatusRB.Checked = true;
              }*/
        }

        private void UpdateAntState(AntState antState)
        {
            if (antState.type == 0x04)
            {
                if (antState.ant[0] == 0x01)
                    cbAnt1.Checked = true;
                else
                    cbAnt1.Checked = false;
                if (antState.ant[1] == 0x01)
                    cbAnt2.Checked = true;
                else
                    cbAnt2.Checked = false;
                if (antState.ant[2] == 0x01)
                    cbAnt3.Checked = true;
                else
                    cbAnt3.Checked = false;
                if (antState.ant[3] == 0x01)
                    cbAnt4.Checked = true;
                else
                    cbAnt4.Checked = false;
            }
            else
            {
                for (int i = 0; i < 32; i++)
                {
                    if (i < antState.type)
                    {
                        if (antState.ant[i] == 0x01)
                            multiAntCB[i].Checked = true;
                        else
                            multiAntCB[i].Checked = false;
                    }
                    else
                    {
                        multiAntCB[i].Checked = false;
                    }

                }
            }

        }

        private void UpdateecustomFrequcney(CustomFrequcney customFrequcney)
        {
            FrequencyAreaCB.SelectedIndex = customFrequcney.type - 1;
            if (customFrequcney.type == 0x06)
            {
                ConstantfrequencyTB.Text = (customFrequcney.point / 1000.000).ToString();
            }
            else if (customFrequcney.type == 0x05)
            {
                for (int j = 0; j < frequencysCB.Length; j++)
                {
                    frequencysCB[j].Checked = false;
                }
                for (int i = 0; i < customFrequcney.points.Count; i++)
                {
                    frequencysCB[customFrequcney.points[i]].Checked = true;
                }
            }

        }

        private void UpdateeFrequencyArea(FrequencyArea frequencyArea)
        {
            FrequencyAreaCB.SelectedIndex = frequencyArea.area - 1;
        }

        private void UpdateMultiAnt(multichannelAnt ant)
        {
            for (int i = 0; i < 32; i++)
            {
                multiAntCB[i].Checked = false;
            }
            if (ant.state == 16)
            {
                ShowResult(rm.GetString("strGetMultiAnt16"), 0x00);
                rb16Ant.Checked = true;
            }
            else if (ant.state == 32)
            {
                ShowResult(rm.GetString("strGetMultiAnt32"), 0x00);
                rb32Ant.Checked = true;
            }
            for (int index = 0; index < ant.enable.Length; index++)
            {
                if (ant.enable[index] == 0x01)
                {
                    multiAntCB[index].Checked = true;
                }
                else
                {
                    multiAntCB[index].Checked = false;
                }
            }
            MultiAntTime.Text = ant.dwell_time.ToString();
            MultiAntPowerTB.Text = ant.power.ToString();
        }

        private void UpdateTriggerModeDalayTime(TriggerModeDalayTime triggerModeDalayTime)
        {
            tbDelayTime.Text = triggerModeDalayTime.time.ToString();
        }

        private void UpdateWorkMode(WorkMode workMode)
        {
            comboBoxWorkMode.SelectedIndex = workMode.workMode - 1;
        }

        private void UpdateBuzzer(Buzzer buzzer)
        {
            if (1 == buzzer.status)
            {
                rdoBuzzerOn.Checked = true;
            }
            else
            {
                rdoBuzzerOff.Checked = true;
            }
        }

        private void UpdateadjacentJudgment(AdjacentJudgment adjacentJudgment)
        {
            tbNeighJudge.Text = adjacentJudgment.value.ToString();
        }

        private void UpdateDev(Dev dev)
        {
            tbDevNo.Text = dev.dev.ToString();
        }

        private void UpdateGPIO(GPIO gpio)
        {

            if (gpio.states[0] == 1)
            {
                cbIn1.Checked = true;
            }
            else
            {
                cbIn1.Checked = false;
            }

            if (gpio.states[1] == 1)
            {
                cbIn2.Checked = true;
            }
            else
            {
                cbIn2.Checked = false;
            }
        }

        private void UpdateAntInfo(AntInfo antInfo)
        {
            Rb4Channel.Checked = true;
            cbAnt1.Checked = (antInfo.antEnable[0] == 1);
            cbAnt2.Checked = (antInfo.antEnable[1] == 1);
            cbAnt3.Checked = (antInfo.antEnable[2] == 1);
            cbAnt4.Checked = (antInfo.antEnable[3] == 1);
            comboBoxWT1.Text = antInfo.dwell_time[0].ToString();
            comboBoxWT2.Text = antInfo.dwell_time[1].ToString();
            comboBoxWT3.Text = antInfo.dwell_time[2].ToString();
            comboBoxWT4.Text = antInfo.dwell_time[3].ToString();
            comboBoxPower1.Text = (antInfo.power[0] / 10).ToString();
            comboBoxPower2.Text = (antInfo.power[1] / 10).ToString();
            comboBoxPower3.Text = (antInfo.power[2] / 10).ToString();
            comboBoxPower4.Text = (antInfo.power[3] / 10).ToString();
        }

        private void UpdateReadWriteTag(ReadWriteTag readWriteTag)
        {
            tbRWData.Text = DataConvert.ConvertToString(readWriteTag.data, 0, readWriteTag.data.Length);
        }

        private void UpdateGerneral(Gerneral gerneral)
        {
            switch (gerneral.type)
            {
                case "UhfSetAntConfig":
                    ShowResult(rm.GetString("strMsgFailedSetAnt"), gerneral.result);
                    break;
                case "UhfKillTag":
                    ShowResult(rm.GetString("strMsgSucceedDes"), gerneral.result);
                    break;
                case "UhfLockTag":
                    ShowResult(rm.GetString("strGbLockTag"), gerneral.result);
                    break;
                case "UhfWriteTagData":
                    ShowResult(rm.GetString("strBtnWriteTagData") + rm.GetString("strLabRWData"), gerneral.result);
                    break;
                case "UhfReadTagData":
                    ShowResult(rm.GetString("strMsgFailedReadData"), gerneral.result);
                    break;
                case "UhfInvMultiplyEnd":
                    ShowResult(rm.GetString("strBtnStopReadData"), gerneral.result);
                    break;
                case "UhfInvMultiplyBegin":
                    ShowResult(rm.GetString("strBtnStartReadData"), gerneral.result);
                    LockButton();             //锁定一些操作
                    break;
                case "UHF_INV_ONCE_END":                //单次查询完成 显示查询到的数据
                    UpdateInvOnceData();
                    if (btnClear.Enabled == false && btnStartInv.Enabled == false)
                    {
                        btnClear.Enabled = true;
                        btnStartInv.Enabled = true;
                    }
                    ShowResult(rm.GetString("strBtnReadOnce"), 0x00);
                    break;
                case "UHF_INV_ONCE_START":                //单次查询完成 显示查询到的数据
                    sw = new System.Diagnostics.Stopwatch();
                    sw.Start();
                    break;
                case "UHF_SET_DI_STATE":                //设置GPIO
                    ShowResult(rm.GetString("strGbIOOpr"), gerneral.result);
                    break;
                case "UHF_SET_DEV":                //设置设备号
                    ShowResult(rm.GetString("strBtnSetAlive") + rm.GetString("strLabDevNo"), gerneral.result);
                    break;
                case "UHF_SET_ADJACENT_JUDGMENT":                //设置相邻判断
                    ShowResult(rm.GetString("strBtnSetAlive") + rm.GetString("strLabNeighJudge"), gerneral.result);
                    break;
                case "UHF_SET_BUZZER":                //设置蜂鸣器
                    ShowResult(rm.GetString("strBtnSetAlive") + rm.GetString("strBuzzer"), gerneral.result);
                    break;
                case "UHF_SET_WORK_MODE":                //设置工作模式
                    ShowResult(rm.GetString("strBtnSetAlive") + rm.GetString("strWormode"), gerneral.result);
                    break;
                case "UHF_SET_TRIGGER_DELAY":                //设置触发延时
                    ShowResult(rm.GetString("strBtnSetAlive") + rm.GetString("strTriggerDelayLB"), gerneral.result);
                    break;
                case "UHF_SET_DATACHANNEL":                //设置数据发送通道
                    ShowResult(rm.GetString("strBtnSetAlive") + rm.GetString("strTransMode"), gerneral.result);
                    break;
                case "UHF_SET_TRIGGER_ALARM":                //设置触发报警时长
                    ShowResult(rm.GetString("strBtnSetAlive") + rm.GetString("strAlert"), gerneral.result);
                    break;
                case "UHF_SET_FREQUENCY_AREA":                //设置触发报警时长
                    ShowResult(rm.GetString("strBtnSetAlive") + rm.GetString("strFrequencyArea"), gerneral.result);
                    break;
                case "UHF_SET_FREQUENCY":                //设置频点
                    ShowResult(rm.GetString("strBtnSetAlive") + rm.GetString("strFrequency"), gerneral.result);
                    break;
                case "UHF_SET_DATA_FORMATE":                //设置标签数据格式
                    ShowResult(rm.GetString("strBtnSetAlive") + rm.GetString("strDataFormat"), gerneral.result);
                    break;
                case "UHF_FACTORY":                //恢复出厂设置
                    ShowResult(rm.GetString("strFactory"), gerneral.result);
                    break;
                case "UHF_SET_QUENCY_PARA":                //设置盘存参数
                    ShowResult(rm.GetString("strBtnSetAlive") + rm.GetString("strQuencyPara"), gerneral.result);
                    break;
                default:
                    break;
            }
        }

        private void LockButton()
        {
            ;
        }

        /* private void ShowResult(string v, byte result)
          {
              if (result == 0x00)
              {
                  ShowResultLB.Text = v + rm.GetString("strSuccess");
              }
              else if (result == 0x80)
              {
                  ShowResultLB.Text = v + rm.GetString("strFailed");
              }

          }*/

        private void ShowResult(string v)
        {
            //  ListViewItem lvi = new ListViewItem();
            //  lvi.Text = resultMessageNum++.ToString();
            //  lvi.SubItems.Add(DateTime.Now.ToString());
            //  lvi.SubItems.Add(v);
            //  showResultLV.Items.Add(lvi);
            //  this.showResultLV.EnsureVisible(this.showResultLV.Items.Count - 1);

        }


        int resultMessageNum = 1;
        private void ShowResult(string v, byte result)
        {
            string resultStr = "";
            if (result == 0x00)
            {
                if (language == 0x00)
                    resultStr = v + rm.GetString("strSuccess");
                else if (language == 0x01)
                    resultStr = v + " " + rm.GetString("strSuccess");
            }
            else if (result == 0x80)
            {
                if (language == 0x00)
                    resultStr = v + rm.GetString("strFailed");
                else if (language == 0x01)
                    resultStr = v + " " + rm.GetString("strFailed");
            }
            //   ListViewItem lvi = new ListViewItem();
            //  lvi.Text = resultMessageNum++.ToString();
            //  lvi.SubItems.Add(DateTime.Now.ToString());
            //  lvi.SubItems.Add(resultStr);
            //   showResultLV.Items.Add(lvi);
            //   this.showResultLV.EnsureVisible(this.showResultLV.Items.Count - 1);
            label13.Text = resultStr;
        }

        private void ShowResult(string v, byte result, string str)
        {
            string resultStr = "";
            if (result == 0x00)
            {
                resultStr = v + rm.GetString("strSuccess") + str;
            }
            else if (result == 0x80)
            {
                resultStr = v + rm.GetString("strFailed") + str;
            }
            // ListViewItem lvi = new ListViewItem();
            // lvi.Text = resultMessageNum++.ToString();
            // lvi.SubItems.Add(DateTime.Now.ToString());
            //  lvi.SubItems.Add(resultStr);
            //  showResultLV.Items.Add(lvi);
            //  this.showResultLV.EnsureVisible(this.showResultLV.Items.Count - 1);
            label13.Text = resultStr;
        }

        /*  private void ShowResult(string v, byte result,string str)
          {
              if (result == 0x00)
              {
                  ShowResultLB.Text = v + rm.GetString("strSuccess")+ str;
              }
              else if (result == 0x80)
              {
                  ShowResultLB.Text = v + rm.GetString("strFailed")+ str;
              }
          }*/


        private async void UpdateInvOnceData()
        {
            string x1 = "", x2 = "", x3 = "";
            if (sw != null)
            {
                TimeSpan ts = sw.Elapsed;
                if (ts.Minutes < 10)
                { x1 = "0" + ts.Minutes; }
                else
                { x1 = "" + ts.Minutes; }
                if (ts.Seconds < 10)
                { x2 = "0" + ts.Seconds; }
                else
                { x2 = "" + ts.Seconds; }


                if (ts.Milliseconds < 10)
                { x3 = "00" + ts.Milliseconds; }
                else if (ts.Milliseconds < 100 && ts.Milliseconds > 9)
                {
                    x3 = "0" + ts.Milliseconds;
                }
                else
                { x3 = "" + ts.Milliseconds; }
                labReadClock.Text = x1 + ":" + x2 + ":" + x3;
            }

            await UpdateListView();
            if (quencyTagForm != null)
            {
                quencyTagForm.StartARound();
            }
        }

        private void WriteTagsToLog()
        {
            throw new NotImplementedException();
        }

        private void UpdateTag(Tag tag)
        {
            bNewTag = true;
            lock (Tag_data)
            {
                for (int i = 0; i < Tag_data.Count; ++i)
                {
                    if (tag.EPC == Tag_data[i].EPC && tag.device == Tag_data[i].device && tag.IP == Tag_data[i].IP)
                    {
                        Tag_data[i].counts++;
                        Tag_data[i].isAnt = tag.isAnt;
                        Tag_data[i].isRssi = tag.isRssi;
                        Tag_data[i].isDev = tag.isDev;
                        Tag_data[i].isDir = tag.isDir;
                        Tag_data[i].ant = tag.ant;
                        Tag_data[i].rssi = tag.rssi;
                        Tag_data[i].device = tag.device;
                        Tag_data[i].direction = tag.direction;
                        bNewTag = false;     // 不是新标签
                        break;
                    }
                }
                if (bNewTag)
                {
                    Tag_data.Add(tag);
                }
            }
        }

        private void UpdateVersion(UHFReader.Model.Version version)
        {
            Version.Text = rm.GetString("strVersion") + version.version;
        }

        // 常用操作页
        private void InitGeneralControl()
        {
            // this.tabSpecialFun.Parent = null;
            // 生成不闪烁的标签数据列表
            this.lvTagData = new ListViewNF();
            this.lvTagData.Location = new System.Drawing.Point(0, 60);
            this.lvTagData.Name = "lvTagData";
            this.lvTagData.Size = new System.Drawing.Size(755, 475);
            this.lvTagData.TabIndex = 13;
            this.lvTagData.View = View.Details;
            // 增加数据列
            lvTagData.Columns.Add("No", 50, HorizontalAlignment.Center);
            lvTagData.Columns.Add("EPC", 220, HorizontalAlignment.Center);
            lvTagData.Columns.Add("Counts", 80, HorizontalAlignment.Center);
            lvTagData.Columns.Add("Rssi", 50, HorizontalAlignment.Center);
            lvTagData.Columns.Add("Ant", 50, HorizontalAlignment.Center);
            lvTagData.Columns.Add("Dir", 60, HorizontalAlignment.Center);
            lvTagData.Columns.Add("Dev", 80, HorizontalAlignment.Center);
            lvTagData.Columns.Add("IP/COM", 120, HorizontalAlignment.Center);
            this.TagAccessPage.Controls.Add(this.lvTagData);
            GetSerialPortList(this.serialPortCB);       //获取串口
            GetLoaclIP(this.ServerIPTB);                //获取本机IP
            ReadConfig();                               //读取配置文件信息
        }

        private void ReadConfig()
        {
            ServerPortTB.Text = ConfigurationManager.AppSettings["ServerPort"];
        }

        private void GetLoaclIP(ComboBox serverIP)
        {
            // string result = RunApp("route", "print", true);
            //  Match m = Regex.Match(result, @"0.0.0.0\s+0.0.0.0\s+(\d+.\d+.\d+.\d+)\s+(\d+.\d+.\d+.\d+)");
            serverIP.Items.Clear();
            string name = Dns.GetHostName();
            IPAddress[] me = Dns.GetHostAddresses(name);
            if (me.Length > 1)
            {
                foreach (IPAddress ip in me)
                {
                    if (ValidateIPAddress(ip.ToString()))
                        serverIP.Items.Add(ip.ToString());
                }
            }
            if (serverIP.Items.Count > 0)
            {
                serverIP.SelectedIndex = 0;
            }
        }
        public static bool ValidateIPAddress(string ipAddress)
        {
            Regex validipregex = new Regex(@"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");
            return (ipAddress != "" && validipregex.IsMatch(ipAddress.Trim())) ? true : false;
        }

        public static string RunApp(string filename, string arguments, bool recordLog)
        {
            try
            {

                Process proc = new Process();
                proc.StartInfo.FileName = filename;
                proc.StartInfo.CreateNoWindow = true;
                proc.StartInfo.Arguments = arguments;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.UseShellExecute = false;
                proc.Start();

                using (System.IO.StreamReader sr = new System.IO.StreamReader(proc.StandardOutput.BaseStream, Encoding.Default))
                {
                    //上面标记的是原文，下面是我自己调试错误后自行修改的  
                    Thread.Sleep(100);           //貌似调用系统的nslookup还未返回数据或者数据未编码完成，程序就已经跳过直接执行  
                                                 //txt = sr.ReadToEnd()了，导致返回的数据为空，故睡眠令硬件反应  
                    if (!proc.HasExited)         //在无参数调用nslookup后，可以继续输入命令继续操作，如果进程未停止就直接执行  
                    {                            //txt = sr.ReadToEnd()程序就在等待输入，而且又无法输入，直接掐住无法继续运行  
                        proc.Kill();
                    }
                    string txt = sr.ReadToEnd();
                    sr.Close();
                    return txt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // 标签访问页
        private void InitAccessTagControl()
        {
            // 读写区域
            cbbRWBank.Items.Add("Reserve");
            cbbRWBank.Items.Add("EPC");
            cbbRWBank.Items.Add("TID");
            cbbRWBank.Items.Add("User");

            // 锁卡区域
            cbbLockBank.Items.Add("Kill");
            cbbLockBank.Items.Add("Access");
            cbbLockBank.Items.Add("EPC");
            cbbLockBank.Items.Add("TID");
            cbbLockBank.Items.Add("User");

            cbbLockType.Items.Add(rm.GetString("strItemUnLock"));
            cbbLockType.Items.Add(rm.GetString("strItemPerWritable"));
            cbbLockType.Items.Add(rm.GetString("strItemSecuLock"));
            cbbLockType.Items.Add(rm.GetString("strItemPerUnwritable"));
            tbRWAccessPwd.Text = "00 00 00 00";
        }
        // 网络参数页
        private void InitCommParamControl()
        {
            comboBoxNetMode.Items.Add("TCP Server");
            comboBoxNetMode.Items.Add("TCP Client");
            comboBoxNetMode.Items.Add("UDP");
            comboBoxNetMode.Items.Add("UDP Group");

            comboBoxIPMode.Items.Add("Static");
            comboBoxIPMode.Items.Add("Dynamic");

            comboBoxBaudRate.Items.Add("1200");
            comboBoxBaudRate.Items.Add("2400");
            comboBoxBaudRate.Items.Add("4800");
            comboBoxBaudRate.Items.Add("7200");
            comboBoxBaudRate.Items.Add("9600");
            comboBoxBaudRate.Items.Add("14400");
            comboBoxBaudRate.Items.Add("19200");
            comboBoxBaudRate.Items.Add("28800");
            comboBoxBaudRate.Items.Add("38400");
            comboBoxBaudRate.Items.Add("57600");
            comboBoxBaudRate.Items.Add("76800");
            comboBoxBaudRate.Items.Add("115200");
            comboBoxBaudRate.Items.Add("230400");

            comboBoxDataBits.Items.Add("8");
            comboBoxDataBits.Items.Add("7");
            comboBoxDataBits.Items.Add("6");
            comboBoxDataBits.Items.Add("5");

            comboBoxCheckBits.Items.Add("None");
            comboBoxCheckBits.Items.Add("Odd");
            comboBoxCheckBits.Items.Add("Even");
            comboBoxCheckBits.Items.Add("Marked");
            comboBoxCheckBits.Items.Add("Space");
        }

        // 设备参数页
        private void InitDeviceParamControl()
        {
            for (int i = 100; i <= 5000; i += 100)
            {
                comboBoxWT1.Items.Add(i.ToString());
                comboBoxWT2.Items.Add(i.ToString());
                comboBoxWT3.Items.Add(i.ToString());
                comboBoxWT4.Items.Add(i.ToString());
                MultiAntTime.Items.Add(i.ToString());
            }
            for (int i = 5; i <= 30; i++)
            {
                comboBoxPower1.Items.Add(i.ToString());
                comboBoxPower2.Items.Add(i.ToString());
                comboBoxPower3.Items.Add(i.ToString());
                comboBoxPower4.Items.Add(i.ToString());
                MultiAntPowerTB.Items.Add(i.ToString());
            }
            comboBoxWorkMode.Items.Add(rm.GetString("strMaster"));
            comboBoxWorkMode.Items.Add(rm.GetString("strTimming"));
            comboBoxWorkMode.Items.Add(rm.GetString("strTrigger"));
            //   dtpClock.CustomFormat = "yyyy-MM-dd hh:mm:ss";

            //   comboZoneBank.Items.Add("EPC");
            //   comboZoneBank.Items.Add("TID");
            //   comboZoneBank.Items.Add("User");

            /*    for (int i = 0; i < 32; i++)
                {
                    comboZoneBegin.Items.Add(i.ToString());
                    comboZoneLength.Items.Add((i + 1).ToString());
                }*/
        }
        /* void connectTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
         {
             connectTimer.Stop();
             MessageBox.Show("连接失败");
         }*/

        public void GetSerialPortList(ComboBox cb)
        {
            cb.Items.Clear();
            cb.Text = "";
            foreach (string s in SerialPort.GetPortNames())
            {
                cb.Items.Add(s);
            }
            if (cb.Items.Count > 0)
            {
                cb.SelectedIndex = 0;
            }
        }



        bool isMUltiEPC = false;
        private void btnStartInv_Click(object sender, EventArgs e)
        {

            try
            {
                if (IsSingleDeviceCB.Checked == true)
                {
                    reader.BeginInv(currentClient, 0x00);
                }
                else
                {
                    reader.BeginInv(0x00);
                }
                sw = new System.Diagnostics.Stopwatch();
                sw.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }


            btnInvOnce.Enabled = false;
            btnClear.Enabled = false;
            btnReadBuffer.Enabled = false;
            btnClearBuffer.Enabled = false;
            btnReadBuffer.Enabled = false;
            btnClearBuffer.Enabled = false;
            //   btnGetAnts.Enabled = false;
            btnSetAnts.Enabled = false;
            //  tabControl1.TabPages[1].Enabled = false;
            //   tabControl1.TabPages[2].Enabled = false;
            //   tabControl1.TabPages[3].Enabled = false;

            sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            //  ReadClock1.Start();
            //  ReadClock1.Enabled = true;

            isMUltiEPC = true;
        }


        long readLastCounts = 0x00;
        byte showSpeed = 1;
        private async Task UpdateListView()
        {
            bool isNewTag = true;
            int readCounts = 0;
            lock (Tag_data)
            {
                foreach (Tag epcData in Tag_data)
                {
                    Debug.WriteLine($"Data found: {epcData}");
                    readCounts += epcData.counts;
                    if (auto != null)
                    {
                        if ((auto.save & 0x02) != 0)
                            DataLog.Write(epcPath, epcData.EPC + "    " + epcData.time);
                    }
                    lock (lvTagData.Items)
                    {
                        for (int index = 0; index < lvTagData.Items.Count; index++)
                        {
                            if (epcData.EPC == lvTagData.Items[index].SubItems[1].Text && epcData.IP == lvTagData.Items[index].SubItems[7].Text)
                            {
                                lvTagData.Items[index].SubItems[2].Text = epcData.counts.ToString();
                                if (epcData.isRssi)
                                    lvTagData.Items[index].SubItems[3].Text = epcData.rssi.ToString();
                                if (epcData.isAnt)
                                    lvTagData.Items[index].SubItems[4].Text = epcData.ant.ToString();
                                if (epcData.isDir)
                                    lvTagData.Items[index].SubItems[5].Text = epcData.direction.ToString("X2");
                                if (epcData.isDev)
                                    lvTagData.Items[index].SubItems[6].Text = epcData.device.ToString();
                                isNewTag = false;
                                break;
                            }
                        }
                        if (isNewTag)
                        {
                            ListViewItem lvi = new ListViewItem();
                            lvi.Text = (lvTagData.Items.Count + 1).ToString();
                            lvi.SubItems.Add(epcData.EPC);
                            lvi.SubItems.Add(epcData.counts.ToString());
                            if (epcData.isRssi == false)
                                lvi.SubItems.Add("");
                            else
                                lvi.SubItems.Add(epcData.rssi.ToString());
                            if (epcData.isAnt == false)
                                lvi.SubItems.Add("");
                            else
                                lvi.SubItems.Add(epcData.ant.ToString());
                            if (epcData.isDir == false)
                                lvi.SubItems.Add("");
                            else
                                lvi.SubItems.Add(epcData.direction.ToString("X2"));
                            if (epcData.isDev == false)
                                lvi.SubItems.Add("");
                            else
                                lvi.SubItems.Add(epcData.device.ToString());
                            lvi.SubItems.Add(epcData.IP);
                            lvTagData.Items.Add(lvi);
                        }
                        isNewTag = true;
                    }
                }
                showSpeed++;
                readCountsLB.Text = readCounts.ToString();
                labelTagCount.Text = Tag_data.Count.ToString();// 更新标签计数
                if (showSpeed >= 20)
                {
                    readSpeedLB.Text = (readCounts - readLastCounts).ToString();
                    readLastCounts = readCounts;
                    showSpeed = 1;
                }
                if (auto != null)
                {
                    if ((auto.save & 0x01) != 0)
                        DataLog.Write(countPath, Tag_data.Count.ToString() + "      " + DateTime.Now.ToString());
                }
            }

            //await services.NotifyToRedis(Tag_data);
        }

        private void btnStopInv_Click(object sender, EventArgs e)
        {

            //   ReadClock1.Enabled = false;

            try
            {
                reader.StopInv(0x00);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            showSpeed = 1;
            btnInvOnce.Enabled = true;
            btnClear.Enabled = true;
            btnReadBuffer.Enabled = true;
            btnClearBuffer.Enabled = true;
            btnReadBuffer.Enabled = true;
            btnClearBuffer.Enabled = true;
            //  btnGetAnts.Enabled = true;
            btnSetAnts.Enabled = true;
            tabControl1.TabPages[1].Enabled = true;
            tabControl1.TabPages[2].Enabled = true;
            tabControl1.TabPages[3].Enabled = true;

            isMUltiEPC = false;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                labReadClock.Text = "00:00:00";//
                lock (lvTagData.Items)
                {
                    lvTagData.Items.Clear();
                }
                lock (Tag_data)
                {
                    Tag_data.Clear();
                }
                readCountsLB.Text = "0";
                labelTagCount.Text = "0";
                labelTagTIDCount.Text = "0";
                readSpeedLB.Text = "0";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private async void btnInvOnce_Click(object sender, EventArgs e)
        {
            //mock
            //await services.NotifyToRedis(Tag_data);
            //return;

            try
            {
                if (IsSingleDeviceCB.Checked == true)
                {
                    if (currentClient != null)
                    {
                        reader.InvOnce(currentClient, 0x00);
                        ShowResult(rm.GetString("strIsInventory"));
                        //  btnClear.Enabled = false;
                        //  btnStartInv.Enabled = false;

                    }
                    else
                    {
                        MessageBox.Show(noDeviceChoose);
                    }
                }
                else
                {
                    if (reader.InvOnce(0x00))
                    {
                        ShowResult(rm.GetString("strIsInventory"));
                    }
                    //  btnClear.Enabled = false;
                    //  btnStartInv.Enabled = false;
                    sw = new System.Diagnostics.Stopwatch();
                    sw.Start();
                }
                
                await services.NotifyToRedis(Tag_data);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnGetAnts_Click(object sender, EventArgs e)
        {

            try
            {
                if (currentClient != null)
                {
                    reader.GetAnt(currentClient);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private void btnSetAnts_Click(object sender, EventArgs e)
        {
            labelResult.Text = "";
            int[] dt = new int[4];
            int[] pwr = new int[4];

            System.DateTime currentTime = new System.DateTime();
            currentTime = System.DateTime.Now;
            string strT = currentTime.ToString("t");//<llp>

            if (int.TryParse(comboBoxWT1.Text, out dt[0]))
            {
                if (dt[0] < 50 || dt[0] > 10000)
                {
                    MessageBox.Show(rm.GetString("strErrorAnttime"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorAnttime"));
                return;
            }
            if (int.TryParse(comboBoxWT2.Text, out dt[1]))
            {
                if (dt[1] < 50 || dt[1] > 10000)
                {
                    MessageBox.Show(rm.GetString("strErrorAnttime"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorAnttime"));
                return;
            }
            if (int.TryParse(comboBoxWT3.Text, out dt[2]))
            {
                if (dt[2] < 50 || dt[2] > 10000)
                {
                    MessageBox.Show(rm.GetString("strErrorAnttime"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorAnttime"));
                return;
            }
            if (int.TryParse(comboBoxWT4.Text, out dt[3]))
            {
                if (dt[3] < 50 || dt[3] > 10000)
                {
                    MessageBox.Show(rm.GetString("strErrorAnttime"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorAnttime"));
                return;
            }
            if (int.TryParse(comboBoxPower1.Text, out pwr[0]))
            {
                if (pwr[0] > 33)
                {
                    MessageBox.Show(rm.GetString("strErrorPower"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorPower"));
                return;
            }
            if (int.TryParse(comboBoxPower2.Text, out pwr[1]))
            {
                if (pwr[1] > 33)
                {
                    MessageBox.Show(rm.GetString("strErrorPower"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorPower"));
                return;
            }
            if (int.TryParse(comboBoxPower3.Text, out pwr[2]))
            {
                if (pwr[2] > 33)
                {
                    MessageBox.Show(rm.GetString("strErrorPower"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorPower"));
                return;
            }
            if (int.TryParse(comboBoxPower4.Text, out pwr[3]))
            {
                if (pwr[3] > 33)
                {
                    MessageBox.Show(rm.GetString("strErrorPower"));
                    return;
                }
            }
            else
            {
                MessageBox.Show(rm.GetString("strErrorPower"));
                return;
            }

            /* if (0 == NR2k.SetAnt(currentDevice.hDev, ref AntCfg))
             {
                 labAnt.Text = "天线参数设置成功  " + strT;
             }
             else
             {
                 labAnt.Text = "天线参数设置失败  " + strT;
             }*/
            try
            {
                AntInfo antnfo = new AntInfo(4);
                antnfo.antEnable[0] = (byte)(cbAnt1.Checked ? 1 : 0);
                antnfo.antEnable[1] = (byte)(cbAnt2.Checked ? 1 : 0);
                antnfo.antEnable[2] = (byte)(cbAnt3.Checked ? 1 : 0);
                antnfo.antEnable[3] = (byte)(cbAnt4.Checked ? 1 : 0);
                for (int i = 0; i < 4; ++i)
                {
                    antnfo.dwell_time[i] = dt[i];
                    antnfo.power[i] = pwr[i] * 10;
                }
                if (currentClient != null)
                {
                    reader.SetAnt(currentClient, antnfo);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnSetOutPort_Click(object sender, EventArgs e)
        {
            labelResult.Text = "";
            byte outPort1 = 0;
            byte outPort2 = 0;
            if (cbOut1.Checked)
            {
                outPort1 = 1;
            }
            if (cbOut2.Checked)
            {
                outPort2 = 1;
            }
            /*  if (0 == NR2k.SetDO(currentDevice.hDev, 1, outPort1) &&
                  0 == NR2k.SetDO(currentDevice.hDev, 2, outPort2))
              {
                  labelResult.Text = "设置成功";
              }
              else
              {
                  labelResult.Text = "设置失败";
              }*/
            try
            {
                if (currentClient != null)
                {
                    GPIO gpio = new GPIO
                    {
                        gpio = 1,
                        state = outPort1
                    };
                    reader.SetGPIO(currentClient, gpio);
                    gpio.gpio = 2;
                    gpio.state = outPort2;
                    Thread.Sleep(500);
                    reader.SetGPIO(currentClient, gpio);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnGetDI_Click(object sender, EventArgs e)
        {
            /*  labelResult.Text = "";
              byte[] portState = new byte[4];
              if (0 == NR2k.GetDI(currentDevice.hDev, portState))
              {
                  if (1 == portState[0])
                  {
                      cbIn1.Checked = true;
                  }
                  else
                  {
                      cbIn1.Checked = false;
                  }
                  if (1 == portState[1])
                  {
                      cbIn2.Checked = true;
                  }
                  else
                  {
                      cbIn2.Checked = false;
                  }
              }
              else
              {
                  labelResult.Text = "读取输入口失败";
              }*/
            try
            {
                if (currentClient != null)
                {
                    reader.GetGPIO(currentClient);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnSetDevNo_Click(object sender, EventArgs e)
        {
            labelResult.Text = "";
            if (tbDevNo.Text == "")
            {
                MessageBox.Show(rm.GetString("strMsgDevNoNotEmpty"));
                return;
            }
            if (!Regex.IsMatch(tbDevNo.Text, "^[0-9]+$"))
            {
                MessageBox.Show(rm.GetString("strMsgPwdInvalidChar"));
                return;
            }
            int devno = 0;
            try
            {
                devno = int.Parse(tbDevNo.Text);
            }
            catch
            {
                MessageBox.Show(rm.GetString("strMsgDevNoValid"));
                return;
            }
            if (devno > 65535 || devno < 0)
            {
                MessageBox.Show(rm.GetString("strMsgDevNoValid"));
                return;
            }
            try
            {
                if (currentClient != null)
                {
                    Dev dev = new Dev
                    {
                        dev = (ushort)devno,
                    };
                    reader.SetDev(currentClient, dev);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnSetNeighJudge_Click(object sender, EventArgs e)
        {
            labelResult.Text = "";
            if (tbNeighJudge.Text == "")
            {
                MessageBox.Show(rm.GetString("strMsgDevNoNotEmpty")); // rm.GetString("strMsgNJNotEmpty")
                return;
            }
            if (!Regex.IsMatch(tbNeighJudge.Text, "^[0-9]+$"))
            {
                MessageBox.Show(rm.GetString("strMsgPwdInvalidChar")); // rm.GetString("strMsgNotDigit")
                tbNeighJudge.Text = "";
                return;
            }

            int neighJudge = int.Parse(tbNeighJudge.Text);
            if (neighJudge > 255)
            {
                MessageBox.Show(rm.GetString("strFilterInValue"));
                tbNeighJudge.Text = "";
                return;
            }
            /* if (0 == NR2k.SetNeighJudge(currentDevice.hDev, (byte)neighJudge))
             {
                 labelResult.Text = "设置成功";
             }
             else
             {
                 labelResult.Text = "设置失败";
             }*/
            try
            {
                if (currentClient != null)
                {
                    AdjacentJudgment adjacentJudgmentev = new AdjacentJudgment
                    {
                        value = (ushort)neighJudge,
                    };
                    reader.SetAdjacentJudgment(currentClient, adjacentJudgmentev);
                    if (adjacentJudgmentev.value == 0x00)
                    {
                        MessageBox.Show(rm.GetString("strReboot"));
                    }
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnGetDevNo_Click(object sender, EventArgs e)
        {
            /* labelResult.Text = "";
             byte devNo = 0;
             if (0 == NR2k.GetDeviceNo(currentDevice.hDev, out devNo))
             {
                 tbDevNo.Text = devNo.ToString();
             }
             else
             {
                 labelResult.Text = "读设备号失败";
             }*/
            try
            {
                if (currentClient != null)
                {
                    reader.GetDev(currentClient);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private void btnGetNeighJudge_Click(object sender, EventArgs e)
        {
            /* labelResult.Text = "";
             byte enableNJ = 0;
             byte njTime = 0;
             if (0 == NR2k.GetNeighJudge(currentDevice.hDev, out enableNJ, out njTime))
             {
                 tbNeighJudge.Text = njTime.ToString();
             }
             else
             {
                 labelResult.Text = "读相邻判定失败";
             }*/
            try
            {
                if (currentClient != null)
                {
                    reader.GetAdjacentJudgment(currentClient);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnReadData_Click(object sender, EventArgs e)
        {

            try
            {
                int RWBank = -1;
                int StartAdd = -1;
                int Length = -1;
                labResult.Text = "";
                if ((RWBank = cbbRWBank.SelectedIndex) == -1)
                {
                    MessageBox.Show(rm.GetString("strMsgSelectRWBank"));// rm.GetString("strMsgSelectRWBank")
                    return;
                }

                StartAdd = int.Parse(cbbStartAdd.Text);
                Length = int.Parse(cbbLength.Text);

                string strpwd = tbRWAccessPwd.Text.Replace(" ", "");
                if (strpwd.Length != 8)
                {
                    MessageBox.Show(rm.GetString("strMsgPwdMustEight")); // rm.GetString("strMsgPwdMustEight")
                    return;
                }
                if (!IsHexCharacter(strpwd))
                {
                    MessageBox.Show(rm.GetString("strMsgPwdMustEight"));// rm.GetString("strMsgPwdInvalidChar")
                    return;
                }

                // 转密码框的字符转为byte数组
                byte[] pwd = new byte[4];
                for (int i = 0; i < 4; ++i)
                {
                    pwd[i] = Convert.ToByte(strpwd.Substring(i * 2, 2), 16); // 把字符串的子串转为16进制的8位无符号整数
                }
                byte[] byteArray = new byte[64];
                tbRWData.Text = "";
                ReadWriteTag readTag = new ReadWriteTag();
                readTag.state = 0x00;
                readTag.bank = (byte)RWBank;
                readTag.offset = (byte)StartAdd;
                readTag.len = (byte)Length;
                readTag.password = pwd;
                if (currentClient != null)
                {
                    reader.ReadTagData(currentClient, readTag);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnClearData_Click(object sender, EventArgs e)
        {
            tbRWData.Text = "";
        }

        private void btnWriteData_Click(object sender, EventArgs e)
        {

            try
            {
                int RWBank = -1;
                int StartAdd = -1;
                int Length = -1;
                labResult.Text = "";
                if ((RWBank = cbbRWBank.SelectedIndex) == -1)
                {
                    MessageBox.Show(rm.GetString("strMsgSelectRWBank"));//rm.GetString("strMsgSelectRWBank")
                    return;
                }

                StartAdd = int.Parse(cbbStartAdd.Text);
                Length = int.Parse(cbbLength.Text);

                string strpwd = tbRWAccessPwd.Text.Replace(" ", "");
                if (strpwd.Length != 8)
                {
                    MessageBox.Show(rm.GetString("strMsgPwdMustEight"));
                    return;
                }
                if (!IsHexCharacter(strpwd))
                {
                    MessageBox.Show(rm.GetString("strMsgPwdMustEight"));
                    return;
                }

                byte[] pwd = new byte[4];
                for (int i = 0; i < 4; ++i)
                {
                    pwd[i] = Convert.ToByte(strpwd.Substring(i * 2, 2), 16); // 把字符串的子串转为16进制的8位无符号整数
                }

                string strData = tbRWData.Text.Replace(" ", "");// 去空格
                if (strData.Length % 4 != 0 || strData.Length / 4 != Length)
                {
                    MessageBox.Show(rm.GetString("strMsgLengthDiff"));// rm.GetString("strMsgLengthDiff")
                    return;
                }
                if (!IsHexCharacter(strData))
                {
                    MessageBox.Show(rm.GetString("strMsgNotDigit"));
                    return;
                }
                byte[] byteArray = new byte[Length * 2];
                for (int i = 0; i < 2 * Length; ++i)
                {
                    byteArray[i] = Convert.ToByte(strData.Substring(2 * i, 2), 16);
                }
                ReadWriteTag readTag = new ReadWriteTag();
                readTag.state = 0x02;
                readTag.bank = (byte)RWBank;
                readTag.offset = (byte)StartAdd;
                readTag.len = (byte)Length;
                readTag.password = pwd;
                readTag.data = byteArray;
                if (currentClient != null)
                {
                    reader.WriteTagData(currentClient, readTag);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private bool IsHexCharacter(string str)
        {
            return Regex.IsMatch(str, "^[0-9A-Fa-f]+$");
        }

        private void cbbRWBank_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 根据操作区域，确定有效的起始地址
            if (cbbRWBank.SelectedIndex == 0) // 保留区
            {
                RWBank = 0;
                addStart = 0;
                addEnd = 3;
            }
            else if (cbbRWBank.SelectedIndex == 1) // EPC区
            {
                RWBank = 1;
                addStart = 2;
                addEnd = 7;
            }
            else if (cbbRWBank.SelectedIndex == 2) // TID
            {
                RWBank = 2;
                addStart = 0;
                addEnd = 5;
            }
            else if (cbbRWBank.SelectedIndex == 3) // User
            {
                RWBank = 3;
                addStart = 0;
                addEnd = 95;
            }
            cbbStartAdd.Items.Clear();
            for (int i = addStart; i <= addEnd; ++i)
            {
                cbbStartAdd.Items.Add(i.ToString());
            }
            cbbStartAdd.SelectedIndex = 0;
        }

        private void cbbStartAdd_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 根据起始地址，确定长度选项
            int nItem = cbbStartAdd.SelectedIndex; //取出起始地址索引值
            int maxLength = addEnd - addStart - nItem + 1;
            cbbLength.Items.Clear();
            for (int i = 1; i <= maxLength; ++i)
            {
                cbbLength.Items.Add(i.ToString());
            }
            try
            {
                cbbLength.SelectedIndex = cbbLength.Items.Count - 1;
            }
            catch
            {
                cbbLength.SelectedIndex = 0;
            }

        }

        private void btnLockTag_Click(object sender, EventArgs e)
        {
            int lockBank = -1;
            int locktType = -1;
            labResult.Text = "";
            if ((lockBank = cbbLockBank.SelectedIndex) == -1)
            {
                MessageBox.Show(rm.GetString("strMsgSelecOprBank"));
                return;
            }
            if ((locktType = cbbLockType.SelectedIndex) == -1)
            {
                MessageBox.Show(rm.GetString("strMsgSelectOprType"));
                return;
            }
            string strpwd = tbLockAccessPwd.Text.Replace(" ", "");
            if (strpwd.Length != 8)
            {
                MessageBox.Show(rm.GetString("strMsgPwdMustEight"));
                return;
            }
            if (!IsHexCharacter(strpwd))
            {
                MessageBox.Show(rm.GetString("strMsgPwdMustEight"));
                return;
            }

            byte[] pwd = new byte[4];
            for (int i = 0; i < 4; ++i)
            {
                pwd[i] = Convert.ToByte(strpwd.Substring(i * 2, 2), 16); // 把字符串的子串转为16进制的8位无符号整数
            }
            /*  if (0 == NR2k.LockTag(currentDevice.hDev, (byte)locktType, (byte)lockBank, pwd))
              {
                  labResult.Text = "操作成功";
              }
              else
              {
                  labResult.Text = "操作失败";
              }*/
            try
            {
                LockTag lockTag = new LockTag();
                lockTag.operation = (byte)locktType;
                lockTag.block = (byte)lockBank;
                lockTag.password = pwd;

                if (currentClient != null)
                {
                    if (lockTag.block == 0x00)
                    {
                        KIllTag killTag = new KIllTag();
                        killTag.accessPassWord = pwd;
                        string killpwd = KillTagPWDTB.Text.Replace(" ", "");
                        if (killpwd.Length != 8)
                        {
                            MessageBox.Show(rm.GetString("strMsgPwdMustEight"));
                            return;
                        }
                        if (!IsHexCharacter(killpwd))
                        {
                            MessageBox.Show(rm.GetString("strMsgPwdMustEight"));
                            return;
                        }
                        byte[] killPwd = new byte[4];
                        for (int i = 0; i < 4; ++i)
                        {
                            killPwd[i] = Convert.ToByte(killpwd.Substring(i * 2, 2), 16); // 把字符串的子串转为16进制的8位无符号整数
                        }
                        killTag.killPassWord = killPwd;
                        reader.KIllTag(currentClient, killTag);
                    }
                    else
                    {
                        reader.LockTag(currentClient, lockTag);
                    }


                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return;
        }

        //private void btnKillTag_Click(object sender, EventArgs e)
        //{
        //    labResult.Text = "";
        //    string strAccessPwd = tbKillAccessPwd.Text.Replace(" ", "");
        //    string strKillPwd = tbKillKillPwd.Text.Replace(" ", "");
        //    if (!IsHexCharacter(strAccessPwd) || !IsHexCharacter(strKillPwd))
        //    {
        //        MessageBoxBox.Show("密码中有无效字符");
        //        return;
        //    }
        //    if (strAccessPwd.Length != 8 || strKillPwd.Length != 8)
        //    {
        //        MessageBoxBox.Show("密码必须8位");
        //        return;
        //    }
        //    byte[] byteAccessPwd = new byte[4];
        //    byte[] byteKillPwd = new byte[4];
        //    for (int i = 0; i < 4; ++i)
        //    {
        //        byteAccessPwd[i] = Convert.ToByte(strAccessPwd.Substring(i * 2, 2), 16);
        //        byteKillPwd[i] = Convert.ToByte(strKillPwd.Substring(i * 2, 2), 16);
        //    }
        //    if (0 == NR2k.KillTag(currentDevice.hDev, byteAccessPwd, byteKillPwd))
        //    {
        //        labResult.Text = "操作成功";
        //    }
        //    else
        //    {
        //        labResult.Text = "操作失败";
        //    }
        //}

        private void btnDefaultParams_Click(object sender, EventArgs e)
        {
            comboBoxNetMode.SelectedIndex = 1;
            comboBoxIPMode.SelectedIndex = 0;
            textBoxIPAdd.Text = "192.168.1.200";
            textBoxNetMask.Text = "255.255.255.0";
            textBoxPortNo.Text = "20058";
            textBoxGateway.Text = "192.168.1.1";
            textBoxDestIP.Text = "192.168.1.100";
            textBoxDestPort.Text = "20058";
            comboBoxBaudRate.SelectedIndex = 12;
            comboBoxDataBits.SelectedIndex = 0;
            comboBoxCheckBits.SelectedIndex = 0;
        }

        private void btnSearchDev_Click(object sender, EventArgs e)
        {
            lvZl.Items.Clear();
            ZLDM.m_DevCnt = ZLDM.StartSearchDev();
            for (byte i = 0; i < ZLDM.m_DevCnt; ++i)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.SubItems.Add(Marshal.PtrToStringAnsi(ZLDM.GetIP(i)));
                lvi.SubItems.Add(ReverseByte(ZLDM.GetPort(i)).ToString());
                lvi.SubItems.Add(Marshal.PtrToStringAnsi(ZLDM.GetDevID(i)));
                lvZl.Items.Add(lvi);
            }
            if (ZLDM.m_DevCnt > 0)
            {
                UpdateCommParamControl(); // 更新页面控件
                lvZl.FocusedItem = lvZl.Items[0];// 设置第一项为焦点项
            }
        }
        private void UpdateCommParamControl()
        {
            if (ZLDM.m_DevCnt > 0)
            {
                comboBoxNetMode.SelectedIndex = ZLDM.GetWordMode(ZLDM.m_SelectedDevNo);
                comboBoxIPMode.SelectedIndex = ZLDM.GetIPMode(ZLDM.m_SelectedDevNo);
                textBoxIPAdd.Text = Marshal.PtrToStringAnsi(ZLDM.GetIP(ZLDM.m_SelectedDevNo));
                textBoxNetMask.Text = Marshal.PtrToStringAnsi(ZLDM.GetNetMask(ZLDM.m_SelectedDevNo));
                textBoxPortNo.Text = ReverseByte(ZLDM.GetPort(ZLDM.m_SelectedDevNo)).ToString();
                textBoxGateway.Text = Marshal.PtrToStringAnsi(ZLDM.GetGateWay(ZLDM.m_SelectedDevNo));
                textBoxDestIP.Text = Marshal.PtrToStringAnsi(ZLDM.GetDestName(ZLDM.m_SelectedDevNo));
                textBoxDestPort.Text = ReverseByte(ZLDM.GetDestPort(ZLDM.m_SelectedDevNo)).ToString();
                comboBoxBaudRate.SelectedIndex = ZLDM.GetBaudrateIndex(ZLDM.m_SelectedDevNo);
                comboBoxDataBits.SelectedIndex = ZLDM.GetDataBits(ZLDM.m_SelectedDevNo);
                comboBoxCheckBits.SelectedIndex = ZLDM.GetParity(ZLDM.m_SelectedDevNo);
            }
        }
        // 大小端转换
        public static UInt16 ReverseByte(UInt16 value)
        {
            return (UInt16)((value & 0xFFU) << 8 | (value & 0xFF00U) >> 8);
        }

        private void lvZl_ItemActivate(object sender, EventArgs e)
        {
            if (lvZl.Items.Count > 0)
            {
                ZLDM.m_SelectedDevNo = (byte)(lvZl.Items.IndexOf(lvZl.FocusedItem));
                UpdateCommParamControl();
            }
        }

        private void btnModifyDev_Click(object sender, EventArgs e)
        {
            if (lvZl.SelectedItems != null)
                lvZl_ItemActivate(sender, e);
        }

        private void btnSetParams_Click(object sender, EventArgs e)
        {
            if (-1 == comboBoxNetMode.SelectedIndex)
            {
                MessageBox.Show(rm.GetString("strMsgSelectNetMode"));
                return;
            }
            if (-1 == comboBoxIPMode.SelectedIndex)
            {
                MessageBox.Show(rm.GetString("strMsgSelectIPMode"));
                return;
            }
            if (-1 == comboBoxBaudRate.SelectedIndex)
            {
                MessageBox.Show(rm.GetString("strMsgSelectBaudRate"));
                return;
            }
            if (-1 == comboBoxDataBits.SelectedIndex)
            {
                MessageBox.Show(rm.GetString("strMsgSelectDataBits"));
                return;
            }
            if (-1 == comboBoxCheckBits.SelectedIndex)
            {
                MessageBox.Show(rm.GetString("strMsgSelectParity"));
                return;
            }

            ushort port = ReverseByte(ushort.Parse(textBoxPortNo.Text));
            ushort destport = ReverseByte(ushort.Parse(textBoxDestPort.Text));

            byte[] ip = new byte[32];
            byte[] netmask = new byte[32];
            byte[] gateway = new byte[32];
            byte[] destip = new byte[32];
            ip = Encoding.ASCII.GetBytes(textBoxIPAdd.Text);
            netmask = Encoding.ASCII.GetBytes(textBoxNetMask.Text);
            gateway = Encoding.ASCII.GetBytes(textBoxGateway.Text);
            destip = Encoding.ASCII.GetBytes(textBoxDestIP.Text);

            ZLDM.SetWorkMode(ZLDM.m_SelectedDevNo, (byte)comboBoxNetMode.SelectedIndex);
            ZLDM.SetIPMode(ZLDM.m_SelectedDevNo, (byte)comboBoxIPMode.SelectedIndex);


            ZLDM.SetIP(ZLDM.m_SelectedDevNo, ip);
            ZLDM.SetNetMask(ZLDM.m_SelectedDevNo, netmask);
            ZLDM.SetPort(ZLDM.m_SelectedDevNo, port);
            ZLDM.SetGateWay(ZLDM.m_SelectedDevNo, gateway);

            ZLDM.SetDestName(ZLDM.m_SelectedDevNo, destip);
            ZLDM.SetDestPort(ZLDM.m_SelectedDevNo, destport);

            ZLDM.SetBaudrateIndex(ZLDM.m_SelectedDevNo, (byte)comboBoxBaudRate.SelectedIndex);
            ZLDM.SetDataBits(ZLDM.m_SelectedDevNo, (byte)comboBoxDataBits.SelectedIndex);
            ZLDM.SetParity(ZLDM.m_SelectedDevNo, (byte)comboBoxCheckBits.SelectedIndex);

            bool res = ZLDM.SetParam(ZLDM.m_SelectedDevNo);
            if (res)
            {
                MessageBox.Show(rm.GetString("strMsgSucceedSetCommParam"));
                string[] key = new string[] { "ServerPort" };
                string[] value = new string[] { textBoxDestPort.Text };
                SaveConfig(key, value);
                ShowResult(rm.GetString("strMsgSucceedSetCommParam"));
            }
            else
            {
                MessageBox.Show(rm.GetString("strMsgFailedSetCommParam"));
            }

        }

        private void SaveConfig(string[] key, string[] value)
        {
            if (key.Length != value.Length)
            {
                return;
            }
            else
            {
                Configuration cfa = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                for (int index = 0; index < key.Length; index++)
                {
                    cfa.AppSettings.Settings[key[index]].Value = value[index];
                }
                cfa.Save();
                System.Configuration.ConfigurationManager.RefreshSection("appSettings");
            }
        }

        private void btnGetBuzzer_Click(object sender, EventArgs e)
        {
            /* labelResult.Text = "";
             byte status;
             if (0 == NR2k.GetBuzzer(currentDevice.hDev, out status))
             {
                 if (1 == status)
                 {
                     rdoBuzzerOn.Checked = true;
                 }
                 else
                 {
                     rdoBuzzerOff.Checked = true;
                 }
             }*/
            try
            {
                if (currentClient != null)
                {
                    reader.GetBuzzer(currentClient);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnSetBuzzer_Click(object sender, EventArgs e)
        {
            labelResult.Text = "";
            byte status = 0;
            if (rdoBuzzerOn.Checked)
            {
                status = 1;
            }
            else if (rdoBuzzerOff.Checked)
            {
                status = 0;
            }
            else
            {
                MessageBox.Show("请选择一个状态再设置");
                return;
            }
            /* if (0 == NR2k.SetBuzzer(currentDevice.hDev, status))
             {
                 labelResult.Text = "设置成功";
             }
             else
             { 
                 labelResult.Text = "设置失败";
             }*/
            try
            {
                if (currentClient != null)
                {
                    Buzzer buzzer = new Buzzer
                    {
                        status = status,
                    };
                    reader.SetBuzzer(currentClient, buzzer);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnGetWorkMode_Click(object sender, EventArgs e)
        {
            /* labelResult.Text = "";
             byte mode = 0;
             if (0 == NR2k.GetWorkMode(currentDevice.hDev, out mode))
             {
                 comboBoxWorkMode.SelectedIndex = mode - 1;
             }
             else
             {
                 labelResult.Text = "读取工作模式失败";
             }*/
            try
            {
                if (currentClient != null)
                {
                    reader.GetWorkMode(currentClient);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnSetWorkMode_Click(object sender, EventArgs e)
        {
            labelResult.Text = "";
            byte mode = (byte)(comboBoxWorkMode.SelectedIndex + 1);
            if (mode == 0)
            {
                MessageBox.Show(rm.GetString("strMsgSelectWorkMode"));
                return;
            }
            /*  if (0 == NR2k.SetWorkMode(currentDevice.hDev, mode))
              {
                  labelResult.Text = "设置成功";
              }
              else
              {
                  labelResult.Text = "设置失败";
              }*/
            try
            {
                if (currentClient != null)
                {
                    WorkMode workmode = new WorkMode
                    {
                        workMode = mode,
                    };
                    reader.SetWorkMode(currentClient, workmode);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnGetDelayTime_Click(object sender, EventArgs e)
        {
            /* labelResult.Text = "";
             byte mode = 0;
             if (0 == NR2k.GetTrigModeDelayTime(currentDevice.hDev, out mode))
             {
                 tbDelayTime.Text = mode.ToString();
             }
             else
             {
                 labelResult.Text = "读取触发延时失败";
             }*/
            try
            {
                if (currentClient != null)
                {
                    reader.GetTriggerModeDalayTime(currentClient);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnSetDelayTime_Click(object sender, EventArgs e)
        {
            labelResult.Text = "";
            if (!Regex.IsMatch(tbDelayTime.Text, "^[0-9]+$"))
            {
                MessageBox.Show(rm.GetString("strMsgNotDigit"));
                tbDelayTime.Text = "";
                return;
            }
            int trigTime = int.Parse(tbDelayTime.Text);
            if (trigTime == 0 || trigTime > 255)
            {
                MessageBox.Show(rm.GetString("strFilterInValue"));
                tbDelayTime.Text = "";
                return;
            }
            /* if (0 == NR2k.SetTrigModeDelayTime(currentDevice.hDev, (byte)trigTime))
             {
                 labelResult.Text = "设置成功";
             }
             else
             {
                 labelResult.Text = "设置失败";
             }*/
            try
            {
                if (currentClient != null)
                {
                    TriggerModeDalayTime time = new TriggerModeDalayTime
                    {
                        time = (byte)trigTime,
                    };
                    reader.SetTriggerModeDalayTime(currentClient, time);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnUpdateClock_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            //  dtpClock.Value = dt;
        }


        private void tvDevList_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            currentDevice.host = e.Node.Text;
            currentDevice.hDev = 0;
        }

        private async void ReadClock1_Tick(object sender, EventArgs e)
        {
            if (isMUltiEPC)
            {
                string x1 = "", x2 = "", x3 = "";

                TimeSpan ts = sw.Elapsed;
                if (ts.Hours < 10)
                { x1 = "0" + ts.Hours; }
                else
                { x1 = "0" + ts.Hours; }
                if (ts.Minutes < 10)
                { x2 = "0" + ts.Minutes; }
                else
                { x2 = "" + ts.Minutes; }

                if (ts.Seconds < 10)
                { x3 = "0" + ts.Seconds; }
                else
                { x3 = "" + ts.Seconds; }
                labReadClock.Text = x1 + ":" + x2 + ":" + x3;
            }
            await UpdateListView();
        }


        private async void readonceClock_Tick(object sender, EventArgs e)
        {
            lvTagData.Items.Clear();
            await UpdateListView();

            readonceClock.Enabled = false;
            btnInvOnce.Enabled = true;

            btnInvOnce.Enabled = true;
            btnClear.Enabled = true;
            btnReadBuffer.Enabled = true;
            btnClearBuffer.Enabled = true;
            btnReadBuffer.Enabled = true;
            btnClearBuffer.Enabled = true;
            //  btnGetAnts.Enabled = true;
            btnSetAnts.Enabled = true;
            tabControl1.TabPages[1].Enabled = true;
            tabControl1.TabPages[2].Enabled = true;
            tabControl1.TabPages[3].Enabled = true;
            //make changes gere?
        }

        private void tvDevList_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void Form_Closed(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(rm.GetString("strExit"), rm.GetString("strExitTips"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                services.Dispose();
            }
            // MessageBoxBoxEx(NULL, L"This is an English MessageBoxBox!", L"Alert", MB_OKCANCEL, MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US));
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void RefreshTimeSetBtn_Click(object sender, EventArgs e)
        {
            try
            {

                ReadClock1.Interval = 50;
                if (ReadClock1.Interval < 10)
                {
                    ReadClock1.Interval = 10;
                }
                ShowResult("设置数据刷新时间", 0x00);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void ServerStartBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (ServerStartBtn.Text == rm.GetString("Open") + rm.GetString("strServer"))
                {
                    bool result = reader.ServerStart(ServerIPTB.Text, ushort.Parse(ServerPortTB.Text), 0x00);
                    if (rm != null)
                    {
                        ServerStartBtn.Text = rm.GetString("Close") + rm.GetString("strServer");
                    }
                    NetConnectState = true;
                    ServerStartBtn.BackColor = Color.LightGreen;
                    ShowResult(rm.GetString("Open") + rm.GetString("strServer"), 0x00, "," + rm.GetString("strWaitConnect"));
                    comOpenBtn.Enabled = false;
                }
                else
                {
                    reader.ServerClose();
                    if (rm != null)
                    {
                        ServerStartBtn.Text = rm.GetString("Open") + rm.GetString("strServer");
                    }
                    NetConnectState = false;
                    ServerStartBtn.BackColor = Color.LightGray;
                    ShowResult(rm.GetString("Close") + rm.GetString("strServer"), 0x00);
                    comOpenBtn.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }


        private void button1_Click_1(object sender, EventArgs e)
        {
            //  List<Client> clientList = reader.GetClients();
            if (currentClient != null)
            {
                int result = reader.GetVersion(currentClient);
            }
            else
            {
                MessageBox.Show(noDeviceChoose);
            }
        }

        private void GetClientsInfoTimer_Tick(object sender, EventArgs e)
        {
            clientList = reader.GetClients();
            ClientInfoLV.Items.Clear();
            bool isNewClient = true;
            int no = 0;
            lock (clientList)
            {
                foreach (Client clientInfo in clientList)
                {
                    for (int index = 0; index < ClientInfoLV.Items.Count; index++)
                    {
                        if (clientInfo.IpAddress == ClientInfoLV.Items[index].SubItems[1].Text && clientInfo.ConnId.ToString() == ClientInfoLV.Items[index].SubItems[4].Text)
                        {
                            ClientInfoLV.Items[index].SubItems[3].Text = clientInfo.State;
                            isNewClient = false;
                            if (currentClient != null)
                            {
                                if (ClientInfoLV.Items[index].SubItems[1].Text == currentClient.IpAddress && currentClient.ConnId.ToString() == ClientInfoLV.Items[index].SubItems[4].Text)
                                {
                                    ClientInfoLV.Items[index].BackColor = Color.CadetBlue;
                                }
                            }
                            break;
                        }
                    }
                    if (isNewClient)
                    {
                        no++;
                        ListViewItem lvi = new ListViewItem();
                        lvi.Text = (ClientInfoLV.Items.Count + 1).ToString();
                        lvi.SubItems.Add(clientInfo.IpAddress);
                        lvi.SubItems.Add(clientInfo.Port.ToString());
                        lvi.SubItems.Add(clientInfo.State);
                        lvi.SubItems.Add(clientInfo.ConnId.ToString());
                        //   lvi.SubItems.Add(new MyListViewSubItem { counID = (long)clientInfo.ConnId});
                        ClientInfoLV.Items.Add(lvi);
                        //     ClientInfoLV.Items[ClientInfoLV.Items.Count - 1].EnsureVisible();
                        if (currentClient != null)
                        {
                            if (clientInfo.IpAddress == currentClient.IpAddress && currentClient.ConnId.ToString() == clientInfo.ConnId.ToString())
                            {
                                ClientInfoLV.Items[ClientInfoLV.Items.Count - 1].BackColor = Color.CadetBlue;
                            }
                        }
                    }
                    isNewClient = true;
                }
                /* bool isExit = false;
                 foreach (ListViewItem lvi in ClientInfoLV.Items)
                 {
                     int index = 0;
                     for (; index < clientList.Count; index++)
                     {
                         if (clientList[index].IpAddress == lvi.SubItems[1].Text)
                         {
                             isExit = true;
                             break;
                         }
                     }
                     if (!isExit)
                     {
                         ClientInfoLV.Items.Remove(lvi);
                     }
                     isNewClient = true;
                 }*/
            }
        }


        private void ClientInfoLV_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ClientInfoLV.SelectedIndices != null && ClientInfoLV.SelectedIndices.Count > 0)
            {
                ListView.SelectedIndexCollection c = ClientInfoLV.SelectedIndices;
                foreach (Client cli in clientList)
                {
                    if (cli.IpAddress == ClientInfoLV.Items[c[0]].SubItems[1].Text && cli.Port.ToString() == ClientInfoLV.Items[c[0]].SubItems[2].Text && cli.ConnId.ToString() == ClientInfoLV.Items[c[0]].SubItems[4].Text)
                    {
                        currentClient = cli;
                        CurrentClientLB.Text = ClientInfoLV.Items[c[0]].SubItems[1].Text + "  " + ClientInfoLV.Items[c[0]].SubItems[2].Text;
                        break;
                    }

                }
            }
        }

        private void ClientInfoLV_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ;
        }

        private void gbDevParams_Enter(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            reader.ReFresh();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (comOpenBtn.Text == rm.GetString("strBtnConnect"))
            {
                if (reader.ComStart(serialPortCB.Text, 115200, 0, 8, 1, 0))
                {
                    ShowResult(rm.GetString("strOpen") + rm.GetString("strRbSerialPort"), 0x00);
                }
                else
                {
                    ShowResult(rm.GetString("strOpen") + rm.GetString("strRbSerialPort"), 0x80);
                }
                comOpenBtn.Text = rm.GetString("strBtnDisconnect");
                COMConnectState = true;
                comOpenBtn.BackColor = Color.LightGreen;
                ServerStartBtn.Enabled = false;

            }
            else
            {
                if (reader.ComClose())
                {
                    ShowResult(rm.GetString("Close") + rm.GetString("strRbSerialPort"), 0x00);
                }
                else
                {
                    ShowResult(rm.GetString("Close") + rm.GetString("strRbSerialPort"), 0x80);
                }
                comOpenBtn.BackColor = Color.LightGray;
                comOpenBtn.Text = rm.GetString("strBtnConnect");
                COMConnectState = false;
                ServerStartBtn.Enabled = true;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            GetSerialPortList(this.serialPortCB);
            GetLoaclIP(this.ServerIPTB);
            ReadConfig();
            ShowResult(rm.GetString("strAutoGetConnectMode"), 0x00);
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*  if ((NetConnectState || COMConnectState) && currentClient != null)
              {
                  if (isMUltiEPC)
                  {
                      if (tabControl1.SelectedIndex != 0)
                      {
                          tabControl1.SelectedIndex = 0;
                          MessageBoxBox.Show(rm.GetString("strMultiTips"));
                      }
                  }
                  else
                  {
                      if (tabControl1.SelectedIndex == 2)           //自动获取参数
                      {
                          btnGetDevNo_Click(null, null);
                          btnGetBuzzer_Click(null, null);
                          btnGetWorkMode_Click(null, null);
                          btnGetDelayTime_Click(null, null);
                          btnGetNeighJudge_Click(null, null);

                      }
                  }
              }*/
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (HeartBeatOnRB.Checked == true)
            {
                reader.StartHeartBeatServer(5000);
            }
            else
            {
                reader.StopHeartBeatServer();
            }
        }

        private void lvZl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvZl.SelectedIndices != null && lvZl.SelectedIndices.Count > 0)
            {
                ZLDM.m_SelectedDevNo = (byte)(lvZl.SelectedItems[0].Index);
                UpdateCommParamControl();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                DataChannel channel = new DataChannel
                {
                    channel = (byte)(DataChannelCB.SelectedIndex + 1),
                };
                if (currentClient != null)
                {
                    reader.SetDataChannel(currentClient, channel);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void TriggerAlarmBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (ConstantfrequencyTB.Text == "")
                {
                    MessageBox.Show(rm.GetString("TriggerAlarmTimeTB"));
                    return;
                }
                TriggerAlarmTime time = new TriggerAlarmTime
                {
                    time = byte.Parse(TriggerAlarmTimeTB.Text),
                };
                if (currentClient != null)
                {
                    reader.SetTriggerAlarmTime(currentClient, time);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {

                if (currentClient != null)
                {
                    /*  FrequencyArea area = new FrequencyArea
                      {
                          area = (byte)(FrequencyAreaCB.SelectedIndex + 1),
                      };
                      byte showArea = area.area;
                      reader.SetFrequencyArea(currentClient, area);*/
                    byte area = (byte)(FrequencyAreaCB.SelectedIndex + 1);
                    CustomFrequcney frequency = new CustomFrequcney();
                    frequency.points = new List<byte>();
                    frequency.type = area;
                    if (area == 0x05)                 //自定义频点
                    {
                        for (int index = 0; index < 50; index++)
                        {
                            if (frequencysCB[index].Checked == true)
                            {
                                frequency.points.Add((byte)index);
                            }
                        }
                    }
                    if (area == 0x06)                      //定频
                    {
                        if (ConstantfrequencyTB.Text == "")
                        {
                            MessageBox.Show(rm.GetString("strMsgDevNoNotEmpty"));
                            return;
                        }
                        if (float.Parse(ConstantfrequencyTB.Text) > 1000)
                        {
                            MessageBox.Show(rm.GetString("strInvalid1000"));
                            return;
                        }
                        frequency.point = (int)(float.Parse(ConstantfrequencyTB.Text) * 1000);
                    }
                    reader.SetFrequency(currentClient, frequency);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                if (currentClient != null)
                {
                    reader.GetFrequency(currentClient);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void checkBox11_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox11.Checked)
            {
                frequency1.Checked = true; frequency2.Checked = true; frequency3.Checked = true; frequency4.Checked = true; frequency5.Checked = true;
                frequency6.Checked = true; frequency7.Checked = true; frequency8.Checked = true; frequency9.Checked = true; frequency10.Checked = true;
            }
            else
            {
                frequency1.Checked = false; frequency2.Checked = false; frequency3.Checked = false; frequency4.Checked = false; frequency5.Checked = false;
                frequency6.Checked = false; frequency7.Checked = false; frequency8.Checked = false; frequency9.Checked = false; frequency10.Checked = false;
            }
        }

        private void checkBox12_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox12.Checked)
            {
                frequency20.Checked = true; frequency19.Checked = true; frequency18.Checked = true; frequency17.Checked = true; frequency16.Checked = true;
                frequency15.Checked = true; frequency14.Checked = true; frequency13.Checked = true; frequency12.Checked = true; frequency11.Checked = true;
            }
            else
            {
                frequency20.Checked = false; frequency19.Checked = false; frequency18.Checked = false; frequency17.Checked = false; frequency16.Checked = false;
                frequency15.Checked = false; frequency14.Checked = false; frequency13.Checked = false; frequency12.Checked = false; frequency11.Checked = false;
            }
        }

        private void checkBox34_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox34.Checked)
            {
                frequency30.Checked = true; frequency29.Checked = true; frequency28.Checked = true; frequency27.Checked = true; frequency26.Checked = true;
                frequency25.Checked = true; frequency24.Checked = true; frequency23.Checked = true; frequency22.Checked = true; frequency21.Checked = true;
            }
            else
            {
                frequency30.Checked = false; frequency29.Checked = false; frequency28.Checked = false; frequency27.Checked = false; frequency26.Checked = false;
                frequency25.Checked = false; frequency24.Checked = false; frequency23.Checked = false; frequency22.Checked = false; frequency21.Checked = false;
            }
        }

        private void checkBox23_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox23.Checked)
            {
                frequency40.Checked = true; frequency39.Checked = true; frequency38.Checked = true; frequency37.Checked = true; frequency36.Checked = true;
                frequency35.Checked = true; frequency34.Checked = true; frequency33.Checked = true; frequency32.Checked = true; frequency31.Checked = true;
            }
            else
            {
                frequency40.Checked = false; frequency39.Checked = false; frequency38.Checked = false; frequency37.Checked = false; frequency36.Checked = false;
                frequency35.Checked = false; frequency34.Checked = false; frequency33.Checked = false; frequency32.Checked = false; frequency31.Checked = false;
            }
        }

        private void checkBox45_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox45.Checked)
            {
                frequency50.Checked = true; frequency49.Checked = true; frequency48.Checked = true; frequency47.Checked = true; frequency46.Checked = true;
                frequency45.Checked = true; frequency44.Checked = true; frequency43.Checked = true; frequency42.Checked = true; frequency41.Checked = true;
            }
            else
            {
                frequency50.Checked = false; frequency49.Checked = false; frequency48.Checked = false; frequency47.Checked = false; frequency46.Checked = false;
                frequency45.Checked = false; frequency44.Checked = false; frequency43.Checked = false; frequency42.Checked = false; frequency41.Checked = false;
            }
        }

        ResourceManager rm = null;
        private bool isStartIntelligentDetection = true;

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (languageSelectCB.SelectedIndex == 0x00)    //中文简体
            {
                rm = rmArray[0];
                language = 0x00;
                this.Font = new Font("黑体", (float)10.0, FontStyle.Regular);
            }
            else if (languageSelectCB.SelectedIndex == 0x01)   //English
            {
                rm = rmArray[1];
                language = 0x01;
                this.Font = new Font("黑体", (float)10.0, FontStyle.Regular);
            }
            UpdateView();
        }

        private void UpdateView()
        {
            //Main page
            ClientInfoLV.Columns[1].Text = rm.GetString("strIPOrPort");
            ClientInfoLV.Columns[2].Text = rm.GetString("strZlHeadPort");
            ClientInfoLV.Columns[3].Text = rm.GetString("strState");
            AutoConnect.Text = rm.GetString("strAutoGetConnectMode");
            AutoGetConnBT.Text = rm.GetString("strBtnGet");
            NetWorkModeCB.Text = rm.GetString("strLabNetMode");
            ConnectSeverIPLB.Text = rm.GetString("strZlHeadIP");
            ConnectSeverPortLB.Text = rm.GetString("strZlHeadPort");
            ConnectSerialCB.Text = rm.GetString("strRbSerialPort");
            SerialNoLB.Text = rm.GetString("strRbSerialPort");
            Version.Text = rm.GetString("strVersion");
            tabControl1.TabPages[0].Text = rm.GetString("strTpGeneral");
            tabControl1.TabPages[1].Text = rm.GetString("strTpTagAccess");
            tabControl1.TabPages[2].Text = rm.GetString("strTpSetReaderParam");
            tabControl1.TabPages[3].Text = rm.GetString("strTpSetCommParam");
            if (tabSpecialFun.Parent != null)
                tabControl1.TabPages[4].Text = rm.GetString("strSpecialFun");
            label11.Text = rm.GetString("strCurrentDev");
            label12.Text = rm.GetString("strOperResult");
            button1.Text = rm.GetString("strBtnGet");
            button2.Text = rm.GetString("strRefresh");
            //  label13.Text = rm.GetString("strTimeInterval");
            //   ReadCountLB.Text = rm.GetString("strCount");
            ReadSpeedSLb.Text = rm.GetString("strSpeed");
            if (NetConnectState)
            {
                ServerStartBtn.Text = rm.GetString("Close") + rm.GetString("strServer");
            }
            else
            {
                ServerStartBtn.Text = rm.GetString("Open") + rm.GetString("strServer");
            }
            if (COMConnectState)
            {
                comOpenBtn.Text = rm.GetString("strBtnDisconnect");
            }
            else
            {
                comOpenBtn.Text = rm.GetString("strBtnConnect");
            }

            //
            label18.Text = rm.GetString("strQuecyTimeLong");
            label19.Text = rm.GetString("strTIDCounts");
            TotalTagLB.Text = rm.GetString("strEPCCounts");
            ReadCountLB.Text = rm.GetString("strTagCounts");
            label8.Text = rm.GetString("strTimes");
            label9.Text = rm.GetString("strTimes");
            label6.Text = rm.GetString("strCounts");
            label10.Text = rm.GetString("strCounts") + "/S";
            //常用操作
            btnInvOnce.Text = rm.GetString("strBtnReadOnce");
            btnStartInv.Text = rm.GetString("strBtnStartReadData");
            btnStopInv.Text = rm.GetString("strBtnStopReadData");
            btnClear.Text = rm.GetString("strBtnClearListView");

            IsSingleDeviceCB.Text = rm.GetString("strSingleDev");
            //UHF 标签 读写操作
            labOprBank.Text = rm.GetString("strLabLockBank");
            labStartAdd.Text = rm.GetString("strLabStartAdd");
            labData.Text = rm.GetString("strLabRWData");
            labRWAccessPwd.Text = rm.GetString("strLabRWAccessPwd");
            labLength.Text = rm.GetString("strLabLength");
            btnReadData.Text = rm.GetString("strBtnReadTagData");
            btnWriteData.Text = rm.GetString("strBtnWriteTagData");
            btnClearData.Text = rm.GetString("strBtnClearListView");
            cbbLockType.Items.Clear();
            cbbLockType.Items.Add(rm.GetString("strItemUnLock"));
            cbbLockType.Items.Add(rm.GetString("strItemPerWritable"));
            cbbLockType.Items.Add(rm.GetString("strItemSecuLock"));
            cbbLockType.Items.Add(rm.GetString("strItemPerUnwritable"));

            gbLockTag.Text = rm.GetString("strGbLockTag");
            labLockBank.Text = rm.GetString("strLabLockBank");
            labLockType.Text = rm.GetString("strLabLockType");
            labLockAccessPwd.Text = rm.GetString("strLabRWAccessPwd");
            label14.Text = rm.GetString("strLabDestrlyPwd");
            btnLockTag.Text = rm.GetString("strBtnSetAlive");
            label15.Text = rm.GetString("strKillTag");

            gbIOOpr.Text = rm.GetString("strGbIOOpr");
            cbIn1.Text = rm.GetString("strCbIn1");
            cbIn2.Text = rm.GetString("strCbIn2");
            cbOut1.Text = rm.GetString("strCbOut1");
            cbOut2.Text = rm.GetString("strCbOut2");
            btnGetDI.Text = rm.GetString("strBtnGet");
            btnSetOutPort.Text = rm.GetString("strBtnSetAlive");
            //RFID 设备参数
            groupBox7.Text = rm.GetString("strGbDevParams");
            labDevNo.Text = rm.GetString("strLabDevNo");
            labNeightJudge.Text = rm.GetString("strLabNeighJudge");
            label3.Text = rm.GetString("strBuzzer");
            TriggerDelay.Text = rm.GetString("strAlert");
            WorkModeGB.Text = rm.GetString("strWormode");
            WorkModeLB.Text = rm.GetString("strWormode");
            DataTransMode.Text = rm.GetString("strTransMode");
            TriggerDelayLB.Text = rm.GetString("strTriggerDelayLB");
            rdoBuzzerOn.Text = rm.GetString("Open");
            rdoBuzzerOff.Text = rm.GetString("Close");
            comboBoxWorkMode.Items.Clear();
            comboBoxWorkMode.Items.Add(rm.GetString("strMaster"));
            comboBoxWorkMode.Items.Add(rm.GetString("strTimming"));
            comboBoxWorkMode.Items.Add(rm.GetString("strTrigger"));
            MultiAntWorktime.Text = rm.GetString("strWorktime");
            MultiAntPower.Text = rm.GetString("strPower");
            button4.Text = rm.GetString("strBtnGet");
            button6.Text = rm.GetString("strBtnSetAlive");
            checkBox95.Text = rm.GetString("strAll");
            checkBox91.Text = rm.GetString("strAll");
            checkBox90.Text = rm.GetString("strAll");
            checkBox89.Text = rm.GetString("strAll");
            groupBox1.Text = rm.GetString("str16Or32Ant");
            DataChannelCB.Items.Clear();
            DataChannelCB.Items.Add(rm.GetString("strRbSerialPort"));
            DataChannelCB.Items.Add(rm.GetString("strRJ45"));
            DataChannelCB.Items.Add("485");
            DataChannelCB.Items.Add("Wifi");

            factoryBtn.Text = rm.GetString("strFactory");
            label27.Text = rm.GetString("strFactory");
            label27.Text = rm.GetString("strAntTimePower");
            //  gbAntParams.Text = rm.GetString("strAntTimePower");
            cbAnt1.Text = rm.GetString("strCbAnt1");
            cbAnt2.Text = rm.GetString("strCbAnt2");
            cbAnt3.Text = rm.GetString("strCbAnt3");
            cbAnt4.Text = rm.GetString("strCbAnt4");
            GetAntStateBt.Text = rm.GetString("strAntState");
            LoginBtn.Text = rm.GetString("strLogin");
            SpecalFunLB.Text = rm.GetString("strSpecialFun");

            btnGetDevNo.Text = rm.GetString("strBtnGet");
            btnGetNeighJudge.Text = rm.GetString("strBtnGet");
            btnGetBuzzer.Text = rm.GetString("strBtnGet");
            btnGetWorkMode.Text = rm.GetString("strBtnGet");
            btnGetDelayTime.Text = rm.GetString("strBtnGet");
            //  btnGetAnts.Text = rm.GetString("strBtnGet");
            btnSetDevNo.Text = rm.GetString("strBtnSetAlive");
            btnSetNeighJudge.Text = rm.GetString("strBtnSetAlive");
            btnSetBuzzer.Text = rm.GetString("strBtnSetAlive");
            btnSetWorkMode.Text = rm.GetString("strBtnSetAlive");
            DataChannelSetBtn.Text = rm.GetString("strBtnSetAlive");
            btnSetDelayTime.Text = rm.GetString("strBtnSetAlive");
            TriggerAlarmBtn.Text = rm.GetString("strBtnSetAlive");
            btnSetAnts.Text = rm.GetString("strBtnSetAlive");

            Rb4Channel.Text = rm.GetString("str4Ant");
            rb16Ant.Text = rm.GetString("str16Ant");
            rb32Ant.Text = rm.GetString("str32Ant");
            frequncyGB.Text = rm.GetString("strWorkFre");
            BtnGetFrequency.Text = rm.GetString("strBtnGet");
            BtnSetFrequency.Text = rm.GetString("strBtnSetAlive");
            LBFRequency.Text = rm.GetString("strFrequencyArea");
            LBFrequencypoint.Text = rm.GetString("strFrequencyPoint");
            checkBox11.Text = rm.GetString("strAll");
            checkBox12.Text = rm.GetString("strAll");
            checkBox34.Text = rm.GetString("strAll");
            checkBox23.Text = rm.GetString("strAll");
            checkBox45.Text = rm.GetString("strAll");

            FrequencyAreaCB.Items.Clear();
            FrequencyAreaCB.Items.Add(rm.GetString("strUSA"));
            FrequencyAreaCB.Items.Add(rm.GetString("strChina1"));
            FrequencyAreaCB.Items.Add(rm.GetString("strChina2"));
            FrequencyAreaCB.Items.Add(rm.GetString("strEurope"));
            FrequencyAreaCB.Items.Add(rm.GetString("strFrequency") + "(" + rm.GetString("strHopping") + ")");
            FrequencyAreaCB.Items.Add(rm.GetString("strFrequency") + "(" + rm.GetString("strFix") + ")");
            //网络参数
            gbNetParams.Text = rm.GetString("strGbNetParams");
            labNetMode.Text = rm.GetString("strGbNetParams");
            labIPMode.Text = rm.GetString("strLabIPMode");
            labIPAdd.Text = rm.GetString("strLabIPAdd");
            labMask.Text = rm.GetString("strLabMask");
            labPort.Text = rm.GetString("strLabPort");
            labGateway.Text = rm.GetString("strLabGateway");
            labPromotion.Text = rm.GetString("strLabPromotion");
            labDestIP.Text = rm.GetString("strZlHeadIP");
            labDestPort.Text = rm.GetString("strLabPort");
            btnDefaultParams.Text = rm.GetString("strBtnDefaultParams");
            btnSetParams.Text = rm.GetString("strBtnSetParams");
            lvZl.Columns[0].Text = rm.GetString("strLvHeadNo");
            lvZl.Columns[2].Text = rm.GetString("strLabPort");
            btnSearchDev.Text = rm.GetString("strBtnSearchDev");
            // btnModifyDev.Text = rm.GetString("strBtnModifyDev");

            gbSPParams.Text = rm.GetString("strGbSPParams");
            labBaudRate.Text = rm.GetString("strLabBaudRate");
            labCheckBits.Text = rm.GetString("strLabCheckBits");
            labDataBits.Text = rm.GetString("strLabDataBits");
            //初始化提示语
            noDeviceChoose = rm.GetString("strChooseDevice");
            //特殊功能页
            ReadCardModeGB.Text = rm.GetString("strReadCardMode");
            //   label9.Text = rm.GetString("strIsMultiCard");
            button10.Text = rm.GetString("strBtnGet");
            button11.Text = rm.GetString("strBtnSetAlive");
            button14.Text = rm.GetString("strBtnGet");
            button13.Text = rm.GetString("strBtnSetAlive");
            DataFormateGB.Text = rm.GetString("strDataFormate");
            haveAntCB.Text = rm.GetString("strLvHeadAntNo");
            haveDevCB.Text = rm.GetString("strLvHeadDevNo");
            haveChannelCB.Text = rm.GetString("strChannelDir");
            AutoMatchGb.Text = rm.GetString("strAutoPoints");
            StartMatchBtn.Text = rm.GetString("strStartAutoPoints");
            //  MultiCardStatusRB.Text = rm.GetString("Open");
            //  MultiCardStatusOffRB.Text = rm.GetString("Close");
            //反键菜单页面
            /*  MultiAntPower.Text = rm.GetString("strPower");
              button4.Text = rm.GetString("strBtnGet");
              button6.Text = rm.GetString("strBtnSetAlive");
              checkBox95.Text = rm.GetString("strAll");
              checkBox91.Text = rm.GetString("strAll");
              checkBox90.Text = rm.GetString("strAll");
              checkBox89.Text = rm.GetString("strAll");
              groupBox1.Text = rm.GetString("str16Or32Ant");*/
            //消息栏目
            // showResultLV.Columns[0].Text = rm.GetString("strZlHeadNo");
            //  showResultLV.Columns[1].Text = rm.GetString("strTime");
            //  showResultLV.Columns[2].Text = rm.GetString("strMsg");
            //更新自动匹配参数页面
            if (processBox != null)
            {
                processBox.language(rm);
            }
        }

        private void checkBox60_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void groupBox7_Enter(object sender, EventArgs e)
        {

        }

        private void rdoBuzzerOff_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rdoBuzzerOn_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click_2(object sender, EventArgs e)
        {
            try
            {
                if (currentClient != null)
                {
                    reader.GetAnt(currentClient);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (Rb4Channel.Checked == false)
            {
                int[] dt = new int[4];
                int[] pwr = new int[4];
                if (int.TryParse(MultiAntTime.Text, out dt[0]) || MultiAntTime.Text == "")
                {
                    if (dt[0] < 50 || dt[0] > 10000)
                    {
                        MessageBox.Show(rm.GetString("strErrorAnttime"));
                        return;
                    }
                }
                if (int.TryParse(MultiAntPowerTB.Text, out pwr[0]))
                {
                    if (pwr[0] > 33 || MultiAntPowerTB.Text == "")
                    {
                        MessageBox.Show(rm.GetString("strErrorPower"));
                        return;
                    }
                }
                else
                {
                    MessageBox.Show(rm.GetString("strErrorPower"));
                    return;
                }

                byte channel = 16;
                for (int i = 24; i < 32; i++)
                {
                    if (multiAntCB[i].Checked == true)
                    {
                        channel = 32;
                    }
                }
                for (int i = 16; i < 24; i++)
                {
                    if (multiAntCB[i].Checked == true)
                    {
                        channel = 32;
                    }
                }
                if (rb32Ant.Checked == true)
                {
                    channel = 32;
                }
                multichannelAnt ant = new multichannelAnt(channel);
                ant.state = channel;
                if (ant.state == 16)
                {
                    for (int index = 0; index < 16; index++)
                    {
                        if (multiAntCB[index].Checked == true)
                        {
                            ant.enable[index] = 0x01;
                        }
                    }
                }
                else if (ant.state == 32)
                {
                    for (int index = 0; index < 32; index++)
                    {
                        if (multiAntCB[index].Checked == true)
                        {
                            ant.enable[index] = 0x01;
                        }
                    }
                }
                ant.dwell_time = int.Parse(MultiAntTime.Text);
                ant.power = int.Parse(MultiAntPowerTB.Text);
                try
                {
                    if (currentClient != null)
                    {
                        reader.SetMultiAnt(currentClient, ant);
                    }
                    else
                    {
                        MessageBox.Show(noDeviceChoose);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                btnSetAnts_Click(null, null);
            }

        }

        private void checkBox95_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox95.Checked)
            {
                for (int index = 0; index < 8; index++)
                {
                    multiAntCB[index].Checked = true;
                }
            }
            else
            {
                for (int index = 0; index < 8; index++)
                {
                    multiAntCB[index].Checked = false;
                }
            }
        }

        private void checkBox91_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox91.Checked)
            {
                for (int index = 8; index < 16; index++)
                {
                    multiAntCB[index].Checked = true;
                }
            }
            else
            {
                for (int index = 8; index < 16; index++)
                {
                    multiAntCB[index].Checked = false;
                }
            }
        }

        private void checkBox90_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox90.Checked)
            {
                for (int index = 16; index < 24; index++)
                {
                    multiAntCB[index].Checked = true;
                }
            }
            else
            {
                for (int index = 16; index < 24; index++)
                {
                    multiAntCB[index].Checked = false;
                }
            }
        }

        private void checkBox89_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox89.Checked)
            {
                for (int index = 24; index < 32; index++)
                {
                    multiAntCB[index].Checked = true;
                }
            }
            else
            {
                for (int index = 24; index < 32; index++)
                {
                    multiAntCB[index].Checked = false;
                }
            }
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {

        }

        private void FrequencyAreaCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (FrequencyAreaCB.SelectedIndex == 0 || FrequencyAreaCB.SelectedIndex == 1 || FrequencyAreaCB.SelectedIndex == 2 || FrequencyAreaCB.SelectedIndex == 3)
            {
                ConstantfrequencyTB.Enabled = false;
                for (int i = 0; i < frequencysCB.Length; i++)
                {
                    frequencysCB[i].Enabled = false;
                }
                checkBox11.Enabled = false;
                ConstantfrequencyTB.Enabled = false;
                checkBox23.Enabled = false;
                checkBox34.Enabled = false;
                checkBox45.Enabled = false;
            }
            else if (FrequencyAreaCB.SelectedIndex == 4)
            {
                ConstantfrequencyTB.Enabled = false;
                for (int i = 0; i < frequencysCB.Length; i++)
                {
                    frequencysCB[i].Enabled = true;
                }
                checkBox11.Enabled = true;
                checkBox23.Enabled = true;
                checkBox34.Enabled = true;
                checkBox45.Enabled = true;
            }
            else if (FrequencyAreaCB.SelectedIndex == 5)
            {
                ConstantfrequencyTB.Text = "902.5";
                ConstantfrequencyTB.Enabled = true;
                for (int i = 0; i < frequencysCB.Length; i++)
                {
                    frequencysCB[i].Enabled = false;
                }
                checkBox11.Enabled = false;
                checkBox23.Enabled = false;
                checkBox34.Enabled = false;
                checkBox45.Enabled = false;
            }
        }

        private void GetAntStateBt_Click(object sender, EventArgs e)
        {
            try
            {
                if (currentClient != null)
                {
                    reader.GetAntState(currentClient);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button9_Click_1(object sender, EventArgs e)
        {
            /*  byte status = (byte)(MultiCardStatusRB.Checked ? 1 : 0);
              try
              {
                  if (currentClient != null)
                  {
                      reader.SetMultiCard(currentClient, status);
                  }
                  else
                  {
                      MessageBoxBox.Show(noDeviceChoose);
                  }
              }
              catch (Exception ex)
              {
                  MessageBoxBox.Show(ex.ToString());
              }*/
        }

        private void button7_Click_1(object sender, EventArgs e)
        {
            if (SpecialFunTabTB.Text == "admin")
            {
                this.tabSpecialFun.Parent = this.tabControl1;
                UpdateView();
            }
        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (currentClient != null)
                {
                    reader.GetMultiCard(currentClient);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox10_Enter(object sender, EventArgs e)
        {

        }

        private void rb32Ant_CheckedChanged(object sender, EventArgs e)
        {
            MultiAntEnable(32);
            FourAntDisAble();
        }

        private void rb16Ant_CheckedChanged(object sender, EventArgs e)
        {
            MultiAntEnable(16);
            FourAntDisAble();
        }

        private void button7_Click_2(object sender, EventArgs e)
        {

        }

        private void tabSpecialFun_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                if (currentClient != null)
                {
                    reader.GetQuencyPara(currentClient, 0x00);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                if (sessionCB.Text == "" || ValueQCB.Text == "" || tagFocusCB.Text == "" || valueABCB.Text == "")
                {
                    MessageBox.Show(rm.GetString("strMsgDevNoNotEmpty"));
                    return;
                }
                if (currentClient != null)
                {
                    QuencyPara quencyPara = new QuencyPara
                    {
                        session = byte.Parse(sessionCB.Text),
                        valueQ = byte.Parse(ValueQCB.Text),
                        tagfocus = byte.Parse(tagFocusCB.Text),
                        valueAB = byte.Parse(valueABCB.Text),
                    };
                    reader.SetQuencyPara(currentClient, quencyPara);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            try
            {
                if (currentClient != null)
                {
                    reader.GetFormartTagsData(currentClient, 0x00);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            try
            {
                if (currentClient != null)
                {
                    reader.Factory(currentClient, 0x00);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            byte type = 0x00;
            if (haveAntCB.Checked)
                type |= 0x80;
            if (haveRssiCB.Checked)
                type |= 0x40;
            if (haveDevCB.Checked)
                type |= 0x20;
            if (haveChannelCB.Checked)
                type |= 0x10;
            try
            {
                if (currentClient != null)
                {
                    reader.SetFormartTagsData(currentClient, type);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }
        QuencyTagForm quencyTagForm;
        private void 查询标签ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            quencyTagForm = new QuencyTagForm(reader, this, currentClient, rm);
            quencyTagForm.StartPosition = FormStartPosition.Manual;
            quencyTagForm.Location = Control.MousePosition;
            quencyTagForm.Show();
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
        //自动匹配参数

        private void button7_Click_3(object sender, EventArgs e)
        {
            processBox = new ProcessBox(reader, currentClient, rm);
            processBox.StartPosition = FormStartPosition.CenterScreen;
            processBox.Show();
            try
            {
                if (currentClient != null)
                {
                    //可以自定义从第几步开始
                    reader.IntelligentDetectionStart(currentClient, 0x05, language);
                }
                else
                {
                    MessageBox.Show(noDeviceChoose);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private async void button15_Click(object sender, EventArgs e)
        {
            lvTagData.Items.Clear();
            lvTagData.DefaultSort();
            await UpdateListView();
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }
        TagFormat tagFormat = null;
        private void 数据显示格式ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tagFormat = new TagFormat(reader, currentClient, rm);
            tagFormat.StartPosition = FormStartPosition.Manual;
            tagFormat.Location = Control.MousePosition;
            tagFormat.Show();
        }
        QuencyMode quencyMode = null;
        private void 寻卡模式ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            quencyMode = new QuencyMode(reader, currentClient, rm);
            quencyMode.StartPosition = FormStartPosition.Manual;
            quencyMode.Location = Control.MousePosition;
            quencyMode.Show();
        }
        FrequencyPoints frequencyPoints = null;
        private void 频点ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frequencyPoints = new FrequencyPoints(reader, currentClient, rm);
            frequencyPoints.StartPosition = FormStartPosition.Manual;
            frequencyPoints.Location = Control.MousePosition;
            frequencyPoints.Show();
        }
        AntForm antForm = null;
        private ProcessBox processBox;
        private AutoReadCard auto;
        private string epcPath = "epc";
        private string countPath = "count";

        private void 天线ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            antForm = new AntForm(reader, currentClient, rm);
            antForm.StartPosition = FormStartPosition.Manual;
            antForm.Location = Control.MousePosition;
            antForm.Show();
        }

        private void rb8Ant_CheckedChanged(object sender, EventArgs e)
        {
            MultiAntEnable(8);
            FourAntDisAble();
        }

        private void FourAntDisAble()
        {
            cbAnt1.Enabled = false;
            cbAnt2.Enabled = false;
            cbAnt3.Enabled = false;
            cbAnt4.Enabled = false;
            comboBoxWT1.Enabled = false;
            comboBoxWT2.Enabled = false;
            comboBoxWT3.Enabled = false;
            comboBoxWT4.Enabled = false;
            comboBoxPower1.Enabled = false;
            comboBoxPower2.Enabled = false;
            comboBoxPower3.Enabled = false;
            comboBoxPower4.Enabled = false;
        }

        private void Rb4Channel_CheckedChanged(object sender, EventArgs e)
        {
            MultiAntEnable(0);
            FourAntEnAble();
        }

        private void FourAntEnAble()
        {
            cbAnt1.Enabled = true;
            cbAnt2.Enabled = true;
            cbAnt3.Enabled = true;
            cbAnt4.Enabled = true;
            comboBoxWT1.Enabled = true;
            comboBoxWT2.Enabled = true;
            comboBoxWT3.Enabled = true;
            comboBoxWT4.Enabled = true;
            comboBoxPower1.Enabled = true;
            comboBoxPower2.Enabled = true;
            comboBoxPower3.Enabled = true;
            comboBoxPower4.Enabled = true;
        }

        private void MultiAntEnable(int v)
        {
            for (int index = 0; index < multiAntCB.Length; index++)
            {
                if (index < v)
                {
                    multiAntCB[index].Enabled = true;
                }
                else
                {
                    multiAntCB[index].Enabled = false;
                }
            }
        }

        private void frequency50_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void frequncyGB_Enter(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, panel1.ClientRectangle,
                         Color.Gray, 1, ButtonBorderStyle.Solid, //左边
                         Color.Gray, 1, ButtonBorderStyle.Solid, //上边
                         Color.Gray, 1, ButtonBorderStyle.Solid, //右边
                         Color.Gray, 1, ButtonBorderStyle.Solid);//底边
        }

        private void button8_Click_2(object sender, EventArgs e)
        {
            try
            {
                ConstantfrequencyTB.Text = (float.Parse(ConstantfrequencyTB.Text) - 0.125).ToString();
                this.ConstantfrequencyTB.DroppedDown = false;
                Cursor.Current = Cursors.Default;
            }
            catch
            {
                rm.GetString("strInvalid1000");
            }

        }

        private void button9_Click_2(object sender, EventArgs e)
        {
            try
            {
                ConstantfrequencyTB.Text = (float.Parse(ConstantfrequencyTB.Text) + 0.125).ToString();
                this.ConstantfrequencyTB.DroppedDown = false;
                Cursor.Current = Cursors.Default;
            }
            catch
            {
                rm.GetString("strInvalid1000");
            }
        }

        private string[] frequencyStr = new string[] { "902.5", "903.0", "903.5", "904.0", "904.5", "905.0", "905.5", "906.0", "906.5","907.0",
                                                 "907.5","908.0","908.5","909.0","909.5","910.0","910.5","911.0","911.5","912.0",
                                                 "912.5","913.0","913.5","914.0","914.5","915.0","915.5","916.0","916.5","917.0",
                                                 "917.5","918.0","918.5","919.0","919.5","920.0","920.5","921.0","921.5","922.0",
                                                 "922.5","923.0","923.5","924.0","924.5","925.0","925.5","926.0","926.5","927.0"};
        private byte language;



        private void ConstantfrequencyTB_Click(object sender, EventArgs e)
        {
            // ConstantfrequencyTB.Text = "";
            this.ConstantfrequencyTB.DroppedDown = true;
        }


        private void ConstantfrequencyTB_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void button7_Click_4(object sender, EventArgs e)
        {

        }

        private void ConstantfrequencyTB_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void ConstantfrequencyTB_TextUpdate(object sender, EventArgs e)
        {
            try
            {
                string zn = ConstantfrequencyTB.Text;

                ConstantfrequencyTB.Items.Clear();
                //当在ComboBox输入内容时，内容文本是倒序输出的，光标位置始终在最前面。
                this.ConstantfrequencyTB.SelectionStart = this.ConstantfrequencyTB.Text.Length;
                foreach (string str in frequencyStr)
                {
                    if (str.Contains(zn))
                    {
                        ConstantfrequencyTB.Items.Add(str);
                    }
                }
                if (ConstantfrequencyTB.Items.Count == 0)
                    ConstantfrequencyTB.Items.Add("");
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void TagAccessPage_Click(object sender, EventArgs e)
        {

        }
    }

}

//...................................旧版程序中初始化socket使用 已弃用
public struct WSADATA
{
    public short wVersion;
    public short wHighVersion;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 6)]
    public string szDescription;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 6)]
    public string szSystemStatus;
    [Obsolete] // Ignored for v2.0 and above 
    public int wMaxSockets;
    [Obsolete] // Ignored for v2.0 and above 
    public int wMAXUDPDG;
    public IntPtr dwVendorInfo;
}
public struct Device
{
    public int hDev;
    public string host;
}
//...................................旧版程序中初始化socket使用 已弃用

public sealed class EPC_data : IComparable
{
    public string epc;
    public int count;
    public ushort devNo;
    public byte antNo;
    public string IP;
    public byte rssi;
    public byte dir;
    int IComparable.CompareTo(object obj)
    {
        EPC_data temp = (EPC_data)obj;
        {
            return string.Compare(this.epc, temp.epc);
        }
    }
}

